﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Pandora Engine Library")]
[assembly: AssemblyDescription("Engine Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Projekt-Pandora.de")]
[assembly: AssemblyProduct("Engine")]
[assembly: AssemblyCopyright("Copyright © Projekt-Pandora.de 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: AssemblyVersion("0.0.1.0")]
[assembly: AssemblyFileVersion("0.0.1.0")]

[assembly: Guid("5819b286-33bc-4bdb-9d6a-166af8728e95")]
