﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Stellt einen Konfigurationseintrag dar
    /// </summary>
    public sealed class ConfigurationEntry
    {
        /// <summary>
        /// Erstellt einbe neue Instanz der ConfigurationEntry-Klasse
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public ConfigurationEntry(string name, string value)
        {
            Name = name;
            Value = value;
            LastType = typeof(string);
        }

        /// <summary>
        /// Liefert den Datentyp
        /// </summary>
        public Type LastType { get; private set; }

        /// <summary>
        /// Liefert den Namen des Eintrags
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Liefert den Wert
        /// </summary>
        public string Value { get; internal set; }

        /// <summary>
        /// Liefert den Wert der hinter dem Eintrag steht
        /// </summary>
        /// <param name="type"></param>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public object GetValue(Type type, object defaultvalue)
        {
            LastType = type;

            if (type.IsEnum)
            {
                try
                {
                    return Enum.Parse(type, Value);
                }
                catch (Exception)
                {
                    return defaultvalue;
                }
            }
            else
            {
                try
                {
                    return Convert.ChangeType(Value, type);
                }
                catch (Exception)
                {
                    return defaultvalue;
                }
            }
        }

        /// <summary>
        /// Liefert den Wert der hinter dem Eintrag steht
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public T GetValue<T>(T defaultvalue)
        {
            return (T)GetValue(typeof(T), defaultvalue);
        }

        /// <summary>
        /// Legt den Wert der hinter dem Eintrag steht fest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void SetValue<T>(T value)
        {
            Value = Convert.ToString(value);
        }
    }
}
