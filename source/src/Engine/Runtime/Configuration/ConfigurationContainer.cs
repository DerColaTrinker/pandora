﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Eine Kontainer der eine Kofuguration speichert und verwaltet
    /// </summary>
    public sealed class ConfigurationContainer : IEnumerable<ConfigurationSection>
    {
        private Dictionary<string, ConfigurationSection> _sections = new Dictionary<string, ConfigurationSection>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        /// Erstellt eine neue Instanz der ConfigurationContainer-Klasse
        /// </summary>
        public ConfigurationContainer()
        { }

        /// <summary>
        /// Liest eine Konfigurationdatei ein
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Open(string filename)
        {
            File = new FileInfo(filename);

            if (!File.Exists) return false;

            using (var reader = new StreamReader(File.OpenRead(), Encoding.Default, true))
            {
                var currentsection = new ConfigurationSection("global");
                Add(currentsection);

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Trim();

                    if (string.IsNullOrEmpty(line)) continue;
                    if (string.IsNullOrWhiteSpace(line)) continue;

                    // Hadelt es sich um eine Kommentarzeile?
                    if (line.StartsWith(";") | line.StartsWith("#")) continue;

                    // Ist es eine Sektion?
                    if (line.StartsWith("[") & line.EndsWith("]"))
                    {
                        // Es wurde kein Name angegeben
                        if (line.IndexOf("]") <= 2) continue;

                        var name = line.Substring(1, line.Length - 2).Trim();

                        currentsection = new ConfigurationSection(name);
                        Add(currentsection);
                    }
                    else
                    {
                        // Sicherstellen das keine fehlerhaften Sektionen als Einträge erkannt werden
                        if (!line.StartsWith("[") & line.EndsWith("]")) continue;
                        if (line.StartsWith("[") & !line.EndsWith("]")) continue;

                        var assignindex = line.IndexOf("=");

                        if (assignindex == 0) continue;

                        if (assignindex == -1)
                        {
                            currentsection.Add(line, "");
                        }
                        else
                        {
                            if (assignindex >= line.Length)
                            {
                                var name = line.Substring(0, assignindex).Trim();

                                currentsection.Add(name, "");
                            }
                            else
                            {
                                var name = line.Substring(0, assignindex).Trim();
                                var value = line.Substring(assignindex + 1).Trim();

                                currentsection.Add(name, value);
                            }
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Speichert die aktuelle Konfiguration in die angegebene Datei
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool SaveAs(string filename)
        {
            File = new FileInfo(filename);

            return Save();
        }

        /// <summary>
        /// Speichert die aktuelle Konfiguration
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            if (File == null) return false;

            using (var writer = new StreamWriter(File.Open(FileMode.Create, FileAccess.Write), Encoding.Default))
            {
                foreach (var section in this)
                {
                    if (section.Sectionname != "global") writer.WriteLine("[{0}]", section.Sectionname);

                    foreach (var entry in section)
                    {
                        writer.WriteLine("{0}={1}", entry.Name, entry.Value);
                    }

                    writer.WriteLine();
                    writer.Flush();
                }
            }

            return true;
        }

        /// <summary>
        /// Liefert die Konfigurationsdatei
        /// </summary>
        public FileInfo File { get; private set; }

        //public string Sectionname { get; internal set; }

        /// <summary>
        /// Fügt eine Sektion hinzu
        /// </summary>
        /// <param name="sectionname"></param>
        public void Add(string sectionname)
        {
            Add(new ConfigurationSection(sectionname));
        }

        /// <summary>
        /// Fügt eine Sektion hinzu
        /// </summary>
        /// <param name="section"></param>
        public void Add(ConfigurationSection section)
        {
            _sections[section.Sectionname] = section;
        }

        /// <summary>
        /// Entfernt eine Sektion
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(string key)
        {
            return _sections.Remove(key);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Sektionen
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ConfigurationSection> GetEnumerator()
        {
            return _sections.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _sections.Values.GetEnumerator();
        }

        /// <summary>
        /// Liefert die Anzahl der Sektionen
        /// </summary>
        public int Count { get { return _sections.Count; } }

        /// <summary>
        /// Liefert den Wert der hinter der Sektion und Eintrag steht
        /// </summary>
        /// <param name="sectionname"></param>
        /// <param name="entryname"></param>
        /// <param name="type"></param>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public object GetValue(string sectionname, string entryname, Type type, object defaultvalue)
        {
            var entry = (ConfigurationSection)null;

            if (_sections.TryGetValue(sectionname, out entry))
            {
                return entry.GetValue(entryname, type, defaultvalue);
            }
            else
            {
                return defaultvalue;
            }
        }

        /// <summary>
        /// Liefert den Wert der hinter der Sektion und Eintrag steht
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sectionname"></param>
        /// <param name="entryname"></param>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public T GetValue<T>(string sectionname, string entryname, T defaultvalue)
        {
            var entry = (ConfigurationSection)null;

            if (_sections.TryGetValue(sectionname, out entry))
            {
                return (T)entry.GetValue<T>(entryname, defaultvalue);
            }
            else
            {
                return defaultvalue;
            }
        }

        /// <summary>
        /// Legt den Wert der hinter der Sektion und Eintrag steht fest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sectionname"></param>
        /// <param name="entryname"></param>
        /// <param name="value"></param>
        public void SetValue<T>(string sectionname, string entryname, T value)
        {
            var entry = (ConfigurationSection)null;

            if (_sections.TryGetValue(sectionname, out entry))
            {
                entry.SetValue<T>(entryname, value);
            }
            else
            {
                var section = new ConfigurationSection(sectionname);
                section.Add(entryname, Convert.ToString(value));
                Add(section);
            }
        }
    }
}
