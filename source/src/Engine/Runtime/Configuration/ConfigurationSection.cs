﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Stellt eine Konfiguration-Sektion dar
    /// </summary>
    public sealed class ConfigurationSection : IEnumerable<ConfigurationEntry>
    {
        private Dictionary<string, ConfigurationEntry> _entries = new Dictionary<string, ConfigurationEntry>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        /// Erstellt eine neue Instanz der ConfigurationSection-Klasse
        /// </summary>
        /// <param name="sectionname"></param>
        public ConfigurationSection(string sectionname)
        {
            Sectionname = sectionname;
        }

        /// <summary>
        /// Liefert den Namen der Sektion
        /// </summary>
        public string Sectionname { get; internal set; }

        /// <summary>
        /// Fügt einen Eintrag hinzu
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(string key, string value)
        {
            Add(new ConfigurationEntry(key, value));
        }

        /// <summary>
        /// Fügt einen Eintrag hinzu
        /// </summary>
        /// <param name="entry"></param>
        public void Add(ConfigurationEntry entry)
        {
            _entries[entry.Name] = entry;
        }

        /// <summary>
        /// Entfernt einen Eintrag
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(string key)
        {
            return _entries.Remove(key);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Einträge
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ConfigurationEntry> GetEnumerator()
        {
            return _entries.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _entries.Values.GetEnumerator();
        }

        /// <summary>
        /// Liefert die Anzahl der Einträge
        /// </summary>
        public int Count { get { return _entries.Count; } }

        /// <summary>
        /// Liefert den Wert der hinter der Sketion und Eintrag steht
        /// </summary>
        /// <param name="entryname"></param>
        /// <param name="type"></param>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public object GetValue(string entryname, Type type, object defaultvalue)
        {
            var entry = (ConfigurationEntry)null;

            if (_entries.TryGetValue(entryname, out entry))
            {
                return entry.GetValue(type, defaultvalue);
            }
            else
            {
                return defaultvalue;
            }
        }

        /// <summary>
        /// Liefert den Wert der hinter der Sketion und Eintrag steht
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entryname"></param>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public T GetValue<T>(string entryname, T defaultvalue)
        {
            var entry = (ConfigurationEntry)null;

            if (_entries.TryGetValue(entryname, out entry))
            {
                return (T)entry.GetValue<T>(defaultvalue);
            }
            else
            {
                return defaultvalue;
            }
        }

        /// <summary>
        /// Legt den Wert der hinter der Sketion und Eintrag steht fest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entryname"></param>
        /// <param name="value"></param>
        public void SetValue<T>(string entryname, T value)
        {
            var entry = (ConfigurationEntry)null;

            if (_entries.TryGetValue(entryname, out entry))
            {
                entry.SetValue<T>(value);
            }
            else
            {
                Add(entryname, Convert.ToString(value));
            }
        }
    }
}
