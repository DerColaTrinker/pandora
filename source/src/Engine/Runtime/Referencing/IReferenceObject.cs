﻿using System;

namespace Pandora.Runtime.Referencing
{
    /// <summary>
    /// Stellt einen Referenz-Pointer bereit
    /// </summary>
    public interface IReferenceObject : IDisposable
    {
        /// <summary>
        /// Liefert den Referenz-Pointer
        /// </summary>
        uint ReferenceID { get; }
    }
}
