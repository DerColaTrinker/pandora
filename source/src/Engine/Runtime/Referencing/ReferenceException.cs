﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Runtime.Referencing
{
    /// <summary>
    /// Ausnahme die bei der Bearbeitung der Objekt Referenzierung aufgetreten ist
    /// </summary>
    [Serializable]
    public class ReferenceException : Exception
    {
        /// <summary>
        /// Erstellt eine neue Instanz der ObjectReferencingException-Klasse
        /// </summary>
        public ReferenceException()
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der ObjectReferencingException-Klasse
        /// </summary>
        /// <param name="message">Die Fehlermeldung</param>
        public ReferenceException(string message)
            : base(message)
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der ObjectReferencingException-Klasse
        /// </summary>
        /// <param name="message">Die Fehlermeldung</param>
        /// <param name="inner">Eine weitere Ausnahme die gekapselt wird</param>
        public ReferenceException(string message, Exception inner)
            : base(message, inner)
        { }
    }
}
