﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Referencing
{
    /// <summary>
    /// Stellt eine Objekt-Referenz dar und verwaltet den Referenz-Pointer
    /// </summary>
    public abstract class ReferenceObject : IReferenceObject
    {
        private bool _disposed;

        /// <summary>
        /// Erstellt eine neue Instanz der ObjectBase-Klasse mit einer neuen ObjectID
        /// </summary>
        public ReferenceObject()
        {
            ReferenceID = ReferenceManager.Create(this);
        }

        /// <summary>
        /// Erstellt eine neue Instanz der ObjectBase-Klasse mit der angegebenen ObjectID
        /// </summary>
        /// <param name="objid">Die ObjektID</param>
        //protected ReferenceObject(uint objid)
        //{
        //    ReferenceID = objid;
        //    ReferenceManager.Insert(this);
        //}

        /// <summary>
        /// Dekonstruktor
        /// </summary>
        ~ReferenceObject()
        {
            Dispose(false);

            // Objekt Refenrenz entfernen
            ReferenceManager.Release(this);
        }

        /// <summary>
        /// Liefert die Objekt ID
        /// </summary>
        public uint ReferenceID { get; internal set; }

        /// <summary>
        /// Prüft ob die beiden Spielobjekte gleich sind
        /// </summary>
        /// <param name="a">Linkes Objekt</param>
        /// <param name="b">Rechtes Objekt</param>
        /// <returns>Liefert 'true' wenn beide Objekte die gleiche ObjectID besitzen</returns>
        public static bool operator ==(ReferenceObject a, ReferenceObject b)
        {
            if ((object)a == null | (object)b == null) return false;

            return a.Equals(b);
        }

        /// <summary>
        /// Prüft ob die beiden Spielobjekte ungleich sind
        /// </summary>
        /// <param name="a">Linkes Objekt</param>
        /// <param name="b">Rechtes Objekt</param>
        /// <returns>Liefert 'true' wenn beide Objekte eine abweichende ObjectID besitzen</returns>
        public static bool operator !=(ReferenceObject a, ReferenceObject b)
        {
            if ((object)a == null | (object)b == null) return false;

            return !a.Equals(b);
        }

        /// <summary>
        /// Fungiert als Hashfunktion für einen bestimmten Typ.
        /// </summary>
        /// <returns>Ein Hashcode für das aktuelle GameObject.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (int)ReferenceID;
            }
        }

        /// <summary>
        /// Bestimmt, ob das angegebene GameObject und das aktuelle GameObject gleich sind.
        /// </summary>
        /// <param name="obj">Das Objekt, das mit dem aktuellen Objekt verglichen werden soll.</param>
        /// <returns>true, wenn das angegebene Objekt und das aktuelle Objekt gleich sind, andernfalls false.</returns>
        public override bool Equals(object obj)
        {
            if ((object)obj == null) return false;

            var s = this as ReferenceObject;
            var d = obj as ReferenceObject;
            if (s == null || d == null) return false;

            return s.ReferenceID == d.ReferenceID;
        }

        /// <summary>
        /// Konvertiert eine ObjectReference direkt in ein uint
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static implicit operator uint(ReferenceObject obj)
        {
            return obj.ReferenceID;
        }

        #region IDisposable Member

        /// <summary>
        /// Gibt das Objekt und alle Ressourcen frei.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gibt das Objekt und alle Ressourcen frei.
        /// </summary>
        /// <param name="disposing">Gibt an ob die ob der Finalisier aufgerufen werden soll</param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    ReferenceManager.Release(this);
                    OnDisposing();

                    GC.SuppressFinalize(this);
                }

                _disposed = true;
            }
        }

        /// <summary>
        /// Wird aufgerufen, wenn das Objekt entladen wird.
        /// </summary>
        protected virtual void OnDisposing()
        {
        }

        #endregion
    }
}
