﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Referencing
{
    /// <summary>
    /// Stellt Funktionen der Objekt Referenzierung bereit
    /// </summary>
    public static class ReferenceManager
    {
        private readonly static Dictionary<uint, bool> _references = new Dictionary<uint, bool>();
        private static bool _usereferencing;

        static ReferenceManager()
        {
            _usereferencing = true;
        }

        internal static uint Create(ReferenceObject obj)
        {
            if (!ReferenceManager.UseReferencing) return 0;

            lock (_references)
            {
                // Die nächste Freie ObjectID holen
                var id = GetNextObjectID();

                // Angeben, das dieses Objekt referenziert ist
                _references[id] = true;

                // ID zurück liefern
                return id;
            }
        }

        internal static bool Boxing(ReferenceObject obj, uint refid)
        {
            if (!ReferenceManager.UseReferencing) return false;

            lock (_references)
            {
                // Prüfen ob die Reference bereits vergeben ist
                if (_references.ContainsKey(refid) && _references[refid]) return false;

                _references[refid] = true;
                obj.ReferenceID = refid;

                return true;
            }
        }

        internal static void Insert(ReferenceObject obj)
        {
            if (!ReferenceManager.UseReferencing) return;

            lock (_references)
            {
                // Eine Referenz einfügen, sicherstellen das die ObjectID nicht bereits vergeben ist
                if (_references.ContainsKey(obj.ReferenceID) && _references[obj.ReferenceID])
                    throw new ReferenceException(string.Format("ObjectID already in use '{0}'", obj.ReferenceID));

                _references[obj.ReferenceID] = true;
            }
        }

        internal static void Release(ReferenceObject obj)
        {
            lock (_references)
            {
                if (_references.ContainsKey(obj.ReferenceID))
                    _references[obj.ReferenceID] = false;
            }
        }

        /// <summary>
        /// Gibt an das die Referenzierung verwendet wird oder legt sie fest
        /// </summary>
        public static bool UseReferencing
        {
            get { return _usereferencing; }
            set
            {
                _usereferencing = value;

                if (value)
                    Logger.Debug("Enable referencing");
                else
                    Logger.Debug("Disable referencing");
            }
        }

        /// <summary>
        /// Beginnt mit einem Aufräum-Prozess der nicht mehr referenzierte Verweise aus dem Speicher entfernt. Diese Funktion gibt auch nicht mehr verwendete ReferenceIDs frei.
        /// </summary>
        /// <remarks>Diese Funktion sollte nur beim beenden des Programms aufgerufen werden.</remarks>
        public static void FinalizeAllObjects()
        {
            lock (_references)
            {
                var collectedids = _references.Where(m => m.Value == false).Select(m => m.Key);

                foreach (var id in collectedids)
                    _references.Remove(id);

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// Liefert die Anzahl der Objekte
        /// </summary>
        public static int Count { get { return _references.Count; } }

        internal static uint GetNextObjectID()
        {
            lock (_references)
            {
                if (_references.Count == 0)
                {
                    // Start bei 0
                    return (uint)0;
                }
                else
                {
                    // Nach allen ObjectIDs suchen die keine Referenz mehr haben
                    var result = from r in _references
                                 where r.Value == false
                                 select r.Key;

                    if (result.Count() == 0)
                    {
                        // Keine freie ObjectID mehr vorhanden
                        return _references.Keys.Max() + 1;
                    }
                    else
                    {
                        // Nächste freie ObjectID zurückliefern
                        return result.FirstOrDefault();
                    }
                }
            }
        }
    }
}
