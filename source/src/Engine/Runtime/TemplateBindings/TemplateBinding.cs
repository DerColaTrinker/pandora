﻿using Pandora.Game.Templates;
using Pandora.Game.Templates.Configuration;
using Pandora.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.TemplateBindings
{
    public sealed class TemplateBinding
    {
        private TemplateAttribute _tempdesc;

        internal TemplateBinding(Type type)
        {
            TemplateType = type;

            //TODO: Upgrageabble ?
            if (type.BaseType.Name.Contains("UpgradableTemplateBase") && type.BaseType.GenericTypeArguments[0].Name == "BuildingLevelTemplate")
            {
                UpgradeTemplateType = type.BaseType.GenericTypeArguments[0];
            }

            _tempdesc = AttributeUtility.GetAttribute<TemplateAttribute>(TemplateType);
            if (_tempdesc == null)
            {
                _tempdesc = new TemplateAttribute(TemplateType.Name);
            }

            // Eigenschaften suchen
            Properties = (from p in TemplateType.GetProperties()
                          let propdesc = AttributeUtility.GetAttribute<TemplatePropertyAttribute>(p)
                          where propdesc != null
                          select new TemplatePropertyBinding(this, propdesc, p))
                          .OrderByDescending(m => m.SystemValue)
                          .OrderBy(m => m.Order)
                          .ToArray();
        }

        public Type TemplateType { get; private set; }

        public bool IsUpgradable { get { return UpgradeTemplateType != null; } }

        public bool IsUpgrade { get { return TemplateType.BaseType == typeof(UpgradeTemplateBase); } }

        public Type UpgradeTemplateType { get; private set; }

        public string TemplateName { get { return _tempdesc.Name; } }

        public IEnumerable<TemplatePropertyBinding> Properties { get; private set; }
    }
}
