﻿using Pandora.Game.Templates;
using Pandora.Game.Templates.Configuration;
using Pandora.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Pandora.Runtime.TemplateBindings
{
    public sealed class TemplatePropertyBinding
    {
        private TemplatePropertyAttribute _procdesc;
        private SandboxPropertyRequiredAttribute _requiredcolumn;
        private PropertyInfo _property;

        internal TemplatePropertyBinding(TemplateBinding binding, TemplatePropertyAttribute propdesc, PropertyInfo p)
        {
            TemplateBinding = binding;

            _property = p;
            _procdesc = propdesc;

            // Wenn kein alternativer Name vergeben wurde...
            if (string.IsNullOrEmpty(_procdesc.Name)) _procdesc.Name = p.Name;

            // Weitere eigenschaften auslesen
            _requiredcolumn = AttributeUtility.GetAttribute<SandboxPropertyRequiredAttribute>(p);

            GetPropertyType();
        }

        private void GetPropertyType()
        {
            //if (!IsEnum)
            PropertyType = _property.PropertyType;
            //else
            //    PropertyType = _property.PropertyType.GetEnumUnderlyingType();
        }

        #region Template Konfiguration

        public TemplateBinding TemplateBinding { get; private set; }

        public Type PropertyType { get; private set; }

        public bool ReadOnly { get { return !_property.CanWrite; } }

        public bool IsEnum { get { return _property.PropertyType.IsEnum; } }

        public void SetValue(TemplateBase instance, object value)
        {
            _property.SetValue(instance, value);
        }

        public object GetValue(TemplateBase instance)
        {
            return _property.GetValue(instance);
        }

        #endregion

        #region Sandbox Konfiguration

        public string Name { get { return _procdesc.Name; } }

        public SandboxColumnColors ColumnColor { get { return _procdesc.Color; } }

        public bool IsRequiredColumn { get { return _requiredcolumn != null; } }

        public bool SystemValue { get { return _procdesc.SystemValue; } }

        public int Order { get { return _procdesc.Order; } }

        #endregion
    }
}
