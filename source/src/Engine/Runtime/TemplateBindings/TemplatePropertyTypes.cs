﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.TemplateBindings
{
    public enum TemplatePropertyTypes
    {
        Boolean,
        Byte,
        Integer,
        Float,
        String,
        Unkown
    }
}
