﻿using Pandora.Game.Templates;
using Pandora.Game.Templates.Configuration;
using Pandora.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.TemplateBindings
{
    public sealed class TemplateBindingHandler
    {
        private Dictionary<Type, TemplateBinding> _bindings = new Dictionary<Type, TemplateBinding>();

        public TemplateBindingHandler()
        { }

        public TemplateBinding GetChildTemplateBinding(TemplateBinding masterbinding)
        {
            return GetTemplateBinding(masterbinding.UpgradeTemplateType);
        }

        public void ScanTemplateBindings(Assembly asm)
        {
            var foundtypes = from t in asm.GetTypes()
                             let tempdesc = AttributeUtility.GetAttribute<TemplateAttribute>(t)
                             where tempdesc != null
                             select t;

            var havetypes = _bindings.Keys.ToArray();
            var excepts = foundtypes.Except(havetypes);

            foreach (var item in excepts)
            {
                _bindings.Add(item, new TemplateBinding(item));
                Logger.Debug("Add template type '{0}' with {1} bindings", item.Name, _bindings[item].Properties.Count());
            }

            if (BindingCount == 0)
                Logger.Warning("No templates found");
        }

        public bool ContainsTemplateType(Type type)
        {
            return _bindings.ContainsKey(type);
        }

        public bool CreateTemplateBinding(Type type)
        {
            _bindings.Add(type, new TemplateBinding(type));

            return true;
        }

        public IEnumerable<TemplateBinding> GetTemplateBindings()
        {
            return _bindings.Values;
        }

        public TemplateBinding GetTemplateBinding<TTemplate>(TTemplate template)
            where TTemplate : TemplateBase
        {
            return _bindings[typeof(TTemplate)];
        }

        public TemplateBinding GetTemplateBinding(Type templatebinding)
        {
            return _bindings[templatebinding];
        }

        public TemplateBinding GetTemplateBinding(string type)
        {
            var result = _bindings.Values.Where(m => m.TemplateType.FullName == type);

            return result.FirstOrDefault();
        }

        public int BindingCount { get { return _bindings.Count(); } }
    }
}
