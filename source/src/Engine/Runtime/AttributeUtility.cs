﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime
{
    internal class AttributeUtility
    {
        public static TAttribute GetAttribute<TAttribute>(MemberInfo m)
            where TAttribute : Attribute
        {
            return m.GetCustomAttributes(typeof(TAttribute), false).Cast<TAttribute>().FirstOrDefault();
        }

        public static IEnumerable< TAttribute> GetAttributes<TAttribute>(MemberInfo m)
            where TAttribute : Attribute
        {
            return m.GetCustomAttributes(typeof(TAttribute), false).Cast<TAttribute>().ToArray();
        }
    }
}
