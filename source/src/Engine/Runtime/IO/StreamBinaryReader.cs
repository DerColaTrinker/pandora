﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.IO
{
    public sealed class StreamBinaryReader : IReader
    {
        private static readonly Encoding __encoder = Encoding.UTF8;

        private byte[] _buffer = new byte[16];

        public StreamBinaryReader(Stream stream)
        {
            BaseStream = stream;
        }

        private void FillBuffer(int length)
        {
            if (BaseStream.Read(_buffer, 0, length) <= 0) throw new EndOfStreamException();
        }

        public Stream BaseStream { get; private set; }

        #region IReader Member

        public bool ReadBool()
        {
            var value = BaseStream.ReadByte();
            if (value == -1) throw new EndOfStreamException();

            return value > 0;
        }

        public byte ReadByte()
        {
            var value = BaseStream.ReadByte();
            if (value == -1) throw new EndOfStreamException();

            return (byte)value;
        }

        public sbyte ReadSByte()
        {
            var value = BaseStream.ReadByte();
            if (value == -1) throw new EndOfStreamException();

            return (sbyte)value;
        }

        public void ReadBytes(ref byte[] result)
        {
            var length = BaseStream.Read(result, 0, result.Length);
            if (length == 0) throw new EndOfStreamException();
        }

        public byte[] ReadBytes(int length)
        {
            var buffer = new byte[length];
            var rlen = BaseStream.Read(buffer, 0, length);
            if (rlen == 0) throw new EndOfStreamException();

            return buffer;
        }

        public short ReadInt16()
        {
            FillBuffer(2);
            return BitConverter.ToInt16(_buffer, 0);
        }

        public int ReadInt32()
        {
            FillBuffer(4);
            return BitConverter.ToInt32(_buffer, 0);
        }

        public long ReadInt64()
        {
            FillBuffer(8);
            return BitConverter.ToInt64(_buffer, 0);
        }

        public ushort ReadUInt16()
        {
            FillBuffer(2);
            return BitConverter.ToUInt16(_buffer, 0);
        }

        public uint ReadUInt32()
        {
            FillBuffer(4);
            return BitConverter.ToUInt32(_buffer, 0);
        }

        public ulong ReadUInt64()
        {
            FillBuffer(4);
            return BitConverter.ToUInt64(_buffer, 0);
        }

        public double ReadDouble()
        {
            FillBuffer(8);
            return BitConverter.ToDouble(_buffer, 0);
        }

        public float ReadFloat()
        {
            FillBuffer(4);
            return BitConverter.ToSingle(_buffer, 0);
        }

        public string ReadString()
        {
            var length = ReadInt32();
            var buffer = new byte[length];

            ReadBytes(ref buffer);

            return __encoder.GetString(buffer);
        }

        public DateTime ReadDateTime()
        {
            return DateTime.FromBinary(ReadInt64());
        }

        public T ReadEnum<T>()
        {
            var type = typeof(T);
            var value = (object)null;

            if (type.IsEnum)
            {
                var basetype = Enum.GetUnderlyingType(type);
                switch (basetype.Name)
                {
                    case "Int16": value = ReadInt16(); break;
                    case "Int32": value = ReadInt32(); break;
                    case "Int64": value = ReadInt64(); break;
                    case "UInt16": value = ReadUInt16(); break;
                    case "UInt32": value = ReadUInt32(); break;
                    case "UInt64": value = ReadUInt64(); break;
                    case "Byte": value = ReadByte(); break;
                    case "SByte": value = ReadSByte(); break;
                    default: throw new InvalidCastException(string.Format("Enum {0} could not cast to {1}", type.Name, basetype.Name));
                }

                return (T)Enum.Parse(type, value.ToString());
            }
            else
            {
                throw new InvalidCastException("Failed to write binary value.");
            }
        }

        #endregion
    }
}
