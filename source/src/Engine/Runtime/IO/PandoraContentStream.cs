﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.IO
{
    public class PandoraContentStream : Stream
    {
        private Stream _basestream;

        public PandoraContentStream(string file)
            : this(new FileInfo(file))
        { }

        public PandoraContentStream(FileInfo file)
            : this(file.OpenRead())
        {
            ContentType = PandoraContentStreamTypes.Source;
            File = file;
        }

        public PandoraContentStream(Stream stream)
        {
            _basestream = stream;
            ContentType = PandoraContentStreamTypes.Stream;
        }

        public FileInfo File { get; private set; }

        public PandoraContentStreamTypes ContentType { get; private set; }

        #region Stream - Erben

        public override bool CanRead
        {
            get { return _basestream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return _basestream.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return _basestream.CanWrite; }
        }

        public override void Flush()
        {
            _basestream.Flush();
        }

        public override long Length
        {
            get { return _basestream.Length; }
        }

        public override long Position
        {
            get
            {
             return   _basestream.Position;
            }
            set
            {
                _basestream.Position = value;
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _basestream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _basestream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _basestream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _basestream.Write(buffer, offset, count);
        }

        public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
        {
            return _basestream.BeginRead(buffer, offset, count, callback, state);
        }

        public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
        {
            return _basestream.BeginWrite(buffer, offset, count, callback, state);
        }

        public override bool CanTimeout
        {
            get
            {
                return _basestream.CanTimeout;
            }
        }

        public override void Close()
        {
            _basestream.Close();
        }

        public override Task CopyToAsync(Stream destination, int bufferSize, System.Threading.CancellationToken cancellationToken)
        {
            return _basestream.CopyToAsync(destination, bufferSize, cancellationToken);
        }

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return _basestream.CreateObjRef(requestedType);
        }

        public override int EndRead(IAsyncResult asyncResult)
        {
            return _basestream.EndRead(asyncResult);
        }

        public override void EndWrite(IAsyncResult asyncResult)
        {
            _basestream.EndWrite(asyncResult);
        }

        public override Task FlushAsync(System.Threading.CancellationToken cancellationToken)
        {
            return _basestream.FlushAsync(cancellationToken);
        }

        public override Task<int> ReadAsync(byte[] buffer, int offset, int count, System.Threading.CancellationToken cancellationToken)
        {
            return _basestream.ReadAsync(buffer, offset, count, cancellationToken);
        }

        public override int ReadByte()
        {
            return _basestream.ReadByte();
        }

        public override int ReadTimeout
        {
            get
            {
                return _basestream.ReadTimeout;
            }
            set
            {
                _basestream.ReadTimeout = value;
            }
        }

        public override int WriteTimeout
        {
            get
            {
                return _basestream.WriteTimeout;
            }
            set
            {
                _basestream.WriteTimeout = value;
            }
        }

        public override Task WriteAsync(byte[] buffer, int offset, int count, System.Threading.CancellationToken cancellationToken)
        {
            return _basestream.WriteAsync(buffer, offset, count, cancellationToken);
        }

        public override void WriteByte(byte value)
        {
            _basestream.WriteByte(value);
        }
     
        #endregion
    }
}
