﻿using System;

namespace Pandora.Runtime.SFML
{
    /// <summary>
    /// Eine Schnittstelle die ein internen Zeiger bereitstellt
    /// </summary>
    public interface IPointerObject
    {
        /// <summary>
        /// Liefert den internen Zeiger
        /// </summary>
        IntPtr Pointer { get; }
    }
}
