﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.SFML
{
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    internal delegate uint GetPointCountCallbackType(IntPtr UserData);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    internal delegate Vector2f GetPointCallbackType(uint index, IntPtr UserData);
}
