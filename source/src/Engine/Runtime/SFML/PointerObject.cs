using System;
using System.Runtime.InteropServices;

namespace Pandora.Runtime.SFML
{
    /// <summary>
    /// Basisklasse die einen internen Zeiger verwaltet
    /// </summary>
    public abstract class PointerObject : IDisposable, IPointerObject
    {
        /// <summary>
        /// Finalisiert das Object
        /// </summary>
        ~PointerObject()
        {
            Dispose(false);
        }

        /// <summary>
        /// F�hrt dazu das der Zeiger und die Resource f�r die er steht zerst�rt wird
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!Disposed)
            {
                Destroy(disposing);
                InternalPointer = IntPtr.Zero;
                Disposed = true;
            }
        }

        /// <summary>
        /// Liefert den Pointer oder legt ihn fest
        /// </summary>
        /// <remarks>Diese Eigenschaft sollte nur verwendet werden, wenn ein Zeiger erst erstellt oder ge�ndert wird, wenn die Instanz bereits erstellt wurde.</remarks>
        internal protected IntPtr InternalPointer { get; set; }

        /// <summary>
        /// Liefert den internen Zeiger
        /// </summary>
        public IntPtr Pointer { get { return InternalPointer; } }

        /// <summary>
        /// Wird aufgerufen wenn der Zeiger zerst�rt werden muss
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Destroy(bool disposing)
        {
            InternalPointer = IntPtr.Zero;
        }

        /// <summary>
        /// Gibt an ob der Zeiger bereits zerst�rt wurde.
        /// </summary>
        public bool Disposed { get; private set; }
    }
}
