﻿using System;
using System.Security;
using System.Runtime.InteropServices;

namespace Pandora.Runtime.SFML
{
    /// <summary>
    /// Ein Zeitobjekt das die Vergangene Zeit enthält
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Frame : IEquatable<Frame>
    {
        /// <summary>
        /// Ein Zeitobjekt das keine vergangene Zeit beinhaltet
        /// </summary>
        public static readonly Frame Zero = FromMicroseconds(0);

        /// <summary>
        /// Erstellt ein <see cref="Frame"/> Objekt anhand der angegebenen Sekunden
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static Frame FromSeconds(float seconds)
        {
            return NativeSFMLCalls.sfSeconds(seconds);
        }

        /// <summary>
        /// Erstellt ein <see cref="Frame"/> Objekt anhand der angegebenen Millisekunden
        /// </summary>
        /// <param name="milliseconds"></param>
        /// <returns></returns>
        public static Frame FromMilliseconds(int milliseconds)
        {
            return NativeSFMLCalls.sfMilliseconds(milliseconds);
        }

        /// <summary>
        /// Erstellt ein <see cref="Frame"/> Objekt anhand der angegebenen Microsekunden
        /// </summary>
        /// <param name="microseconds"></param>
        /// <returns></returns>
        public static Frame FromMicroseconds(long microseconds)
        {
            return NativeSFMLCalls.sfMicroseconds(microseconds);
        }

        /// <summary>
        /// Gibt die vergangene Zeit in Sekunden zurück
        /// </summary>
        /// <returns></returns>
        public float AsSeconds()
        {
            return NativeSFMLCalls.sfTime_asSeconds(this);
        }

        /// <summary>
        /// Gibt die vergangene Zeit in Millisekunden zurück
        /// </summary>
        /// <returns></returns>
        public int AsMilliseconds()
        {
            return NativeSFMLCalls.sfTime_asMilliseconds(this);
        }

        /// <summary>
        /// Gibt die vergangene Zeit in Microsekunden zurück
        /// </summary>
        /// <returns></returns>
        public long AsMicroseconds()
        {
            return NativeSFMLCalls.sfTime_asMicroseconds(this);
        }

        /// <summary>
        /// Vergleicht zwei Zeitwerte
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Frame left, Frame right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Ungleich zwei Zeitwerte
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(Frame left, Frame right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Vergleicht zwei Zeitwerte
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (obj is Frame) && obj.Equals(this);
        }

        /// <summary>
        /// Vergleicht zwei Zeitwerte
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public bool Equals(Frame time)
        {
            return _microseconds == time._microseconds;
        }

        /// <summary>
        /// Kleiner als
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator <(Frame left, Frame right)
        {
            return left.AsMicroseconds() < right.AsMicroseconds();
        }

        /// <summary>
        /// Kleiner gleich 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator <=(Frame left, Frame right)
        {
            return left.AsMicroseconds() <= right.AsMicroseconds();
        }

        /// <summary>
        /// Größer als
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator >(Frame left, Frame right)
        {
            return left.AsMicroseconds() > right.AsMicroseconds();
        }

        /// <summary>
        /// Größer gleich 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator >=(Frame left, Frame right)
        {
            return left.AsMicroseconds() >= right.AsMicroseconds();
        }

        /// <summary>
        /// Subtrahiert
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator -(Frame left, Frame right)
        {
            return FromMicroseconds(left.AsMicroseconds() - right.AsMicroseconds());
        }

        /// <summary>
        /// Addiert
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator +(Frame left, Frame right)
        {
            return FromMicroseconds(left.AsMicroseconds() + right.AsMicroseconds());
        }

        /// <summary>
        /// Multipliziert
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator *(Frame left, float right)
        {
            return FromSeconds(left.AsSeconds() * right);
        }

        /// <summary>
        /// Dividiert
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator *(Frame left, long right)
        {
            return FromMicroseconds(left.AsMicroseconds() * right);
        }

        /// <summary>
        /// MUltipliziert
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator *(float left, Frame right)
        {
            return FromSeconds(left * right.AsSeconds());
        }

        /// <summary>
        /// Multipliziert
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator *(long left, Frame right)
        {
            return FromMicroseconds(left * right.AsMicroseconds());
        }

        /// <summary>
        /// Dividiert
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator /(Frame left, Frame right)
        {
            return FromMicroseconds(left.AsMicroseconds() / right.AsMicroseconds());
        }

        /// <summary>
        /// Dividiert
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator /(Frame left, float right)
        {
            return FromSeconds(left.AsSeconds() / right);
        }

        /// <summary>
        /// Dividiert
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator /(Frame left, long right)
        {
            return FromMicroseconds(left.AsMicroseconds() / right);
        }

        /// <summary>
        /// Rest division
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Frame operator %(Frame left, Frame right)
        {
            return FromMicroseconds(left.AsMicroseconds() % right.AsMicroseconds());
        }

        /// <summary>
        /// Liefert einen eindeutigen Hashcode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return _microseconds.GetHashCode();
        }

        private long _microseconds;
    }
}
