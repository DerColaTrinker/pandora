﻿namespace Pandora.Runtime.ComponentModel
{
    /// <summary>
    /// Stellt eine Schnittstelle für das PropertyChange System bereit
    /// </summary>
    public interface IPropertyChanged
    {
        /// <summary>
        /// Wird ausgelöst nachdem der Wert einer Eigenschaft geändert wurde
        /// </summary>
        event PropertyChangedDelegate PropertyChanged;

        /// <summary>
        /// Wird ausgelöst bevor der Wert einer Eigenschaft geändert wird
        /// </summary>
        event PropertyChangingDelegate PropertyChanging;
    }
}
