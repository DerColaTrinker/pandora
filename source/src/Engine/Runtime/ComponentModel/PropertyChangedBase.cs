﻿namespace Pandora.Runtime.ComponentModel
{
    /// <summary>
    /// Stellt ein Delegate dar, das auslöst wenn bevor eine Eigenschaft geändert wird
    /// </summary>
    /// <param name="obj">Das Objekt in dem die Eigenschaft eingebettet ist</param>
    /// <param name="propertyname">Der Name der Eingeschaft</param>
    /// <param name="oldvalue">Der aktuelle Wert der Eigenschaft</param>
    /// <param name="newvalue">Der neue Wert der Eigenschaft</param>
    public delegate void PropertyChangingDelegate(object obj, string propertyname, object oldvalue, object newvalue);

    /// <summary>
    /// Stellt ein Delegate dar, das auslöst nachdem eine Eingenschaft geändert wurde
    /// </summary>
    /// <param name="obj">Das Objekt in dem die Eigenschaft eingebettet ist</param>
    /// <param name="propertyname">Der Name der Eingeschaft</param>
    /// <param name="value">Der Aktuelle Wert der Eigenschaft</param>
    public delegate void PropertyChangedDelegate(object obj, string propertyname, object value);

    /// <summary>
    /// Stellt eine Basis für die Eigenschaftsänderung dar
    /// </summary>
    public abstract class PropertyChangedBase : IPropertyChanged
    {
        /// <summary>
        /// Wird aufgerufen bevor eine Eigenschaft geändert wird
        /// </summary>
        public event PropertyChangingDelegate PropertyChanging;

        /// <summary>
        /// Wird aufgerufen nachdem eine Eigenschaft geändert wurde
        /// </summary>
        public event PropertyChangedDelegate PropertyChanged;

        /// <summary>
        /// Löscht das PropertyChanging-Event aus
        /// </summary>
        /// <param name="obj">Das Objekt in dem die Eigenschaft eingebettet ist</param>
        /// <param name="propertyname">Der Name der Eingeschaft</param>
        /// <param name="oldvalue">Der aktuelle Wert der Eigenschaft</param>
        /// <param name="newvalue">Der neue Wert der Eigenschaft</param>
        protected virtual void OnPropertyChanging(object obj, string propertyname, object oldvalue, object newvalue)
        {
            if(PropertyChanging != null) PropertyChanging.Invoke(obj, propertyname, oldvalue, newvalue);
        }

        /// <summary>
        /// Löst das PropertyChanged-Event aus
        /// </summary>
        /// <param name="obj">Das Objekt in dem die Eigenschaft eingebettet ist</param>
        /// <param name="propertyname">Der Name der Eingeschaft</param>
        /// <param name="value">Der Aktuelle Wert der Eigenschaft</param>
        protected virtual void OnPropertyChanged(object obj, string propertyname, object value)
        {
            if(PropertyChanged != null) PropertyChanged.Invoke(obj, propertyname, value);
        }
    }
}
