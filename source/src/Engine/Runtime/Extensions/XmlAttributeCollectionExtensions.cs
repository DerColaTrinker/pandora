﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Pandora.Runtime.Extensions
{
    public static class XmlAttributeCollectionExtensions
    {
        public static T GetValue<T>(this XmlAttributeCollection collection, string name, T defaultvalue)
        {
            try
            {
                return (T)Convert.ChangeType(GetValue(collection, name), typeof(T));
            }
            catch (Exception)
            {
                return defaultvalue;
            }
        }

        public static object GetValue(this XmlAttributeCollection collection, string name)
        {
            try
            {
                return collection[name].Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
