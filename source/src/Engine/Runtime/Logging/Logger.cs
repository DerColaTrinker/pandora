﻿using Pandora.Runtime.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable 1591

namespace Pandora
{
    public static class Logger
    {
        public static void Trace(string message, params object[] args)
        {
            LoggerManager.LogMessage(LoggerMessageType.Trace, message, args);
        }

        public static void Debug(string message, params object[] args)
        {
            LoggerManager.LogMessage(LoggerMessageType.Debug, message, args);
        }

        public static void Normal(string message, params object[] args)
        {
            LoggerManager.LogMessage(LoggerMessageType.Normal, message, args);
        }

        public static void Warning(string message, params object[] args)
        {
            LoggerManager.LogMessage(LoggerMessageType.Warning, message, args);
        }

        public static void Error(string message, params object[] args)
        {
            LoggerManager.LogMessage(LoggerMessageType.Error, message, args);
        }

        public static void Exception(Exception ex, [CallerMemberName] string membername = "", [CallerLineNumber] int linenumber = 0, [CallerFilePath] string file = "")
        {
            LoggerManager.LogException(ex, membername, linenumber, file);
        }

        public static void BeginProgress(float count, string message, params object[] args)
        {
            LoggerManager.BeginProgress(count, message, args);
        }

        public static void UpdateProgress(int count)
        {
            LoggerManager.UpdateProgress(count);
        }

        public static void EndProgress(bool abort)
        {
            LoggerManager.EndProgress(abort);
        }

        public static void BeginState(string message, params object[] args)
        {
            LoggerManager.BeginState(message);
        }

        public static void EndState(LoggerStateResult result)
        {
            LoggerManager.EndState(result);
        }
    }
}
