﻿using Pandora.Game.Communications;
using Pandora.Game.Communications.PCK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Verwaltet die in der Engine verwendeten Netzwerk-Pakete
    /// </summary>
    public static class Packets
    {
        static Packets()
        {
            InQueue = new PacketQueue<PacketIn>();
            OutStack = new PacketStack<PacketOut>(200);
            InStack = new PacketStack<PacketIn>(200);
        }

        #region IN

        internal static PacketQueue<PacketIn> InQueue { get; private set; }

        internal static PacketStack<PacketIn> InStack { get; private set; }

        internal static void ReleasePacket(PacketIn pin)
        {
            InStack.ReleasePacket(pin);
        }

        internal static PacketIn GetPacketIn()
        {
            var packet = InStack.GetPacket();

            //((PacketIn)packet).Clear(); // Ein Clear ist nicht nötig, wird eh gemacht.

            return packet;
        }

        #endregion

        #region OUT

        internal static PacketStack<PacketOut> OutStack { get; private set; }

        /// <summary>
        /// Liefert ein ausgehendes Datenpaket aus dem Pool
        /// </summary>
        /// <returns></returns>
        public static IPacketOut GetPacket()
        {
            var packet = OutStack.GetPacket();

            ((PacketOut)packet).Clear();

            return packet;
        }

        /// <summary>
        /// Legt ein ausgehendes Datenpaket zurück in den Paketpool. Es wird nicht versendet.
        /// </summary>
        /// <param name="pout"></param>
        public static void ReleasePacket(IPacketOut pout)
        {
            OutStack.ReleasePacket((PacketOut)pout);
        }

        #endregion
    }
}
