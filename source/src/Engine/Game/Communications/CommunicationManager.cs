﻿using Pandora.Game.Communications.PCK;
using Pandora.Game.Communications.Handler;
using Pandora.Game.Communications.RPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Communications.TCP;

namespace Pandora.Game.Communications
{
    public sealed class CommunicationManager
    {
        internal CommunicationManager(PandoraEngine engine)
        {
            Engine = engine;

            Logger.Trace("CommunicationManager created");
        }

        public PandoraEngine Engine { get; private set; }

        internal bool Initialize()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                Logger.Error("Network is not available");
                return false;
            }

            RPC = new RPCTargetHandler(this);

            CommunicationMode = CommunicationModes.Stopped;

            Logger.Normal("CommunicationManager initialized");
            return true;
        }

        #region Kommunikationsystem Steuerung

        public RPCTargetHandler RPC { get; private set; }

        public CommunicationHandlerBase Module { get; private set; }

        public CommunicationModes CommunicationMode { get; private set; }

        public bool IsRunning { get; private set; }

        public bool StartServer()
        {
            if (IsRunning & Module != null) return false;

            Module = new ServerHandler(this);
            CommunicationMode = CommunicationModes.Server;
            IsRunning = true;
            Logger.Normal("Multiplayer server started");

            return true;
        }

        public bool StartClient()
        {
            if (IsRunning & Module != null) return false;

            Module = new ClientHandler(this);
            CommunicationMode = CommunicationModes.Server;
            IsRunning = true;
            Logger.Normal("Multiplayer client started");

            return true;
        }

        public bool CloseServer()
        {
            if (Module != null && CommunicationMode == CommunicationModes.Server)
            {
                Module.Stop();
                Module.DisconnectAllClients();

                return true;
            }

            return false;
        }

        public void Stop()
        {
            if (Module != null) Module.DisconnectAllClients();

            switch (CommunicationMode)
            {
                case CommunicationModes.Stopped:
                    return;

                case CommunicationModes.Client:
                case CommunicationModes.Server:
                    Module = null;
                    IsRunning = false;
                    break;
            }

            CommunicationMode = CommunicationModes.Stopped;
        }
             
        #endregion
    }
}
