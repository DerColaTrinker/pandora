﻿using Pandora.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Communications.PCK
{
    /// <summary>
    /// Diese Basisklasse stellt Datenpaket dar
    /// </summary>
    internal abstract class Packet : IPacket
    {
        private byte[] _buffer;

        internal Packet()
            : this(4096)
        { }

        internal Packet(int buffersize)
        {
            _buffer = new byte[buffersize];
        }

        /// <summary>
        /// Ermöglicht direkten Zugriff auf das Byte Buffer Array
        /// </summary>
        protected internal byte[] PackageBuffer { get { return _buffer; } }

        /// <summary>
        /// Liefert die aktuelle Position im Datenpaket
        /// </summary>
        public virtual int Position { get; internal set; }

        /// <summary>
        /// Liefert die Länge des Datenpaket
        /// </summary>
        public virtual int Length { get; internal set; }

        /// <summary>
        /// Liefert den Client
        /// </summary>
        public IClient Client { get; private set; }

        internal void CheckEndOfLength(int length)
        {
            if ((Position) >= Length) throw new IndexOutOfRangeException("Packet");
        }

        internal void CheckEndOfBuffer(int length)
        {
            if ((Position + length) >= PackageBuffer.Length) throw new IndexOutOfRangeException("PackageBuffer");
        }

        internal void Clear()
        {
            Position = 0;
            Length = 0;
            Client = null;
        }

        internal void CleanUp(IClient client, byte[] src, int length)
        {
            Client = client;
            Buffer.BlockCopy(src, 0, PackageBuffer, 0, length);
            Position = 0;
            Length = length;
        }

        internal byte[] ToArray()
        {
            return _buffer;
        }
    }
}
