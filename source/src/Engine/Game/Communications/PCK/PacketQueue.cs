﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Communications.PCK
{
    /// <summary>
    /// Stellt eine Datenpaket-Warteschlange dar
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class PacketQueue<T> where T : Packet
    {
        private Queue<T> _queue = new Queue<T>(500);

        internal PacketQueue()
        { }

        internal void Push(T inpacket)
        {
            lock (_queue)
            {
                _queue.Enqueue(inpacket);
            }
        }

        internal T Pop()
        {
            lock (_queue)
            {
                return _queue.Dequeue();
            }
        }

        /// <summary>
        /// Liefert die Anzahl der Datenpakete die sich in der Warteschlange befinden
        /// </summary>
        public int Count { get { return _queue.Count; } }
    }
}
