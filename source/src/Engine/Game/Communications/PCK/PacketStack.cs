﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Communications.PCK
{
    /// <summary>
    /// Stellt ein Datenpaket Pool dar
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class PacketStack<T> where T : Packet
    {
        private Stack<T> _stack = new Stack<T>();

        internal PacketStack(int capacity)
        {
            for (int i = 0 ; i < capacity ; i++)
            {
                _stack.Push((T)Activator.CreateInstance(typeof(T), true));
            }
        }

        internal T GetPacket()
        {
            lock (_stack)
            {
                if (_stack.Count == 0)
                    return (T)Activator.CreateInstance(typeof(T), true);
                else
                    return _stack.Pop();
            }
        }

        internal void ReleasePacket(T pin)
        {
            lock (_stack)
            {
                _stack.Push(pin);
            }
        }
    }
}
