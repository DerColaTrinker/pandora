﻿using Pandora.Game.Communications;
using Pandora.Game.Communications.TCP;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt einen Client dar
    /// </summary>
    public interface IClient
    {
        /// <summary>
        /// Trennt die Verbindung
        /// </summary>
        void Close();

        /// <summary>
        /// Liefert die IP-Adresse und Port des Ziel Endpunkts
        /// </summary>
        string RemoteAddress { get; }

        /// <summary>
        /// Gibt an ob die Verbindung besteht
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Liefert eine eindeutige GUID der Verbindung
        /// </summary>
        string GUID { get; }

        ClientAuthentificationStatus AuthentificationStatus { get; }
    }
}