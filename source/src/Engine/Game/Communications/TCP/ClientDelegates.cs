﻿using Pandora.Game.Communications.PCK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Communications.TCP
{
    /// <summary>
    /// Klasse die interne Ereignisse für die Verbindungen bereit stellt
    /// </summary>
    public class ClientDelegates
    {
        internal event ClientDisconnectDelegate ClientDisconnect;
        internal event ClientReceiveDelegate ClientReceive;
        internal event ClientSendDelegate ClientSend;

        internal virtual void OnClientDisconnect(IClient client, BySide side)
        {
            if (ClientDisconnect != null) ClientDisconnect(client, side);
        }

        internal virtual void OnClientReceive(IClient client, byte[] buffer, int length)
        {
            if (ClientReceive != null) ClientReceive(client, buffer, length);
        }

        internal virtual void OnClientSend(IClient client, PacketOut pout)
        {
            if (ClientSend != null) ClientSend(client, pout);
        }
    }
}
