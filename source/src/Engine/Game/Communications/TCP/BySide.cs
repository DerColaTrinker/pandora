﻿namespace Pandora.Game.Communications.TCP
{
    /// <summary>
    /// Gibt an von welcher Gegenstelle die Verbindung getrennt wurde
    /// </summary>
    public enum BySide
    {
        /// <summary>
        /// Server
        /// </summary>
        Server,

        /// <summary>
        /// Client
        /// </summary>
        Client,

        /// <summary>
        /// Fehler
        /// </summary>
        Error
    }
}