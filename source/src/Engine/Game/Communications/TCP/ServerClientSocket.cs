﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Pandora.Game.Communications;
using Pandora.Game.Communications.PCK;

namespace Pandora.Game.Communications.TCP
{
    internal class ServerClientSocket : ClientDelegates, IClient
    {
        public const int CONST_RECVBUFFERLENGTH = 4096;

        private Socket _socket;
        private byte[] _recvbuffer = new byte[CONST_RECVBUFFERLENGTH];

        internal ServerClientSocket(Socket socket)
        {
            _socket = socket;
            _socket.NoDelay = true;

            AuthentificationStatus = ClientAuthentificationStatus.Unkown;
            GUID = Guid.NewGuid().ToString();

            _socket.BeginReceive(_recvbuffer, 0, CONST_RECVBUFFERLENGTH, SocketFlags.None, InternalReceive, null);

            RemoteAddress = ((IPEndPoint)_socket.RemoteEndPoint).ToString();

            Logger.Trace("ServerClientSocket created");
        }

        public void Close()
        {
            if (_socket != null && _socket.Connected)
            {
                _socket.BeginDisconnect(false, InternalDisconnect, BySide.Server);
            }
        }

        public void Send(IPacketOut packet)
        {
            if (_socket != null && _socket.Connected)
            {
                _socket.Send(((PacketOut)packet).PackageBuffer, packet.Position, SocketFlags.None);
                //OnClientSend(this, (PacketOut)packet);
            }
        }

        private void InternalReceive(IAsyncResult ar)
        {
            var length = 0;

            if (_socket == null) return;

            try
            {
                length = _socket.EndReceive(ar);
            }
            catch (SocketException ex)
            {
                if (ex.ErrorCode == 10054)
                {
                    OnClientDisconnect(this, BySide.Client);
                }
                else
                {
                    Logger.Exception(ex);
                    OnClientDisconnect(this, BySide.Error);
                }
                return;
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                OnClientDisconnect(this, BySide.Error);
                return;
            }

            Logger.Trace("Server receive {0}bytes from '{1}'", length, RemoteAddress);

            if (length > 0)
            {
                // Das event auslösen
                OnClientReceive(this, _recvbuffer, length);

                if (_socket != null && _socket.Connected)
                    _socket.BeginReceive(_recvbuffer, 0, CONST_RECVBUFFERLENGTH, SocketFlags.None, InternalReceive, null);
            }
            else
            {
                OnClientDisconnect(this, BySide.Client);
            }
        }

        private void InternalDisconnect(IAsyncResult ar)
        {
            _socket.EndDisconnect(ar);

            OnClientDisconnect(this, (BySide)ar.AsyncState);

            _socket.Close();
            _socket = null;
        }

        public string RemoteAddress { get; private set; }

        public ClientAuthentificationStatus AuthentificationStatus { get; internal set; }

        public bool IsConnected { get { return _socket != null && _socket.Connected; } }

        public string GUID { get; private set; }
    }
}
