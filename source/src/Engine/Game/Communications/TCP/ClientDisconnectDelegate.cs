﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Communications.TCP
{
    internal delegate void ClientDisconnectDelegate(IClient client, BySide side);
}
