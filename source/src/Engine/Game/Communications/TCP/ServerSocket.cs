﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using Pandora.Runtime.Logging;

namespace Pandora.Game.Communications.TCP
{
    internal sealed class ServerSocket
    {
        private IPEndPoint _lipe;
        private Socket _socket;

        internal event ClientConnectDelegate ClientConnected;

        public ServerSocket()
        {
            Logger.Trace("ServerSocket created");
        }

        public bool Initialize(string ipaddress, int port)
        {
            if (!CheckNetwork()) return false;
            if (!CheckLIPE(ipaddress, port)) return false;

            Logger.Normal("Server listen initialized");

            return true;
        }

        public bool Start()
        {
            if (_socket != null) return true;
            if (_lipe == null)
            {
                Logger.Error("Server not initialised");
                return false;
            }

            _socket = new Socket(_lipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            _socket.NoDelay = true;

            if (!SocketBinding()) return false;
            if (!SocketListen()) return false;

            Logger.Normal("Server start listening...");
            return true;
        }

        public bool Stop()
        {
            if (_socket == null) return false;
            _socket.Close();
            _socket = null;

            Logger.Normal("Server stop listening...");
            return true;
        }

        private void InternalAccept(IAsyncResult ar)
        {
            if (_socket == null) return;

            var socket = _socket.EndAccept(ar);
            if (socket == null) return;

            Logger.Trace("Server client connecting '{0}'", ((IPEndPoint)socket.RemoteEndPoint).ToString());

            var client = new ServerClientSocket(socket);

            if (ClientConnected != null) ClientConnected.Invoke(client);

            _socket.BeginAccept(InternalAccept, null);
        }

        private bool CheckNetwork()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                Logger.Error("Server network not available");
                return false;
            }

            Logger.BeginState("IPv6 support available");
            if (Socket.OSSupportsIPv6)
                Logger.EndState(LoggerStateResult.Yes);
            else
                Logger.EndState(LoggerStateResult.No);

            return true;
        }

        private bool CheckLIPE(string ipaddress, int port)
        {
            var address = (IPAddress)null;

            if (!IPAddress.TryParse(ipaddress, out address))
            {
                Logger.Error("Server invalid listen ip address '{0}'", ipaddress);
                return false;
            }

            if (port < 1024 | port > UInt16.MaxValue)
            {
                Logger.Error("Server invalid listen port '{0}'", port);
                return false;
            }

            _lipe = new IPEndPoint(address, port);
            Logger.Debug("Server listen point '{0}'", _lipe.ToString());

            return true;
        }

        private bool SocketBinding()
        {
            try
            {
                _socket.Bind(_lipe);

                Logger.Debug("Server socket bind to '{0}'", _lipe.ToString());

                // Wenn der Socket eine IPv6 Adresse ist, diese ebenfalls für IPv4 aktivieren
                if (_lipe.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    _socket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.IPv6Only, false);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
        }

        private bool SocketListen()
        {
            try
            {
                _socket.Listen(200);
                _socket.BeginAccept(InternalAccept, null);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
        }
    }
}
