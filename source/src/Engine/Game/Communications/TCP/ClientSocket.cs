﻿using Pandora.Game.Communications.PCK;
using Pandora.Runtime.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Communications.TCP
{
    sealed class ClientSocket : ClientDelegates, IClient
    {
        public const int CONST_RECVBUFFERLENGTH = 4096;

        private Socket _socket;
        private IPEndPoint _ripe;
        private byte[] _recvbuffer = new byte[CONST_RECVBUFFERLENGTH];

        public ClientSocket()
        {
            GUID = Guid.NewGuid().ToString();

            Logger.Trace("ClientSocket created");
        }

        public bool Initialize(string ipaddress, int port)
        {
            if (!CheckNetwork()) return false;
            if (!CheckRIPE(ipaddress, port)) return false;

            AuthentificationStatus = ClientAuthentificationStatus.Unkown;
            RemoteAddress = _ripe.ToString();

            Logger.Normal("Client initialized");

            return true;
        }

        public bool Connect()
        {
            if (IsConnected) return true;

            _socket = new Socket(_ripe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                _socket.Connect(_ripe);

                Logger.Normal("Client conntected to '{0}'", RemoteAddress);

                InternalInitialize();

                return true;
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
        }

        internal void InternalInitialize()
        {

            _socket.BeginReceive(_recvbuffer, 0, CONST_RECVBUFFERLENGTH, SocketFlags.None, InternalReceive, null);
            _socket.NoDelay = true;

            RemoteAddress = ((IPEndPoint)_socket.RemoteEndPoint).ToString();
        }

        public void Close()
        {
            if (_socket != null && _socket.Connected)
            {
                _socket.BeginDisconnect(false, InternalDisconnect, BySide.Server);
            }
        }

        public void Send(IPacketOut packet)
        {
            if (_socket != null && _socket.Connected)
            {
                _socket.Send(((PacketOut)packet).PackageBuffer, packet.Length, SocketFlags.None);
                //OnClientSend(this, (PacketOut)packet);
            }
        }

        private void InternalReceive(IAsyncResult ar)
        {
            var length = 0;

            try
            {
                length = _socket.EndReceive(ar);
                Logger.Trace("Server receive {0}bytes from '{1}'", length, RemoteAddress);
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                OnClientDisconnect(this, BySide.Error);
            }

            if (length > 0)
            {
                // Das event auslösen
                OnClientReceive(this, _recvbuffer, length);

                if (_socket != null && _socket.Connected)
                    _socket.BeginReceive(_recvbuffer, 0, CONST_RECVBUFFERLENGTH, SocketFlags.None, InternalReceive, null);
            }
            else
            {
                OnClientDisconnect(this, BySide.Client);
            }
        }

        private void InternalDisconnect(IAsyncResult ar)
        {
            _socket.EndDisconnect(ar);

            OnClientDisconnect(this, (BySide)ar.AsyncState);

            _socket.Close();
            _socket = null;
        }

        public string RemoteAddress { get; private set; }

        public ClientAuthentificationStatus AuthentificationStatus { get; internal set; }

        public bool IsConnected { get { return _socket != null && _socket.Connected; } }

        public string GUID { get; private set; }

        private bool CheckNetwork()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                Logger.Error("Server network not available");
                return false;
            }

            Logger.BeginState("IPv6 support available");
            if (Socket.OSSupportsIPv6)
                Logger.EndState(LoggerStateResult.Yes);
            else
                Logger.EndState(LoggerStateResult.No);

            return true;
        }

        private bool CheckRIPE(string ipaddress, int port)
        {
            var address = (IPAddress)null;

            if (!IPAddress.TryParse(ipaddress, out address))
            {
                Logger.Error("Server invalid ip address '{0}'", ipaddress);
                return false;
            }

            if (port < 1024 | port > UInt16.MaxValue)
            {
                Logger.Error("Server invalid port '{0}'", port);
                return false;
            }

            _ripe = new IPEndPoint(address, port);
            Logger.Debug("Client remote ip point '{0}'", _ripe.ToString());

            return true;
        }
    }
}
