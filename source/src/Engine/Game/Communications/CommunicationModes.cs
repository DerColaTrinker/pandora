﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Communications
{
    public enum CommunicationModes
    {
        Stopped,
        Client,
        Server
    }
}
