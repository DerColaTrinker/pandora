﻿using Pandora.Game.Communications.PCK;
using Pandora.Game.Communications.TCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Communications.Handler
{
    /// <summary>
    /// Das Kommunikationsmodul für einen Client
    /// </summary>
    public class ClientHandler : CommunicationHandlerBase
    {
        private ClientSocket _client;

        internal ClientHandler(CommunicationManager manager)
        {
            Manager = manager;
            Engine = manager.Engine;

            Logger.Trace("ClientHandler created");
        }

        public CommunicationManager Manager { get; private set; }

        public PandoraEngine Engine { get; private set; }

        /// <summary>
        /// Erstellt eine Verbindung zu einem Server. Die Verbindungsdaten werden über die Konfiguration [Network].HostIP und [Network].HostPort abgefragt.
        /// </summary>
        /// <returns></returns>
        public override bool Start()
        {
            var hostip = Engine.Configuration.GetValue("Network", "HostIP", "");
            var hostport = Engine.Configuration.GetValue("Network", "HostPort", 32000);

            _client = new ClientSocket();
            if (!_client.Initialize(hostip, hostport)) return false;
            if (!_client.Connect()) return false;

            InternalClientConnected(_client);

            return true;
        }

        /// <summary>
        /// Trennt die Verbindung zum Server
        /// </summary>
        /// <returns></returns>
        public override bool Stop()
        {
            _client.Close();

            //TODO: Engine informieren, das Client getrennt ist, was eigentlich zum abbruch des Spiels führen muss...

            return true;
        }

        /// <summary>
        /// Versendet ein Datenpaket an den Server. Das Datenpaket wird danach gelöscht.
        /// </summary>
        /// <param name="pout"></param>
        public override void Send(IPacketOut pout)
        {
            _client.Send((PacketOut)pout);
            Packets.ReleasePacket(pout);
        }
    }
}
