﻿using Pandora.Game.Communications.PCK;
using Pandora.Game.Communications.TCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Communications.Handler
{
    /// <summary>
    /// Stellt eine Basisklasse für die Kommunikation als Server oder Client dar.
    /// </summary>
    public abstract class CommunicationHandlerBase
    {
        private HashSet<IClient> _connections = new HashSet<IClient>();

        /// <summary>
        /// Startet den Client- oder Serverbetrieb
        /// </summary>
        /// <remarks>Muss in der abgeleiteten Klasse überschrieben werden.</remarks>
        public abstract bool Start();

        /// <summary>
        /// Beendet den Client- oder Serverbetrieb
        /// </summary>
        /// <remarks>Muss in der abgeleiteten Klasse überschrieben werden.</remarks>
        public abstract bool Stop();

        #region Client Event Handling

        /// <summary>
        /// Verbindet den Client intern mit allen Eventsystemen
        /// </summary>
        /// <param name="client"></param>
        protected virtual void InternalClientConnected(IClient client)
        {
            ((ClientDelegates)client).ClientDisconnect += InternalClientDisconnect;
            ((ClientDelegates)client).ClientReceive += InternalClientReceive;
            ((ClientDelegates)client).ClientSend += InternalClientSend;

            Logger.Normal("Client '{0}' conneted", client.RemoteAddress);
            _connections.Add(client);
        }

        private void InternalClientSend(IClient client, PacketOut packet)
        {
            Packets.OutStack.ReleasePacket(packet);

            Logger.Trace("Client '{0}' send {1} bytes", client.RemoteAddress, packet.Length);
        }

        private void InternalClientDisconnect(IClient client, BySide side)
        {
            Logger.Normal("Client '{0}' disconneted", client.RemoteAddress);
            _connections.Remove(client);

            //TOTO: Ein Player hat das Spiel verlassen?
        }

        private void InternalClientReceive(IClient client, byte[] buffer, int length)
        {
            var inpacket = Packets.InStack.GetPacket();
            inpacket.CleanUp(client, buffer, length);

            Packets.InQueue.Push(inpacket);
        }

        #endregion

        #region Connections

        /// <summary>
        /// Liefert die Anzahl der Verbundenen Clients
        /// </summary>
        /// <remarks>Im Clientbetrieb wird nur eine Verbindung angezeigt. Es handelt sich dabei um die Verbindung zum Server.</remarks>
        public int Connections { get { return _connections.Count; } }

        /// <summary>
        /// Liefert alle Clients verbundenen Clients
        /// </summary>
        /// <remarks>Im Clientbetrieb wird nur eine Verbindung zurückgegeben. Es handelt sich dabei um die Verbindung zum Server.</remarks>
        public IEnumerable<IClient> getClients { get { return _connections.ToArray(); } }

        /// <summary>
        /// Trennt alle verbundenen Clients
        /// </summary>
        public virtual void DisconnectAllClients()
        {
            foreach (var client in _connections)
            {
                client.Close();
            }
        }
        
        /// <summary>
        /// Sendet ein Broadcast an alle Clients.
        /// </summary>
        /// <param name="pout"></param>
        /// <remarks>Diese Methode ist zum überschreiben im ClientModul gedacht um schnell ein Packet an den Server zu senden. Sie löst generell ien Broadcast aus</remarks>
        public virtual void Send(IPacketOut pout)
        {
            Broadcast(pout);
        }

        /// <summary>
        /// Sendet ein Datenpaket an den angegebenen Client
        /// </summary>
        /// <param name="client"></param>
        /// <param name="pout"></param>
        public virtual void Send(IClient client, IPacketOut pout)
        {
            if (client is ServerClientSocket) ((ServerClientSocket)client).Send(pout);
            if (client is ClientSocket) ((ClientSocket)client).Send(pout);

            Packets.ReleasePacket(pout);
        }

        private void BroadcastSend(IClient client, IPacketOut pout)
        {
            if (client is ServerClientSocket) ((ServerClientSocket)client).Send(pout);
            if (client is ClientSocket) ((ClientSocket)client).Send(pout);
        }

        /// <summary>
        /// Sendet ein Datenpaket an alle verbundenen Clients
        /// </summary>
        /// <param name="pout"></param>
        public virtual void Broadcast(IPacketOut pout)
        {
            lock (_connections)
            {
                foreach (var client in _connections)
                {
                    BroadcastSend(client, pout);
                }

                Packets.ReleasePacket(pout);
            }
        }

        ///// <summary>
        ///// Sendet ein Datenpaket an alle verbundnen Clients die sich als Spieler authentifiziert haben
        ///// </summary>
        ///// <param name="pout"></param>
        //public virtual void BroadcastPlayers(IPacketOut pout)
        //{
        //    lock (_clients)
        //    {
        //        foreach (var client in _clients.Where(m => m.Player != null))
        //        {
        //            BroadcastSend(client, pout);
        //        }
        //        Packets.ReleasePacket(pout);
        //    }
        //}

        ///// <summary>
        ///// Sendet ein Datenbaket an alle verbundnen Clients die sich nicht als Spieler authentifiziert haben
        ///// </summary>
        ///// <param name="pout"></param>
        //public virtual void BroadcastNonPlayers(IPacketOut pout)
        //{
        //    lock (_clients)
        //    {
        //        foreach (var client in _clients.Where(m => m.Player == null))
        //        {
        //            BroadcastSend(client, pout);
        //        }
        //        Packets.ReleasePacket(pout);
        //    }
        //}

        #endregion
    }
}
