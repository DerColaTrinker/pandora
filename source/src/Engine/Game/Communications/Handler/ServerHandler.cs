﻿using Pandora.Game.Communications.TCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Communications.Handler
{
    /// <summary>
    /// Das Kommunikationsmodul für den Server
    /// </summary>
    public sealed class ServerHandler : CommunicationHandlerBase
    {
        private ServerSocket _server;

        internal ServerHandler(CommunicationManager manager)
        {
            Manager = manager;
            Engine = manager.Engine;

            Logger.Trace("ServerHandler created");
        }

        public CommunicationManager Manager { get; private set; }

        public PandoraEngine Engine { get; private set; }

        /// <summary>
        /// Startet den Serverbetrieb. Die Verbindungsdaten für den Listener wird über die Konfiguration [Network].ListenIP und [Network].ListenPort geladen
        /// </summary>
        /// <returns></returns>
        public override bool Start()
        {
            var listenip = Engine.Configuration.GetValue("Network", "ListenIP", "0.0.0.0");
            var listenport = Engine.Configuration.GetValue("Network", "ListenPort", 32000);

            _server = new ServerSocket();
            if (!_server.Initialize(listenip, listenport)) return false;

            _server.ClientConnected += InternalClientConnected;

            return _server.Start();
        }

        /// <summary>
        /// Beendet den Serverbetrieb, verbundene Clients bleiben erhalten.
        /// </summary>
        /// <returns></returns>
        public override bool Stop()
        {
            if (_server == null) return true;

            return _server.Stop();
        }
    }
}
