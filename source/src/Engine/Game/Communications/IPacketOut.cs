﻿using Pandora.Game.Communications.PCK;
using Pandora.Runtime.IO;
using System;

namespace Pandora.Game.Communications
{
    /// <summary>
    /// Stellt ein ausgehendes Datenpaket dar
    /// </summary>
    public interface IPacketOut : IPacket, IWriter
    {
        void WriteOpCode(OpCodes opcode);
        void WriteOpCode(uint opcode);      
    }
}
