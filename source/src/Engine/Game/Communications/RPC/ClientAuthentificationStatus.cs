﻿namespace Pandora
{
    /// <summary>
    /// Gibt den Authentifizierungsstatus Status des Clients an
    /// </summary>
    public enum ClientAuthentificationStatus : byte
    {
        /// <summary>
        /// Unbekannte Authentifizierung. Der Client hat keine Signatur übermittelt und es wurde kein Spieler generiert.
        /// </summary>
        Unkown = 0,

        /// <summary>
        /// Als Spieler Authentifiziert
        /// </summary>
        Player = 1,

        InGame = 2
    }
}