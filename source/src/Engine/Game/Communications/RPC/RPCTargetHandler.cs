﻿using Pandora.Game;
using Pandora.Game.Communications.PCK;
using Pandora.Game.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Communications.RPC
{
    /// <summary>
    /// Liste mit RPC Zielen
    /// </summary>
    public sealed class RPCTargetHandler
    {
        private Dictionary<uint, RPCTargetContainer> _targets = new Dictionary<uint, RPCTargetContainer>();

        private PandoraEngine _engine;
        private WorldManager _world;

        internal RPCTargetHandler(CommunicationManager manager)
        {
            _engine = manager.Engine;
            _world = manager.Engine.World;

            Logger.Trace("RPCTargetHandler created");
        }

        public void AddEngineTarget(OpCodes opcode, ClientAuthentificationStatus authstatus, EngineTargetDelegate target)
        {
            AddEngineTarget((uint)opcode, authstatus, target);
        }

        public void AddEngineTarget(uint opcode, ClientAuthentificationStatus authstatus, EngineTargetDelegate target)
        {
            _targets[opcode] = new RPCTargetContainer(opcode, authstatus, target);
        }

        //public void AddWorldTarget(OpCodes opcode, ClientAuthentificationStatus authstatus, WorldTargetDelegate target)
        //{
        //    AddWorldTarget((uint)opcode, authstatus, target);
        //}

        //public void AddWorldTarget(uint opcode, ClientAuthentificationStatus authstatus, WorldTargetDelegate target)
        //{
        //    _targets[opcode] = new RPCTargetContainer(opcode, authstatus, target);
        //}

        internal void InvokePacket(PacketIn packet)
        {
            var target = (RPCTargetContainer)null;
            var opcode = packet.ReadUInt32();

            if (_targets.TryGetValue(opcode, out target))
            {
                // Prüfen ob der AuthStatus des Player/Clients passt?
                if (target.AuthentificationStatus >= packet.Client.AuthentificationStatus)
                {
                    switch (target.TargetType)
                    {
                        case RPCTargetTypes.Engine:
                            Logger.Trace("RPC engine invoke '{0}'", opcode);
                            target.EngineTarget.Invoke(_engine, packet.Client, packet);
                            break;

                        //case RPCTargetTypes.World:
                        //    Logger.Trace("RPC world invoke '{0}'", opcode);
                        //    var player = _engine.Players.GetPlayer(packet.Client.GUID);
                        //    if (player == null) return;
                        //    target.WorldTarget.Invoke(_world, player, packet);
                        //    break;
                    }
                }
                else
                {
                    Logger.Warning("Invalid authentification level '{0}' from '{1}'", opcode, packet.Client.AuthentificationStatus);
                    packet.Client.Close();
                }
            }
            else
            {
                Logger.Warning("OpCode unkown opcode '{0}' from '{1}'", opcode, packet.Client.RemoteAddress);
                packet.Client.Close();
            }
        }
    }
}
