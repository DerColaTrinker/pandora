﻿using Pandora.Game.Communications;
using Pandora.Game.Communications.PCK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game;
using Pandora.Game.World;

namespace Pandora.Game.Communications.RPC
{
    class RPCTargetContainer
    {
        public RPCTargetContainer(uint opcode, ClientAuthentificationStatus authstatus, EngineTargetDelegate target)
        {
            OpCode = opcode;
            EngineTarget = target;
            AuthentificationStatus = authstatus;
            TargetType = RPCTargetTypes.Engine;
        }

        //public RPCTargetContainer(uint opcode, ClientAuthentificationStatus authstatus, WorldTargetDelegate target)
        //{
        //    OpCode = opcode;
        //    WorldTarget = target;
        //    AuthentificationStatus = authstatus;
        //    TargetType = RPCTargetTypes.World;
        //}

        public uint OpCode { get; private set; }

        public EngineTargetDelegate EngineTarget { get; private set; }

        //public WorldTargetDelegate WorldTarget { get; private set; }

        public ClientAuthentificationStatus AuthentificationStatus { get; private set; }

        public RPCTargetTypes TargetType { get; private set; }
    }
}
