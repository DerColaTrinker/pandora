﻿using Pandora.Runtime.IO;
using System;

namespace Pandora.Game.Communications
{
    /// <summary>
    /// Ein eingehendes Datenpaket
    /// </summary>
    public interface IPacketIn : IReader
    {
        OpCodes WriteOpCode();
    }
}
