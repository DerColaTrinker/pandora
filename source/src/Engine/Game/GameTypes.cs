﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game
{
    public enum GameTypes
    {
        Singleplayer,
        MultiplayerClient,
        MultiplayerServer
    }
}
