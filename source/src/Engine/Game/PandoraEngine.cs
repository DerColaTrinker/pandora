﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Pandora.Runtime.Configuration;
using Pandora.Game.Interaction;
using Pandora.Game.Globalisation;
using Pandora.Game.Templates;
using Pandora.Game.Communications;
using Pandora.Game.World.Generators;
using Pandora.Game.World;
using System.Net.Sockets;

namespace Pandora.Game
{
    public abstract class PandoraEngine
    {
        /// <summary>
        /// Erstellt eine neue Instanz der Game-Basisklasse
        /// </summary>
        public PandoraEngine()
        {
            Console.Clear();
            Console.WriteLine(" PPPP     AA   NNNN   DDDD     OO   RRRR     AA               ");
            Console.WriteLine(" PP  PP AA  AA NN  NN DD  DD OO  OO RR  RR AA  AA  Game-Engine");
            Console.WriteLine(" PPPP   AAAAAA NN  NN DD  DD OO  OO RRRR   AAAAAA  V {0,-10}  ", Assembly.GetCallingAssembly().GetName().Version);
            Console.WriteLine(" PP     AA  AA NN  NN DD  DD OO  OO RR  RR AA  AA             ");
            Console.WriteLine(" PP     AA  AA NN  NN DDDD     OO   RR  RR AA  AA  http://prokekt-pandora.de");
            Console.Write(new string('-', Console.WindowWidth));

            Configuration = new ConfigurationContainer();

            // Standartpfade
            var binpath = new DirectoryInfo(Environment.CurrentDirectory);
            var rootpath = binpath.Parent;

            Configuration.SetValue("Engine", "BinPath", binpath);
            Configuration.SetValue("Engine", "RootPath", binpath.Parent);
            Configuration.SetValue("Engine", "DataPath", Path.Combine(rootpath.FullName, "data"));
        }

        #region Initialisieren

        /// <summary>
        /// Initialisiert das Spielsystem
        /// </summary>
        /// <returns></returns>
        public bool Initialize()
        {
            // Das erste init aufrufen
            if (!OnInitialize()) return false;

            // Interaction (SFML) starten
            if (!Configuration.GetValue("Engine", "DisableInteraction", false))
            {
                // Nur starten wenn es nicht durch eine Konfiguration blockiert wird
                Interaction = new InteractionManager(this);
                if (!Interaction.Initialize()) return false;
            }

            // Translation starten
            Translations = new TranslationManager();

            // Playersystem starten
            //Players = new PlayerManager(this);

            // Templatesystem starten
            Templates = new TemplateManager();

            // Content laden lassen
            if (!OnLoadContent()) return false;

            World = new WorldManager(this);

            // Netzwerksystem starten
            Communications = new CommunicationManager(this);

            // Das Spielstartet auch ohne Netzwerk unterstützung, wird allerdings kein Multiplayer anbieten
            IsNetworkAvailable = Communications.Initialize();

            return true;
        }

        /// <summary>
        /// Beendet das Spielsystem
        /// </summary>
        /// <returns></returns>
        public bool Destroy()
        {
            IsRunning = false;

            if (!OnUnloadContent()) return false;
            if (!OnDestroy()) return false;

            Translations = null;
            Communications = null;
            Templates = null;
            Translations = null;

            return true;
        }

        #endregion

        #region Methoden für die Abgeleitete Klasse

        /// <summary>
        /// Wird aufgerufen wenn das Spielsystem initialisiert wird
        /// </summary>
        /// <returns></returns>
        protected virtual bool OnInitialize()
        {
            return true;
        }

        /// <summary>
        /// Wird aufgerufen wenn das Spielsystem Spielcontent anfordert
        /// </summary>
        /// <returns></returns>
        protected virtual bool OnLoadContent()
        {
            return true;
        }

        /// <summary>
        /// Wird aufgerufen wenn das Spielsystem beendet wird
        /// </summary>
        /// <returns></returns>
        protected virtual bool OnDestroy()
        {
            return true;
        }

        /// <summary>
        /// Wird aufgerufen um ggf. Content zu entladen
        /// </summary>
        /// <returns></returns>
        protected virtual bool OnUnloadContent()
        {
            return true;
        }

        #endregion

        #region Ablaufsteuerung

        private IntPtr _clock;

        /// <summary>
        /// Startet die Programmausführung
        /// </summary>
        /// <returns></returns>
        public bool Start()
        {
            if (IsRunning)
            {
                Logger.Warning("Game is already running");
                return true;
            }

            IsRunning = true;

            RuntimeLoop();

            return true;
        }

        /// <summary>
        /// Stoppt die Programmausführung
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            if (!IsRunning)
            {
                Logger.Warning("Game is already stopped");
                return true;
            }

            IsRunning = false;
            return true;
        }

        private void RuntimeLoop()
        {
            // Runtime nicht starten, da wir das nicht aktiviert haben
            if (Configuration.GetValue("Engine", "DisableRuntimeLoop", false)) return;

            _clock = NativeSFMLCalls.sfClock_create();
            var elapsedms = 1F;
            var elapseds = 0.001F;

            while (IsRunning)
            {
                NativeSFMLCalls.sfClock_restart(_clock);

                CallSystemUpdate(elapsedms, elapseds);

                elapsedms = NativeSFMLCalls.sfClock_getElapsedTime(_clock).AsMilliseconds();
                elapseds = elapsedms / 1000F;
            }
        }

        protected void CallSystemUpdate(float elapsedms, float elapseds)
        {
            if (!IsRunning) return;

            OnSystemUpdate(elapsedms, elapseds);

            DispatchMessage();
            World.SystemUpdate(elapsedms, elapseds);
            Interaction.SystemUpdate(elapsedms, elapseds);
        }

        public void DispatchMessage()
        {
            if (Packets.InQueue.Count > 0)
            {
                var packet = Packets.InQueue.Pop();

                Communications.RPC.InvokePacket(packet);

                Packets.ReleasePacket(packet);
            }
        }

        public void DispatchAllMessages()
        {
            Logger.Warning("Dispatching all messages");

            while (Packets.InQueue.Count > 0)
            {
                DispatchMessage();
            }
        }

        /// <summary>
        /// Wird aufgerufen wenn das SystemUpdate auslöst
        /// </summary>
        /// <param name="elapsedms"></param>
        /// <param name="elapseds"></param>
        protected virtual void OnSystemUpdate(float elapsedms, float elapseds)
        {
            // Nix machen
        }

        /// <summary>
        /// Gibt an ob das Spielsystem aktiv ist
        /// </summary>
        public bool IsRunning { get; private set; }

        #endregion

        #region Game Control

        //public bool CreateSingleplayergame(SinglelPlayer player)
        //{
        //    if (Communications == null) { Logger.Error("Engine not initialized"); return false; }

        //    GameType = GameTypes.Singleplayer;

        //    Players.Add(player, true);

        //    if (!StartWorld()) return false;

        //    // !!! An dieser stelle sollte der Queue mit Nachrichte zum erstelles der Spielwelt voll sein...
        //    DispatchAllMessages();

        //    return true;
        //}

        //public bool CreateMultiplayerServer(MultiPlayer player, string listenip, int listenport)
        //{
        //    if (Communications == null) { Logger.Error("Engine not initialized"); return false; }




        //    return true;
        //}

        //public bool CreateMultiplayerClient(MultiPlayer player, string connectionip, int connectionport)
        //{
        //    if (Communications == null) { Logger.Error("Engine not initialized"); return false; }




        //    return true;
        //}

        internal bool StartWorld()
        {
            return World.Initialize(new DefaultGenerator());
        }

        public bool IsNetworkAvailable { get; private set; }

        public GameTypes GameType { get; private set; }

        #endregion

        #region Eigenschaften

        public ConfigurationContainer Configuration { get; private set; }

        public InteractionManager Interaction { get; private set; }

        public CommunicationManager Communications { get; private set; }

        public WorldManager World { get; private set; }

        public TranslationManager Translations { get; private set; }

        public TemplateManager Templates { get; private set; }

        //public PlayerManager Players { get; private set; }

        #endregion
    }
}
