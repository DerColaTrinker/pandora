﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Pandora.Runtime.Extensions;

namespace Pandora.Game.Globalisation
{
    public sealed class TranslationManager
    {
        private Dictionary<string, TranslationDictionary> _dictionaries = new Dictionary<string, TranslationDictionary>(StringComparer.InvariantCultureIgnoreCase);
        private HashSet<string> _identifiers = new HashSet<string>();

        #region Dictionaries

        public bool AddDictionary(string langcode)
        {
            if (ContainsLanguage(langcode))
            {
                Logger.Error("Translation dictionary '{0}' already exists", langcode);
                return false;
            }

            var dictionary = new TranslationDictionary(langcode);

            CurrentDictionary = dictionary;

            // Alle Einträge übernehmen
            _identifiers.All(m => { dictionary.Add(m, string.Empty, string.Empty); return true; });

            _dictionaries.Add(langcode, dictionary);

            Logger.Debug("Translation dictionary '{0}' added", langcode);
            return true;
        }

        public bool RemoveDictionary(string langcode)
        {
            if (!ContainsLanguage(langcode))
            {
                Logger.Error("Translation dictionary '{0}' not found", langcode);
                return false;
            }

            _dictionaries.Remove(langcode);

            if (CurrentDictionary.LanguageCode == langcode) CurrentDictionary = _dictionaries.Values.FirstOrDefault();

            Logger.Debug("Translation dictionary '{0}' removed", langcode);
            return true;
        }

        public bool ContainsLanguage(string langcode)
        {
            return _dictionaries.ContainsKey(langcode);
        }

        public IEnumerable<TranslationDictionary> GetDictionaries()
        {
            return _dictionaries.Values;
        }

        public TranslationDictionary GetDictionary(string langcode)
        {
            if (ContainsLanguage(langcode))
            {
                return _dictionaries[langcode];
            }
            else
            {
                return null;
            }
        }

        public TranslationDictionary CurrentDictionary { get; private set; }

        public bool SelectTranslation(string langcode)
        {
            if (!ContainsLanguage(langcode))
            {
                Logger.Error("Translation dictionary '{0}' not found", langcode);
                return false;
            }

            return SelectTranslation(_dictionaries[langcode]);
        }

        public bool SelectTranslation(TranslationDictionary dictionary)
        {
            CurrentDictionary = dictionary;

            Logger.Normal("Translation dictionary changed to '{0}'", dictionary.LanguageCode);
            return true;
        }

        #endregion

        #region Entries

        public bool AddEntry(string identifier, string name, string description)
        {
            if (_identifiers.Add(identifier))
            {
                Logger.Trace("Translation identifier '{0}' created", identifier);

                foreach (var d in _dictionaries.Values) { d.Add(identifier, string.Empty, string.Empty); }
            }

            if (CurrentDictionary != null) CurrentDictionary.Update(identifier, name, description);

            Logger.Debug("Translation entry '{0}' added", identifier);

            return true;
        }

        public bool AddEntry(string identifier)
        {
            return AddEntry(identifier, string.Empty, string.Empty);
        }

        public bool AddEntry(string langcode, string identifier, string name, string description)
        {
            if (_identifiers.Add(identifier))
            {
                Logger.Trace("Translation identifier '{0}' created", identifier);

                foreach (var d in _dictionaries.Values) { d.Add(identifier, string.Empty, string.Empty); }
            }

            var dictionary = GetDictionary(langcode);
            dictionary.Update(identifier, name, description);

            Logger.Debug("Translation entry '{0}' for '{1}' added", identifier, langcode);

            return true;
        }

        public bool RemoveEntry(string identifier)
        {
            if (_identifiers.Remove(identifier))
            {
                 Logger.Trace("Translation identifier '{0}' removed", identifier);
            }
            else
            {
                Logger.Warning("Translation entry identifier '{0}' not found", identifier);
                return false;               
            }

            foreach (var d in _dictionaries.Values) { d.Remove(identifier); }

            Logger.Debug("Translation entry '{0}' removed", identifier);
            return true;
        }
        
        public bool Rename(string oldidentifier, string newidentifier)
        {
            if (_identifiers.Contains(newidentifier))
            {
                Logger.Error("Translation entry rename '{0}' already exists", newidentifier);
                return false;
            }

            if (!_identifiers.Contains(oldidentifier))
            {
                Logger.Error("Translation entry rename '{0}' not found", oldidentifier);
                return false;
            }

            // Identifier austauschen
            _identifiers.Remove(oldidentifier);
            _identifiers.Add(newidentifier);

            foreach (var d in _dictionaries.Values)
            {
                d.Rename(oldidentifier, newidentifier);
            }

            Logger.Debug("Translation entry '{0}' -> '{1}' renamed", oldidentifier, newidentifier);
            return false;
        }

        public bool ContainsIdentifier(string identifier)
        {
            return _identifiers.Contains(identifier);
        }

        public IEnumerable<string> GetIdentifiers()
        {
            return _identifiers.ToArray();
        }

        public IEnumerable<TranslationEntry> GetCurrentTranslationEntries()
        {
            return CurrentDictionary.GetEntries();
        }

        public TranslationEntry GetCurrentTranslationEntry(string identifier)
        {
            if (!ContainsIdentifier(identifier))
            {
                Logger.Warning("Translation entry identifier '{0}' not found", identifier);
                return null;
            }

            return CurrentDictionary.GetEntry(identifier);
        }

        public TranslationEntry GetEntry(string identifier)
        {
            if (_identifiers.Contains(identifier))
                return CurrentDictionary.GetEntry(identifier);
            else
                return null;
        }

        #endregion

        #region Loading File

        public bool Load(PandoraContentStream stream)
        {
            if (stream.ContentType == PandoraContentStreamTypes.Source)
            {
                if (!stream.File.Exists)
                {
                    Logger.Error("Translation file '{0}' not found", stream.File.Name);
                    return false;
                }
            }

            Logger.Normal("Translation file '{0}' loading", stream.File.Name);

            var xd = new XmlDocument();
            xd.Load(stream);

            foreach (XmlNode dnode in xd.SelectNodes("pandora/translations/dictionary"))
            {
                var langcode = dnode.Attributes.GetValue("langcode", "");
                if (string.IsNullOrEmpty(langcode)) continue;
                if (ContainsLanguage(langcode)) { Logger.Warning("Translation language '{0}' already exists", langcode); continue; }
                if (!AddDictionary(langcode)) continue;

                foreach (XmlNode inode in dnode.SelectNodes("entry"))
                {
                    var identifier = inode.Attributes.GetValue("identifier", "");
                    var name = inode.Attributes.GetValue("name", string.Empty);
                    var description = inode.Attributes.GetValue("description", string.Empty);

                    AddEntry(langcode, identifier, name, description);
                }
            }
            return true;
        }

        #endregion
    }
}
