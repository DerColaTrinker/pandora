﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Globalisation
{
    public sealed class TranslationDictionary
    {
        private Dictionary<string, TranslationEntry> _entries = new Dictionary<string, TranslationEntry>(StringComparer.InvariantCultureIgnoreCase);

        internal TranslationDictionary(string langcode)
        {
            LanguageCode = langcode;
        }
        
        internal void Add(string identifier, string name, string description)
        {
            var entry = new TranslationEntry(identifier, name, description);

            _entries[identifier] = entry;
        }

        internal void Remove(string identifier)
        {
            _entries.Remove(identifier);
        }
        
        internal void Update(string identifier, string name, string description)
        {
            var entry = _entries[identifier];
            entry.Name = name;
            entry.Description = description;
        }

        internal void Rename(string oldidentifier, string newidentifier)
        {
            var entry = _entries[oldidentifier];

            _entries.Remove(oldidentifier);
            entry.Identifier = newidentifier;
            _entries.Add(newidentifier, entry);
        }

        public IEnumerable<TranslationEntry> GetEntries()
        {
            return _entries.Values;
        }

        public TranslationEntry GetEntry(string identifier)
        {
            if (_entries.ContainsKey(identifier)) return _entries[identifier];

            return null;
        }

        public string LanguageCode { get; private set; }

     
    }
}
