﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Globalisation
{
    public sealed class TranslationEntry
    {
        internal TranslationEntry(string identifier, string name, string description)
        {

            Identifier = identifier;
            Name = name;
            Description = description;
        }

        public string Identifier { get; internal set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
