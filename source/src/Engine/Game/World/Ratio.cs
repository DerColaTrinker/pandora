﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Generators
{
    public sealed class WorldGeneratorRatio<T>
    {
        private Dictionary<T, int> _ratio = new Dictionary<T, int>();

        public WorldGeneratorRatio(IEnumerable<T> types)
        {
            foreach (var type in types)
            {
                _ratio.Add(type, 0);
            }
        }

        public float Sum()
        {
            return _ratio.Values.Sum();
        }

        public int Count()
        {
            return _ratio.Count;
        }

        public float Avg()
        {
            return (float)_ratio.Values.Average();
        }

        public float Ratio(T type)
        {
            var ratio = _ratio[type] / Sum();

            return ratio;
        }

        public void Increment(T type)
        {
            _ratio[type]++;
        }

        public void Decrement(T type)
        {
            _ratio[type]--;
        }

        public IEnumerable<T> Types { get { return _ratio.Keys; } }

        public IEnumerable<int> Values { get { return _ratio.Values; } }
    }
}
