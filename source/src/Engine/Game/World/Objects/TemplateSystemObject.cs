﻿using Pandora.Game.World.Objects;
using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Runtime.IO;

namespace Pandora.Game.World.Objects
{
    public abstract class TemplateSystemObject<TTemplate> : SystemObject
        where TTemplate : TemplateBase
    {
        public TTemplate Template { get; internal set; }

        internal override bool HasTemplate { get { return true; } }

        protected internal override bool Serialize(IWriter writer)
        {
            base.Serialize(writer);

            // Templatevorlage speichern
            if (HasTemplate)
                writer.WriteString(Template.Identifier);

            return true;
        }

        protected internal override bool Deserialize(IReader reader, PandoraEngine world)
        {
            base.Deserialize(reader, world);

            if (HasTemplate)
            {
                var identifier = reader.ReadString();
                Template = world.Templates.GetTemplate<TTemplate>(identifier);

                if (Template == null)
                {
                    Logger.Error("Template '{0}' for object '{1}' not found", identifier, GetType().Name);
                    return false;
                }
            }

            return true;
        }
    }
}
