﻿using Pandora.Game.Templates.MapObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public sealed class Planet : TemplateSystemObject<PlanetTemplate>
    {
        public StarOrbitTemplates PlanetOrbit { get; set; }

        public int BasePopulation { get; set; }

        public int BaseSize { get; set; }

        public int Wealth { get; set; }

        public int Temperature { get; set; }
    }
}
