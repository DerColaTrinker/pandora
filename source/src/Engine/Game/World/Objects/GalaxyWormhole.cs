﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
 public   sealed class GalaxyWormhole : GalaxyObject
    {
        public StarSystem SystemA { get; set; }

        public StarSystem SystemB { get; set; }

        protected internal override bool Serialize(IWriter writer)
        {
            if (!base.Serialize(writer)) return false;

            writer.WriteUInt32(SystemA.ReferenceID);
            writer.WriteUInt32(SystemB.ReferenceID);

            return true;
        }

        protected internal override bool Deserialize(IReader reader, PandoraEngine engine)
        {
            if (!base.Deserialize(reader, engine)) return false;

            SystemA = engine.World.ObjectAccessor.GetObject<StarSystem>(reader.ReadUInt32());
            SystemB = engine.World.ObjectAccessor.GetObject<StarSystem>(reader.ReadUInt32());

            return true;
        }
    }
}
