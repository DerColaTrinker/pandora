﻿using Pandora.Game.World.Objects;
using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public abstract class SystemObject : GameObject
    {
        public WorldManager World { get; internal set; }

        public GalaxyObject Node { get; internal set; }

        public string Name { get; set; }

        public float PositionX { get; set; }

        public float PositionY { get; set; }

        public virtual bool IsActiv { get { return false; } }

        protected internal override bool Serialize(IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteString(Name);
            writer.WriteFloat(PositionX);
            writer.WriteFloat(PositionY);

            return true;
        }

        protected internal override bool Deserialize(IReader reader, PandoraEngine engine)
        {
            base.Deserialize(reader, engine);

            Name = reader.ReadString();
            PositionX = reader.ReadFloat();
            PositionY = reader.ReadFloat();

            return true;
        }
    }
}
