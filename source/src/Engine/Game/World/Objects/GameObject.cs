﻿using Pandora.Runtime.IO;
using Pandora.Runtime.Referencing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public abstract class GameObject : ReferenceObject
    {
        public GameObject()
            : base()
        { }

        internal static TObject CreateFromReader<TObject>(PandoraEngine engine, IReader reader)
            where TObject : GameObject
        {
            var reftypename = reader.ReadString();
            var reftype = (Type)null;

            try
            {
                reftype = Type.GetType(reftypename);
            }
            catch (Exception)
            {
                Logger.Error("Objecttype '{0}' not found", typeof(TObject).Name);
                return null;
            }

            var hastemplate = reftype.GetInterfaces().Where(m => m.Name.Contains("ITemplate")).FirstOrDefault() != null;
            var instance = default(TObject);

            try
            {
                instance = (TObject)Activator.CreateInstance(reftype);
                Logger.Trace("Object instance created '{0}'", reftype.Name);
            }
            catch (Exception ex)
            {
                Logger.Error("Could not create instance '{0}' ( see exception )", reftype.Name);
                Logger.Exception(ex);
            }

            if (!instance.Deserialize(reader, engine))
            {
                Logger.Error("Deserialisation failed '{0}'", reftype.Name);
                return default(TObject);
            }

            return instance;
        }

        internal virtual bool HasTemplate { get { return false; } }

        protected internal virtual void Clear()
        {
            // Ebenfalls nichts machen           
        }

        protected internal virtual void WorldUpdate(float ms, float s)
        {
            // Nichts machen
        }

        protected internal virtual bool Serialize(IWriter writer)
        {
            // TypeObject schreiben
            writer.WriteString(GetType().FullName);

            // Referenz wegschreiben
            writer.WriteUInt32(ReferenceID);

            return true;
        }

        protected internal virtual bool Deserialize(IReader reader, PandoraEngine engine)
        {
            // Der Type wird in der Factory-Methode gelesen und ist an dieser stelle nicht mehr vorhanden

            var refid = reader.ReadUInt32();
            if (!ReferenceManager.Boxing(this, refid)) return false;

            return true;
        }
    }
}
