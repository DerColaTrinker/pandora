﻿using Pandora.Game.World.Objects;
using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Runtime.IO;

namespace Pandora.Game.World.Objects
{
    public delegate void GalaxyObjectActivityChangedDelegate(GalaxyObject obj);

    /// <summary>
    /// Basisklasse eines Spielobjekts das sich in der Galaxieansicht befindet
    /// </summary>
    public abstract class GalaxyObject : GameObject
    {
        private HashSet<SystemObject> _objects = new HashSet<SystemObject>();
        private int _activeobjectcounter = 0;

        public event GalaxyObjectActivityChangedDelegate GalaxyObjectActivityChanged;
              
        public WorldManager World { get; internal set; }

        public float PositionX { get; set; }

        public float PositionY { get; set; }

        public string Name { get; set; }

        internal void Add(SystemObject obj)
        {
            if (_objects.Add(obj))
            {
                if (obj.IsActiv) ActiveObjectReferenceCounter++;
            }
            else
            {
                Logger.Warning("Object '{0}' already exists in map '{1}'", obj.Name, Name);
            }
        }

        internal void Remove(SystemObject obj)
        {
            if (_objects.Remove(obj))
            {
                if (obj.IsActiv) ActiveObjectReferenceCounter--;
            }
            else
            {
                Logger.Warning("Object '{0}' not found in map '{1}'", obj.Name, Name);
            }
        }

        public IEnumerable<TObject> GetSystemObjects<TObject>()
            where TObject : SystemObject
        {
            return _objects.OfType<TObject>().AsEnumerable();
        }

        public int Count { get { return _objects.Count; } }

        protected internal override bool Serialize(IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteString(Name);
            writer.WriteFloat(PositionX);
            writer.WriteFloat(PositionY);

            return true;
        }

        protected internal override bool Deserialize(IReader reader, PandoraEngine engine)
        {
            base.Deserialize(reader, engine);

            Name = reader.ReadString();
            PositionX = reader.ReadFloat();
            PositionY = reader.ReadFloat();

            return true;
        }

        public int ActiveObjectReferenceCounter
        {
            get { return _activeobjectcounter; }
            internal set
            {
                if (_activeobjectcounter == value) return;

                _activeobjectcounter = value;

                if (GalaxyObjectActivityChanged != null) GalaxyObjectActivityChanged.Invoke(this);
            }
        }
    }
}
