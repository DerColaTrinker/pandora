﻿using Pandora.Game.Communications;
using Pandora.Game.Communications.PCK;
using Pandora.Game.World.Generators;
using Pandora.Game.World.Handler;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World
{
    public class WorldManager
    {
        internal WorldManager(PandoraEngine engine)
        {
            Engine = engine;

            Logger.Trace("WorldManager created");
        }

        internal bool Initialize(GeneratorBase generator)
        {
            ObjectAccessor = new ObjectAccessor();
            Galaxy = new GalaxyMapHandler(this);
            Systems = new SystemMapHandler(this);

            if (!generator.InternalStart(this)) return false;

            return true;
        }

        public PandoraEngine Engine { get; private set; }

        public GameTypes GameType { get { return Engine.GameType; } }

        public ObjectAccessor ObjectAccessor { get; private set; }

        public GalaxyMapHandler Galaxy { get; private set; }

        public SystemMapHandler Systems { get; private set; }
        
        //public void SendTo(MultiPlayer player, IPacketOut pout)
        //{
        //    Engine.Players.SentTo(player, pout);
        //}

        //public void Broadcast(IPacketOut pout)
        //{
        //    switch (Engine.GameType)
        //    {
        //        // Bei einem SPC werden die Nachrichten an sich selber direkt in Warteschleife gelegt, somit spare ich mir die
        //        // Netzwerk-Kommunikation auf dem Loopback.
        //        case GameTypes.Singleplayer:
        //            var pin = Packets.GetPacketIn();                                                             // Ein neues InPacket holen
        //            pin.CleanUp(Engine.Players.CurrentLocalPlayer, ((PacketOut)pout).ToArray(), pout.Length);    // Es mit den OutPacket Daten füllen
        //            Packets.InQueue.Push(pin);                                                                   // Das InPacket in die Warteschleife legen
        //            Packets.ReleasePacket(pout);                                                                 // Das OutPacket wieder freigeben
        //            break;

        //        // Bei einem MPS oder MPC gehen die Nachrichten tatsätlich an die Clients bzw. Server.
        //        // Da bei einem MPC nur der Server in der Verbindungsliste steht kann hier auch ein 
        //        // Broadcast benutzt werden.
        //        case GameTypes.MultiplayerClient:
        //        case GameTypes.MultiplayerServer:
        //            Engine.Players.Broadcast(pout);
        //            break;
        //    }
        //}

        internal void SystemUpdate(float ms, float s)
        {
            Systems.SystemUpdate(ms, s);
        }
    }
}
