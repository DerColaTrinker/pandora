﻿using Pandora.Game.Communications;
using Pandora.Game.Communications.Handler;
using Pandora.Game.Templates.MapObjects;
using Pandora.Game.World.Generators;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pandora.Game.World.Generators
{
    /// <summary>
    /// Stellt ein Spielwelt-Generator dar für die Standard Pandora Spielwelt
    /// </summary>
    public sealed class DefaultGenerator : GeneratorBase
    {
        private IEnumerable<StarOrbitTemplates> _orbits;

        /// <summary>
        /// Wird aufgerufen wenn eine Spielwelt generiert werden muss
        /// </summary>
        /// <returns></returns>
        protected override bool OnCreateWorld()
        {
            var starcount = Engine.Configuration.GetValue("Map", "StarCount", 200);

            _orbits = Engine.Templates.GetTemplates<StarOrbitTemplates>();

            if (!CreateGalaxyObjects<StarTemplate, StarSystem>(starcount, GenerateStarsystems)) return false;

            //if (!CreateSystemObjects<PlanetTemplate, StarSystem, Planet>(GeneratePlanets)) return false;
            //CreateWormholes();

            return true;
        }

        private bool GenerateStarsystems(int index, IEnumerable<StarTemplate> templates, RatioCounter<StarTemplate> ratio)
        {
            var x = 0D;
            var y = 0D;
            var startype = (StarTemplate)null;
            var failcounter = 0;

            // Position ermitteln. Wenn ein Stern in der nähe ist, eine neue Position suchen
            while (true)
            {
                x = RND.NextDouble() * World.Galaxy.GalaxyMapSizeX;
                y = RND.NextDouble() * World.Galaxy.GalaxyMapSizeY;

                if (!HasObjectNeighborhood<StarSystem>(x, y, 20D)) break;

                failcounter++;
                if (failcounter > 200)
                {
                    StopProcess();
                    Logger.Warning("No more space for more stars.");
                    return true;
                }
            }

            failcounter = 0;

            // Sterntyp ermitteln. Zufallsbasierend allerdings abhängig von der Angabe wie oft ein Sterntyp vorkommen darf.
            while (true)
            {
                var startypeindex = RND.Next(ratio.Count());
                startype = templates.Skip(startypeindex).Take(1).First();

                if (ratio.Ratio(startype) < startype.Ratio)
                {
                    ratio.Increment(startype);
                    break;
                }

                failcounter++;
                if (failcounter > 200)
                {
                    // Einfach nehmen...
                    ratio.Increment(startype);
                    break;
                }
            }

            // Objekte können direkt erstellt und konfiguriert werden
            var system = CreateGalaxyObjectInstance<StarSystem, StarTemplate>(startype);
            system.PositionX = (float)x;
            system.PositionY = (float)y;
            system.Name = "System " + (index + 1);

            var star = CreateSystemObjectInstance<Star, StarTemplate>(startype);
            star.PositionX = World.Systems.SystemMapSizeX / 2F;
            star.PositionY = World.Systems.SystemMapSizeY / 2F;
            star.Name = "Stern " + (index + 1);

            // Die zuwesiung der Objekte an die Handler. Der Zugriff auf den ObjectAccessor erfolgt innerhalb der GalaxyMap- und SystemMapHandler
            World.Galaxy.Add(system);
            World.Systems.Add(system, star);

            return true;
        }

        private bool GeneratePlanets(int index, StarSystem galaxyobject, IEnumerable<PlanetTemplate> templates, RatioCounter<PlanetTemplate> ratio)
        {
            var planettemplate = (PlanetTemplate)null;
            var orbittemplate = (StarOrbitTemplates)null;
            var temperature = 0;
            var failcounter = 0;

            var planetcount = RND.Next(1, _orbits.Count());

            for (int planetindex = 0 ; planetindex < planetcount ; planetindex++)
            {
                // Zuerst den Planetentyp auslosen
                while (true)
                {
                    var planettypeindex = RND.Next(ratio.Count());
                    planettemplate = templates.Skip(planettypeindex).Take(1).First();

                    if (ratio.Ratio(planettemplate) < planettemplate.Ratio)
                    {
                        ratio.Increment(planettemplate);
                        break;
                    }

                    failcounter++;
                    if (failcounter > 200)
                    {
                        // Einfach nehmen...
                        ratio.Increment(planettemplate);
                        break;
                    }
                }

                failcounter = 0;

                // Jetzt einen Passenden Orbit suchen
                while (true)
                {
                    temperature = RND.Next(planettemplate.MinTemperature, planettemplate.MaxTemperature);

                    var orbits = _orbits.Where(m => temperature >= m.MinTemperature & temperature <= m.MaxTemperature);

                    if (orbits.Count() == 0)
                    {
                        failcounter++;
                        if (failcounter > 200)
                        {
                            // Einfach nehmen...
                            orbits = _orbits;
                            break;
                        }
                    }

                    foreach (var orbit in orbits)
                    {
                        if (galaxyobject.GetSystemObjects<Planet>().Where(m => m.PlanetOrbit.Identifier != orbit.Identifier).Count() == 0)
                        {
                            orbittemplate = orbit;
                            break;
                        }
                    }

                    System.Diagnostics.Debug.Assert(orbittemplate != null);

                    var planet = CreateSystemObjectInstance<Planet, PlanetTemplate>(planettemplate);
                    planet.PlanetOrbit = orbittemplate;
                    planet.BasePopulation = RND.Next(planettemplate.MinPopulation, planettemplate.MaxPopulation);
                    planet.BaseSize = RND.Next(planettemplate.MinSize, planettemplate.MaxWealth);
                    planet.Wealth = RND.Next(planettemplate.MinWealth, planettemplate.MaxWealth);
                    planet.Temperature = temperature;

                    // Die Position wird im SystemUpdate berechnet
                    planet.PositionX = 0;
                    planet.PositionY = 0;

                    planet.Name = galaxyobject.Name + ' ' + RomanNumber(planetindex + 1);

                    // Planet hinzufügen
                    World.Systems.Add(galaxyobject, planet);
                    break;
                }
            }

            return true;
        }

        private void CreateWormholes()
        {
            var stack = new Stack<StarSystem>();
            var systems = World.Galaxy.GetObjects<StarSystem>();

            var chance = new ChanceCounter<int>(RND);
            chance.Add(1, 0.95F);
            chance.Add(2, 0.05F);
            //chance.Add(3, 0.05F);
            //chance.Add(4, 0.05F);

            stack.Push(systems.First());

            while (stack.Count > 0)
            {
                var csystem = stack.Pop();
                var nsystems = GetObjectNeighborhood(csystem, float.MaxValue);

                // Anzahl der Verbindungen ermitteln
                var count = chance.GetChance();

                // Sicherstellen das nicht mehr Verbindungen hergestellt werden, als wirklich verfügbar sind
                if (count > nsystems.Count()) count = nsystems.Count();

                for (int index = 0 ; index < count ; index++)
                {
                    var wha = new SystemWormhole();
                    var whb = new SystemWormhole();
                    var whg = new GalaxyWormhole();

                    wha.SystemA = csystem;
                    wha.SystemB = nsystems.ElementAt(index);

                    whb.SystemA = nsystems.ElementAt(index);
                    whb.SystemB = csystem;

                    whg.SystemA = csystem;
                    whg.SystemB = nsystems.ElementAt(index);

                    World.Systems.Add(csystem, wha);
                    World.Systems.Add(nsystems.ElementAt(index), whb);

                    World.Galaxy.Add(whg);
                }
            }
        }

        private IEnumerable<StarSystem> GetObjectNeighborhood(StarSystem obj, float distance)
        {
            var x1 = obj.PositionX - distance;
            var x2 = obj.PositionX + distance;
            var y1 = obj.PositionY - distance;
            var y2 = obj.PositionY + distance;

            var result = from o in World.Galaxy.GetObjects<StarSystem>()
                         where obj.PositionX >= x1 & obj.PositionX <= x2 & obj.PositionY >= y1 & obj.PositionY <= y2    // Sicherstellen das nur Objekte im Rahmenbereich berechnet werden
                         let b = Math.Pow(obj.PositionX - o.PositionX, 2)                                               // Fläche b berechnen
                         let c = Math.Pow(obj.PositionY - o.PositionY, 2)                                               // Fläche c berechnen
                         let a = Math.Sqrt(b + c)                                                                       // Länge a aus den addierten Flächen ( Pytagoras )
                         where a <= distance && obj != o && o.GetSystemObjects<SystemWormhole>().Count() == 0 && !Intersect(o, obj)
                         orderby a
                         select o;

            return result;
        }

        private bool Intersect(StarSystem a, StarSystem b)
        {
            bool result = false;

            var data = from s in World.Galaxy.GetObjects<StarSystem>()
                       from w in s.GetSystemObjects<SystemWormhole>()
                       select w;

            Parallel.ForEach<SystemWormhole>(data, delegate(SystemWormhole wormhole)
            {
                if (DoLinesIntersect(a, b, wormhole.SystemA, wormhole.SystemB))
                {
                    result = true;
                }
            });
            return result;
        }

        private bool DoLinesIntersect(StarSystem ss1, StarSystem ss2, StarSystem ss3, StarSystem ss4)
        {
            return !(PointsEqual(ss1, ss3) | PointsEqual(ss1, ss4) | PointsEqual(ss2, ss3) | PointsEqual(ss2, ss4)) && (CounterClockwiseTest(ss1, ss2, ss3) * CounterClockwiseTest(ss1, ss2, ss4) <= 0 & CounterClockwiseTest(ss3, ss4, ss1) * CounterClockwiseTest(ss3, ss4, ss2) <= 0);
        }

        private bool PointsEqual(StarSystem s1, StarSystem s2)
        {
            return s1.PositionX == s2.PositionX & s1.PositionY == s2.PositionY;
        }

        private int CounterClockwiseTest(StarSystem ss1, StarSystem ss2, StarSystem ss3)
        {
            float ssr1 = ss2.PositionX - ss1.PositionX;
            float ssr2 = ss3.PositionX - ss1.PositionX;
            float ssr3 = ss2.PositionY - ss1.PositionY;
            float ssr4 = ss3.PositionY - ss1.PositionY;
            if (ssr1 * ssr4 > ssr3 * ssr2)
            {
                return 1;
            }
            return -1;
        }

        private string RomanNumber(int number)
        {
            var stringBuilder = new StringBuilder();
            var array = new int[] { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 };
            var array2 = new string[] { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" };

            while (number > 0)
            {
                for (int i = array.Count<int>() - 1 ; i >= 0 ; i--)
                {
                    if (number / array[i] >= 1)
                    {
                        number -= array[i];
                        stringBuilder.Append(array2[i]);
                        break;
                    }
                }
            }

            return stringBuilder.ToString();
        }
    }
}
