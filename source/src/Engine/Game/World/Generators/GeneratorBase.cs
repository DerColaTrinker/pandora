﻿using Pandora.Game.Communications;
using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.World.Generators;
using Pandora.Game.World.Objects;
using Pandora.Game.Communications.Handler;

namespace Pandora.Game.World.Generators
{
    /// <summary>
    /// Ein Delegate das verwendet wird wenn Galaxy-Objekte erzeugt weden sollen
    /// </summary>
    /// <typeparam name="TTemplate"></typeparam>
    /// <typeparam name="TObject"></typeparam>
    /// <param name="index"></param>
    /// <param name="templates"></param>
    /// <param name="ratio"></param>
    /// <returns></returns>
    public delegate bool CreateGalaxyObjectDelegate<TTemplate, TObject>(int index, IEnumerable<TTemplate> templates, RatioCounter<TTemplate> ratio)
        where TTemplate : TemplateBase
        where TObject : GalaxyObject;

    /// <summary>
    /// Ein Delegate das verwendet wird wenn System-Objekte erzeugt werden sollen
    /// </summary>
    /// <typeparam name="TTemplate"></typeparam>
    /// <typeparam name="TGalaxyObject"></typeparam>
    /// <typeparam name="TSystemObject"></typeparam>
    /// <param name="index"></param>
    /// <param name="galaxyobject"></param>
    /// <param name="templates"></param>
    /// <param name="ratio"></param>
    /// <returns></returns>
    public delegate bool CreateSystemObjectDelegate<TTemplate, TGalaxyObject, TSystemObject>(int index, TGalaxyObject galaxyobject, IEnumerable<TTemplate> templates, RatioCounter<TTemplate> ratio)
        where TTemplate : TemplateBase
        where TGalaxyObject : GalaxyObject
        where TSystemObject : SystemObject;

    /// <summary>
    /// Ein Delegate das verwendet wird wenn System-Objekte erzeugt weden sollen
    /// </summary>
    /// <typeparam name="TGalaxyObject"></typeparam>
    /// <typeparam name="TSystemObject"></typeparam>
    /// <param name="index"></param>
    /// <param name="galaxyobject"></param>
    /// <returns></returns>
    public delegate bool CreateSystemObjectDelegate<TGalaxyObject, TSystemObject>(int index, TGalaxyObject galaxyobject)
        where TGalaxyObject : GalaxyObject
        where TSystemObject : SystemObject;

    /// <summary>
    /// Basisklasse für den Generator der Spielwelt
    /// </summary>
    public abstract class GeneratorBase
    {
        internal bool InternalStart(WorldManager world)
        {
            World = world;
            Engine = world.Engine;

            RND = new Random(Engine.Configuration.GetValue("Map", "Seed", Environment.TickCount));

            if (!OnCreateWorld()) return false;

            return true;
        }

        /// <summary>
        /// Muss abgeleitet werden und startet den Prozess
        /// </summary>
        /// <returns></returns>
        protected abstract bool OnCreateWorld();

        /// <summary>
        /// Erzeugt Galaxy-Objekte
        /// </summary>
        /// <typeparam name="TTemplate"></typeparam>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="count"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        protected bool CreateGalaxyObjects<TTemplate, TObject>(int count, CreateGalaxyObjectDelegate<TTemplate, TObject> target)
            where TTemplate : TemplateBase
            where TObject : GalaxyObject
        {
            var template = Engine.Templates.GetTemplates<TTemplate>();
            var ratio = new RatioCounter<TTemplate>(template);

            IsProcessStopped = false;

            Logger.BeginProgress(count, "Creating {0} Galaxyobjects", typeof(TObject).Name);

            for (int index = 0 ; index < count & !IsProcessStopped ; index++)
            {
                Logger.UpdateProgress(index + 1);

                if (!target.Invoke(index, template, ratio))
                    return false;
            }

            Logger.EndProgress(IsProcessStopped);

            return true;
        }

        /// <summary>
        /// Erzeugt System-Objekte
        /// </summary>
        /// <typeparam name="TTemplate"></typeparam>
        /// <typeparam name="TGalaxyObject"></typeparam>
        /// <typeparam name="TSystemObject"></typeparam>
        /// <param name="target"></param>
        /// <returns></returns>
        protected bool CreateSystemObjects<TTemplate, TGalaxyObject, TSystemObject>(CreateSystemObjectDelegate<TTemplate, TGalaxyObject, TSystemObject> target)
            where TTemplate : TemplateBase
            where TGalaxyObject : GalaxyObject
            where TSystemObject : SystemObject
        {
            var template = Engine.Templates.GetTemplates<TTemplate>();
            var ratio = new RatioCounter<TTemplate>(template);
            var items = World.Galaxy.GetObjects<TGalaxyObject>().ToArray();

            IsProcessStopped = false;

            Logger.BeginProgress(items.Length, "Creating {0} Systemobjects", typeof(TSystemObject));

            for (int index = 0 ; index < items.Length & !IsProcessStopped ; index++)
            {
                Logger.UpdateProgress(index + 1);

                if (!target.Invoke(index, items[index], template, ratio))
                    return false;
            }

            Logger.EndProgress(IsProcessStopped);

            return true;
        }

        /// <summary>
        /// Erzeugt System-Objekte
        /// </summary>
        /// <typeparam name="TGalaxyObject"></typeparam>
        /// <typeparam name="TSystemObject"></typeparam>
        /// <param name="target"></param>
        /// <returns></returns>
        protected bool CreateSystemObjects<TGalaxyObject, TSystemObject>(CreateSystemObjectDelegate<TGalaxyObject, TSystemObject> target)
            where TGalaxyObject : GalaxyObject
            where TSystemObject : SystemObject
        {
            var items = World.Galaxy.GetObjects<TGalaxyObject>().ToArray();

            IsProcessStopped = false;

            Logger.BeginProgress(items.Length, "Creating {0} Systemobjects", typeof(TSystemObject));

            for (int index = 0 ; index < items.Length & !IsProcessStopped ; index++)
            {
                Logger.UpdateProgress(index + 1);

                if (!target.Invoke(index, items[index]))
                    return false;
            }

            Logger.EndProgress(IsProcessStopped);

            return true;
        }

        /// <summary>
        /// Gibt an ob sich in der angegebenen Position und Distanz andere Objekte der Galaxyansicht befinden
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        protected bool HasObjectNeighborhood<TGalaxyObject>(double x, double y, double distance)
            where TGalaxyObject : GalaxyObject
        {
            var x1 = x - distance;
            var x2 = x + distance;
            var y1 = y - distance;
            var y2 = y + distance;

            var result = from o in World.Galaxy.GetObjects<TGalaxyObject>()
                         where x >= x1 & x <= x2 & y >= y1 & y <= y2                // Sicherstellen das nur Objekte im Rahmenbereich berechnet werden
                         let b = Math.Pow(x - o.PositionX, 2)                       // Fläche b berechnen
                         let c = Math.Pow(y - o.PositionY, 2)                       // Fläche c berechnen
                         let a = Math.Sqrt(b + c)                                   // Länge a aus den addierten Flächen ( Pytagoras )
                         where a <= distance
                         select o;

            return result.Count() > 0;
        }

        protected TObject CreateGalaxyObjectInstance<TObject, TTemplate>(TTemplate template)
            where TTemplate : TemplateBase
            where TObject : TemplateGalaxyObject<TTemplate>
        {
            var instance = Activator.CreateInstance<TObject>();
            instance.Template = template;
            return instance;
        }

        protected TObject CreateGalaxyObjectInstance<TObject>()
            where TObject : GalaxyObject
        {
            var instance = Activator.CreateInstance<TObject>();
            return instance;
        }

        protected TObject CreateSystemObjectInstance<TObject, TTemplate>(TTemplate template)
            where TTemplate : TemplateBase
            where TObject : TemplateSystemObject<TTemplate>
        {
            var instance = Activator.CreateInstance<TObject>();
            instance.Template = template;
            return instance;
        }

        protected TObject CreateSystemObjectInstance<TObject>()
            where TObject : SystemObject
        {
            var instance = Activator.CreateInstance<TObject>();
            return instance;
        }

        /// <summary>
        /// Liefert Zugriff auf die Spielwelt
        /// </summary>
        public WorldManager World { get; private set; }

        public PandoraEngine Engine { get; private set; }

        /// <summary>
        /// Liefert ein Zufallsgenerator
        /// </summary>
        public Random RND { get; private set; }

        /// <summary>
        /// Gibt an das Bearbeitungsprozess beendet wurde
        /// </summary>
        protected bool IsProcessStopped { get; private set; }

        /// <summary>
        /// Beendet den Bearbeitungsprozess
        /// </summary>
        protected void StopProcess()
        {
            IsProcessStopped = true;
        }


    }
}
