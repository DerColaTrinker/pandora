﻿using Pandora.Game;
using Pandora.Game.World;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Handler
{
    /// <summary>
    /// Handler der Objekte für die Galaxyansicht verwaltet
    /// </summary>
    public sealed class GalaxyMapHandler
    {
        private Dictionary<uint, GalaxyObject> _objects = new Dictionary<uint, GalaxyObject>();
        private WorldManager _world;
        private PandoraEngine _engine;

        

        internal GalaxyMapHandler(WorldManager world)
        {
            _world = world;
            _engine = world.Engine;

            GalaxyMapSizeX = world.Engine.Configuration.GetValue("Galaxy", "MapX", 500F);
            GalaxyMapSizeY = world.Engine.Configuration.GetValue("Galaxy", "MapY", 500F);

            Logger.Trace("GalaxyMapHandler created");
        }

        internal bool HasObjectNeighborhood(double x, double y, double p)
        {
            return (from o in _objects.Values
                    let b = Math.Pow(o.PositionX - x, 2)
                    let c = Math.Pow(o.PositionY - y, 2)
                    let a = Math.Sqrt(b + c)
                    where a <= p
                    select 1).Count() > 0;
        }

        public void Add(GalaxyObject obj)
        {
            //switch (_engine.GameType)
            //{
            //    case GameTypes.MultiplayerClient:
            //    case GameTypes.Singleplayer:
            //        {
            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.CMSG_WORLD_GALAXYHANDLER_ADDOBJECT);
            //            obj.Serialize(pout);
            //            _world.Broadcast(pout);
            //        }
            //        break;

            //    case GameTypes.MultiplayerServer:
            //        {
            //            InternalAdd(obj);

            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.SMSG_WORLD_GALAXYHANDLER_ADDOBJECT);
            //            obj.Serialize(pout);
            //            _world.Broadcast(pout);
            //        }
            //        break;
            //}
        }

        public void Remove(GalaxyObject obj)
        {
            //switch (World.GameType)
            //{
            //    case GameTypes.Singleplayer:
            //        InternalRemove(obj);
            //        break;

            //    case GameTypes.MultiplayerClient:
            //        {
            //            InternalRemove(obj);

            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.SMSG_WORLD_GALAXY_REMOVEOBJECT);
            //            obj.Serialize(pout);
            //            World.Broadcast(pout);
            //        }
            //        break;

            //    case GameTypes.MultiplayerServer:
            //        {
            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.CMSG_WORLD_GALAXY_REMOVEOBJECT);
            //            obj.Serialize(pout);
            //            World.Broadcast(pout);
            //        }
            //        break;
            //}
        }

        internal void InternalAdd(GalaxyObject obj)
        {
            // Das Objekt der Zentrale zukommen lassen
            _world.ObjectAccessor.Add(obj);

            // Das Objekt jetzt hinzufügen
            _objects.Add(obj.ReferenceID, obj);

            // Verweis auf die Spielwelt einrichten
            obj.World = _world;
        }

        internal void InternalRemove(GalaxyObject obj)
        {
            // Das Objekt aus der Zentrale entfernen
            _world.ObjectAccessor.Remove(obj);

            // Das Objekt entfernen
            _objects.Add(obj.ReferenceID, obj);

            // Das Objekt anweisen alles innerhalb dessen zu entfernen
            obj.Clear();
        }

        /// <summary>
        /// Liefert eine Liste aller Sternsysteme
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TObject> GetObjects<TObject>()
            where TObject : GalaxyObject
        {
            return _objects.Values.OfType<TObject>().AsEnumerable();
        }

        public TObject GetObject<TObject>(uint id)
            where TObject : GalaxyObject
        {
            var result = (GalaxyObject)null;

            _objects.TryGetValue(id, out result);

            return (TObject)result;
        }

        public int Count { get { return _objects.Count; } }

        public float GalaxyMapSizeX { get; private set; }

        public float GalaxyMapSizeY { get; private set; }

        internal void WorldUpdate(float ms, float s)
        {

        }

     
    }
}
