﻿using Pandora.Runtime.Referencing;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handler
{
    /// <summary>
    /// Eine Zentrale Verwaltungsstelle für alle Spielobjekte
    /// </summary>
    public sealed class ObjectAccessor
    {
        private Dictionary<uint, IReferenceObject> _objects = new Dictionary<uint, IReferenceObject>();

        internal ObjectAccessor()
        {
            Logger.Trace("ObjectAccessor created");
        }

        internal void Add(IReferenceObject obj)
        {
            lock (_objects)
            {
                if (_objects.ContainsKey(obj.ReferenceID))
                {
                    Logger.Warning("Object {0} already exists", obj.ReferenceID);
                    return;
                }

                _objects.Add(obj.ReferenceID, obj);
            }
        }

        internal void Remove(GameObject obj)
        {
            lock (_objects)
            {
                _objects.Remove(obj.ReferenceID);
            }
        }

        /// <summary>
        /// Liefert ein Spieloobjekt anhand der ID
        /// </summary>
        /// <param name="objid"></param>
        /// <returns></returns>
        public GameObject GetObject(uint objid)
        {
            lock (_objects)
            {
                if (!_objects.ContainsKey(objid))
                {
                    Logger.Warning("Object '{0}' not found", objid);
                    return null;
                }

                return _objects[objid] as GameObject;
            }
        }

        /// <summary>
        /// Liefert ein Spielobjekt anhand der ID un konvertiert es in den korrekten Objekt-Typ
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="objid"></param>
        /// <returns></returns>
        public TObject GetObject<TObject>(uint objid) where TObject : GameObject
        {
            return GetObject(objid) as TObject;
        }

        /// <summary>
        /// Liefert eine Liste aller Spielobjekte die den angegebenen Objekt-Typ entsprechen
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public IEnumerable<TObject> GetObjects<TObject>() where TObject : GameObject
        {
            lock (_objects)
            {
                return _objects.Values.Where(m => m is TObject).Cast<TObject>();
            }
        }

        /// <summary>
        /// Liefert die Anzahl aller Spielobjekte
        /// </summary>
        public int Count { get { return _objects.Count; } }

        /// <summary>
        /// Gibt an ob ein Spielobjekt bereits Teil der Verwaltung ist
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool ContainsObject<TObject>(TObject obj) where TObject : GameObject
        {
            return _objects.ContainsKey(obj);
        }
    }
}
