﻿using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Handler
{
    /// <summary>
    /// Stellt einen Handler für das Map-System dar
    /// </summary>
    public sealed class SystemMapHandler
    {
        private Dictionary<uint, GalaxyObject> _systems = new Dictionary<uint, GalaxyObject>();

        public SystemMapHandler(WorldManager world)
        {
            World = world;
            Engine = world.Engine;

            SystemMapSizeX = world.Engine.Configuration.GetValue("System", "MapX", 500F);
            SystemMapSizeY = world.Engine.Configuration.GetValue("System", "MapY", 500F);

            Logger.Trace("SystemMapHandler created");
        }

        public PandoraEngine Engine { get; private set; }

        public WorldManager World { get; private set; }

        public void Add(GalaxyObject node, SystemObject obj)
        {
            //switch (World.GameType)
            //{
            //    case GameTypes.Singleplayer:
            //        InternalAdd(node, obj);
            //        break;

            //    case GameTypes.MultiplayerClient:
            //        {
            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.CMSG_WORLD_SYSTEM_ADDOBJECT);
            //            pout.WriteUInt32(node.ReferenceID);
            //            obj.Serialize(pout);
            //            World.Broadcast(pout);
            //        }
            //        break;

            //    case GameTypes.MultiplayerServer:
            //        {
            //            InternalAdd(node, obj);

            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.SMSG_WORLD_SYSTEM_ADDOBJECT);
            //            pout.WriteUInt32(node.ReferenceID);
            //            obj.Serialize(pout);
            //            World.Broadcast(pout);
            //        }
            //        break;
            //}
        }

        public void Remove(SystemObject obj)
        {
            //switch (World.GameType)
            //{
            //    case GameTypes.Singleplayer:
            //        InternalRemove(obj);
            //        break;

            //    case GameTypes.MultiplayerClient:
            //        {
            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.CMSG_WORLD_SYSTEM_REMOVEOBJECT);
            //            pout.WriteUInt32(obj.ReferenceID);
            //            World.Broadcast(pout);
            //        }
            //        break;

            //    case GameTypes.MultiplayerServer:
            //        {
            //            InternalRemove(obj);

            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.SMSG_WORLD_SYSTEM_REMOVEOBJECT);
            //            pout.WriteUInt32(obj.ReferenceID);
            //            World.Broadcast(pout);
            //        }
            //        break;
            //}
        }

        public void ChangeMap(GalaxyObject node, SystemObject obj)
        {
            //switch (World.GameType)
            //{
            //    case GameTypes.Singleplayer:
            //        InternalChangeMap(node, obj);
            //        break;

            //    case GameTypes.MultiplayerClient:
            //        {
            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.CMSG_WORLD_SYSTEM_CHANGEMAPOBJECT);
            //            pout.WriteUInt32(node.ReferenceID);
            //            pout.WriteUInt32(obj.ReferenceID);
            //            World.Broadcast(pout);
            //        }
            //        break;

            //    case GameTypes.MultiplayerServer:
            //        {
            //            InternalChangeMap(node, obj);

            //            var pout = Packets.GetPacket();
            //            pout.WriteOpCode(OpCodes.SMSG_WORLD_SYSTEM_CHANGEMAPOBJECT);
            //            pout.WriteUInt32(node.ReferenceID);
            //            pout.WriteUInt32(obj.ReferenceID);
            //            World.Broadcast(pout);
            //        }
            //        break;
            //    default:
            //        break;
            //}
        }

        /// <summary>
        /// Übergibt ein Objekt an eine Map
        /// </summary>
        /// <param name="node"></param>
        /// <param name="obj"></param>
        internal void InternalAdd(GalaxyObject node, SystemObject obj)
        {
            // Prüfen ob das Objekt bereits angemeldet ist
            if (World.ObjectAccessor.ContainsObject(obj))
            {
                // Es ist also schon da, dann nur die MapNode ändern
                Logger.Warning("Object '{0}' already exists.", obj.ReferenceID);
            }
            else
            {
                World.ObjectAccessor.Add(obj);    // Das Objekt am Manager anmelden
                node.Add(obj);                  // Das Objekt der Node zuweisen
                obj.Node = node;                 // Dem Objekt die Node zuweisen

                Logger.Debug("Object '{0}' added to '{1}'", obj.Name, node.Name);
            }
        }

        /// <summary>
        /// Entfernt ein Objekt
        /// </summary>
        /// <param name="obj"></param>
        internal void InternalRemove(SystemObject obj)
        {
            // Prüfen ob das Objekt bereits angemeldet ist
            if (!World.ObjectAccessor.ContainsObject(obj))
            {
                // Es ist also schon da, dann nur die MapNode ändern
                Logger.Warning("Object '{0}' not exists", obj.ReferenceID);
            }
            else
            {
                World.ObjectAccessor.Remove(obj);
                obj.Node.Remove(obj);

                Logger.Debug("Object '{0}' removed from '{1}'", obj.Name, obj.Node.Name);
            }
        }

        /// <summary>
        /// Wechselt eine Map
        /// </summary>
        /// <param name="node"></param>
        /// <param name="obj"></param>
        internal void InternalChangeMap(GalaxyObject node, SystemObject obj)
        {
            // Wenn die aktuelle Map gleich die Zielmap ist nichts machen
            if (node == obj.Node) return;

            // Das Objekt aus der aktuellen Map nehmen
            obj.Node.Remove(obj);
            obj.Node = node;
            obj.Node.Add(obj);

            Logger.Debug("Object '{0}' changed map to '{1}'", obj.Name, obj.Node.Name);
        }

        /// <summary>
        /// Gibt an ob eine Map verfügbar ist
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public bool ContainsMap(GalaxyObject node)
        {
            return _systems.ContainsKey(node);
        }

        /// <summary>
        /// Liefert eine MapNode
        /// </summary>
        /// <typeparam name="TNode"></typeparam>
        /// <param name="objid"></param>
        /// <returns></returns>
        public TNode GetNode<TNode>(uint objid) where TNode : GalaxyObject
        {
            if (!_systems.ContainsKey(objid))
            {
                Logger.Warning("Map '{0}' not found", objid);
                return null;
            }

            return _systems[objid] as TNode;
        }

        /// <summary>
        /// Liefert die Anzahl der MapNodes
        /// </summary>
        public int Count { get { return _systems.Count; } }

        public float SystemMapSizeX { get; private set; }

        public float SystemMapSizeY { get; private set; }

        internal void SystemUpdate(float ms, float s)
        {
            //TODO: Aktivierte MapNode durchgehen
        }


    }
}
