﻿using Pandora.Game.World;
using Pandora.Game.World.Objects;
using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Players
{
    public class Player
    {
        /// <summary>
        /// Erstellt eine neue Instanz der Player-Klasse
        /// </summary>
        /// <param name="playername"></param>
        public Player(string name)
        {
            GUID = Guid.NewGuid().ToString();
            Name = name;
        }

        internal Player(string guid, string name)
        {
            GUID = guid;
            Name = name;
        }

        #region System

        /// <summary>
        /// Liefert den Spielernamen
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Liefert die Spieler GUID
        /// </summary>
        public string GUID { get; internal set; }

        /// <summary>
        /// Git an ob es sich um einen Netzwerkspieler handelt
        /// </summary>
        public bool IsNetwork { get { return Client != null; } }

        /// <summary>
        /// Liefert den Netzwerk-Socket
        /// </summary>
        public IClient Client { get; internal set; }

        /// <summary>
        /// Gibt den Spielertyp an
        /// </summary>
        public PlayerTypeFlags PlayerFlags { get; internal set; }

        /// <summary>
        /// Gibt an das der Spieler bereit für das spiel ist
        /// </summary>
        public bool IsReady { get; internal set; }

        /// <summary>
        /// Gibt eine Zeichenfolge zurück, die das aktuelle Objekt darstellt.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var multiplayer = ((PlayerFlags & PlayerTypeFlags.Multiplayer) == PlayerTypeFlags.Multiplayer);
            var client = ((PlayerFlags & PlayerTypeFlags.Client) == PlayerTypeFlags.Client);
            var singleplayer = ((PlayerFlags & PlayerTypeFlags.Singleplayer) == PlayerTypeFlags.Singleplayer);
            var administrator = ((PlayerFlags & PlayerTypeFlags.Administrator) == PlayerTypeFlags.Administrator);

            return string.Format("Player '{0}' ({1}{2}{3}{4}{5})", Name, singleplayer ? "S" : "-", multiplayer ? "M" : "-", client ? "C" : "-", administrator ? "A" : "-", IsReady ? "R" : "-");
        }

        #endregion

        #region RaceValues

        /// <summary>
        /// Gibt an ob eine Sauerstoff-Atmosphäre benötigt wird oder legt es fest
        /// </summary>
        public bool AtmosphereOxygen { get; set; }

        /// <summary>
        /// Gibt an ob eine Stickstoff-Atmosphäre benötigt wird oder legt es fest
        /// </summary>
        public bool AtmosphereNitrogen { get; set; }

        /// <summary>
        /// Gibt an ob eine Methan-Atmosphäre benötigt wird oder legt es fest
        /// </summary>
        public bool AtmosphereMethane { get; set; }

        /// <summary>
        /// Gibt an ob eine Ammoniak-Atmosphäre benötigt wird oder legt es fest
        /// </summary>
        public bool AtmosphereAmmonia { get; set; }

        /// <summary>
        /// Gibt an ob eine Kohlendioxid-Atmosphäre benötigt wird oder legt es fest
        /// </summary>
        public bool AtmosphereCarbonDioxid { get; set; }

        /// <summary>
        /// Gibt an das die Rasse keine Atmosphäre benötigt
        /// </summary>
        public bool Anaerobic { get; set; }

        /// <summary>
        /// Gibt an das die Rasse unabhängig der Temperatur lebt oder legt es fest
        /// </summary>
        public bool Subterranean { get; set; }

        /// <summary>
        /// Gibt an das die Gesamte Sternkarte aufgedeckt ist
        /// </summary>
        public bool Psychic { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Predator { get; set; }

        /// <summary>
        /// Liefert einen Bonus für Angriffe oder legt ihn fest
        /// </summary>
        public float AttackBonus { get; set; }

        /// <summary>
        /// Liefert einen Bonus für Verteidigung oder legt ihn fest
        /// </summary>
        public float DefanceBonus { get; set; }

        /// <summary>
        /// Liefert einen Bonus für Konstruktionen oder legt ihn fest
        /// </summary>
        public float ConstructionBonus { get; set; }

        /// <summary>
        /// Liefert einen Bonus für Forschung oder legt ihn fest
        /// </summary>
        public float ResearchBonus { get; set; }

        /// <summary>
        /// Liefert einen Bonus für Spionage oder legt ihn fest
        /// </summary>
        public float EsponageBonus { get; set; }

        /// <summary>
        /// Liefert einen Bonus für Schiff-Konstruktionen oder legt ihn fest
        /// </summary>
        public float ShipConstructionBonus { get; set; }

        /// <summary>
        /// Liefert einen Bonus für Population oder legt ihn fest
        /// </summary>
        public float ReproductionBonus { get; set; }

        /// <summary>
        /// Liefert einen Bonus für Wirtschaft oder legt ihn fest
        /// </summary>
        public float FinanceBonus { get; set; }

        /// <summary>
        /// Liefert die bevorzugte minimale Temperatur oder legt sie fest
        /// </summary>
        public float MinTemperature { get; set; }

        /// <summary>
        /// Liefert die bevorzugte maximale Temperatur oder legt sie fest
        /// </summary>
        public float MaxTemperature { get; set; }

        #endregion

        internal bool Serialize(IWriter writer)
        {
            writer.WriteString(Name);
            writer.WriteString(GUID);

            writer.WriteBool(AtmosphereOxygen);
            writer.WriteBool(AtmosphereNitrogen);
            writer.WriteBool(AtmosphereMethane);
            writer.WriteBool(AtmosphereAmmonia);
            writer.WriteBool(AtmosphereCarbonDioxid);
            writer.WriteBool(Anaerobic);
            writer.WriteBool(Subterranean);
            writer.WriteBool(Psychic);
            writer.WriteBool(Predator);
            writer.WriteFloat(AttackBonus);
            writer.WriteFloat(DefanceBonus);
            writer.WriteFloat(ConstructionBonus);
            writer.WriteFloat(ResearchBonus);
            writer.WriteFloat(EsponageBonus);
            writer.WriteFloat(ShipConstructionBonus);
            writer.WriteFloat(ReproductionBonus);
            writer.WriteFloat(FinanceBonus);
            writer.WriteFloat(MinTemperature);
            writer.WriteFloat(MaxTemperature);

            return true;
        }

        internal bool Deserialize(IReader reader, WorldManager world)
        {
            Name = reader.ReadString();
            GUID = reader.ReadString();

            AtmosphereOxygen = reader.ReadBool();
            AtmosphereNitrogen = reader.ReadBool();
            AtmosphereMethane = reader.ReadBool();
            AtmosphereAmmonia = reader.ReadBool();
            AtmosphereCarbonDioxid = reader.ReadBool();
            Anaerobic = reader.ReadBool();
            Subterranean = reader.ReadBool();
            Psychic = reader.ReadBool();
            Predator = reader.ReadBool();
            AttackBonus = reader.ReadFloat();
            DefanceBonus = reader.ReadFloat();
            ConstructionBonus = reader.ReadFloat();
            ResearchBonus = reader.ReadFloat();
            EsponageBonus = reader.ReadFloat();
            ShipConstructionBonus = reader.ReadFloat();
            ReproductionBonus = reader.ReadFloat();
            FinanceBonus = reader.ReadFloat();
            MinTemperature = reader.ReadFloat();
            MaxTemperature = reader.ReadFloat();

            return true;
        }
    }
}
