﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Players
{
    public sealed class PlayerManager
    {
        private PandoraEngine _engine;
        private Dictionary<string, Player> _players = new Dictionary<string, Player>();

        internal PlayerManager(PandoraEngine engine)
        {
            _engine = engine;

            Logger.Trace("Playermanager created");
        }
    }
}
