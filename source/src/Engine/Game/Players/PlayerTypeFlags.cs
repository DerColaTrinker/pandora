﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Players
{
    /// <summary>
    /// Spielertypen-Flags
    /// </summary>
    [Flags]
    public enum PlayerTypeFlags : byte
    {
        /// <summary>
        /// Singleplayer
        /// </summary>
        Singleplayer = 1,

        /// <summary>
        /// Multiplayer 
        /// </summary>
        Multiplayer = 2,

        /// <summary>
        /// Client
        /// </summary>
        Client = 4,

        /// <summary>
        /// Administrator
        /// </summary>
        Administrator = 16
    }
}
