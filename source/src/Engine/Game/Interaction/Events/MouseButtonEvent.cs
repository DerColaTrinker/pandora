﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.Events
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct MouseButtonEvent
    {
        public MouseButton Button;

        public int X;

        public int Y;
    }
}
