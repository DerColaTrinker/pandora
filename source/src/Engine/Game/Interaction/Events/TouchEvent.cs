﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.Events
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct TouchEvent
    {
        public uint Finger;

        public int X;

        public int Y;
    }
}
