﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Interaction.GUI.Drawing;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TexturedControl : TransformableControl
    {
        private Texture _texture;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate TextureChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate TextureRectChanged;

        /// <summary>
        /// 
        /// </summary>
        public TexturedControl()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public TexturedControl(TexturedControl copy)
        {
            Texture = copy.Texture;
            TextureRect = copy.TextureRect;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Texture Texture
        {
            get { return _texture; }
            set
            {
                _texture = value;
                NativeSFMLCalls.sfShape_setTexture(Pointer, value != null ? value.Pointer : IntPtr.Zero, false);
                OnTextureChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual IntRect TextureRect
        {
            get { return NativeSFMLCalls.sfShape_getTextureRect(Pointer); }
            set
            {
                NativeSFMLCalls.sfShape_setTextureRect(Pointer, value);
                OnTextureRectChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnTextureChanged()
        {
            if (TextureChanged != null) TextureChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnTextureRectChanged()
        {
            if (TextureRectChanged != null) TextureRectChanged.Invoke(this);
        }
    }
}
