﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Interaction.GUI.Animations;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public class SpriteControl : TexturedControl, IColorProperty
    {
        private Texture _texture;

        /// <summary>
        /// 
        /// </summary>
        public SpriteControl()
        {
            InternalPointer = NativeSFMLCalls.sfSprite_create();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        public SpriteControl(Texture texture)
            : this()
        {
            Texture = texture;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="rectangle"></param>
        public SpriteControl(Texture texture, IntRect rectangle) :
            this()
        {
            Texture = texture;
            TextureRect = rectangle;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public SpriteControl(SpriteControl copy)
        {
            InternalPointer = NativeSFMLCalls.sfSprite_copy(copy.Pointer);
            Origin = copy.Origin;
            Position = copy.Position;
            Rotation = copy.Rotation;
            Scale = copy.Scale;
            Texture = copy.Texture;

        }

        /// <summary>
        /// 
        /// </summary>
        public Color Color
        {
            get { return NativeSFMLCalls.sfSprite_getColor(Pointer); }
            set { NativeSFMLCalls.sfSprite_setColor(Pointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Texture Texture
        {
            get { return _texture; }
            set { _texture = value; NativeSFMLCalls.sfSprite_setTexture(Pointer, value != null ? value.Pointer : IntPtr.Zero, false); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override IntRect TextureRect
        {
            get { return NativeSFMLCalls.sfSprite_getTextureRect(Pointer); }
            set { NativeSFMLCalls.sfSprite_setTextureRect(Pointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override FloatRect GetLocalBounds()
        {
            return NativeSFMLCalls.sfSprite_getLocalBounds(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override FloatRect GetGlobalBounds()
        {
            // we don't use the native getGlobalBounds function,
            // because we override the object's transform
            return Transform.TransformRect(GetLocalBounds());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[Sprite]" +
                   " Color(" + Color + ")" +
                   " Texture(" + Texture + ")" +
                   " TextureRect(" + TextureRect + ")";
        }

        internal override void InternalRenderUpdate(IRenderTarget target)
        {
            base.InternalRenderUpdate(target);

            var state = RenderStates.Default.Marshal();
            NativeSFMLCalls.sfRenderWindow_drawSprite(target.Pointer, Pointer, ref state);
        }
    }
}
