﻿using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public class LineControl : Control
    {
        private LineDirection _direction;
        private Vector2f _p1;
        private Vector2f _p2;
        private Vertex[] _vertices = new Vertex[4];
        private Drawing.Color _color = Color.White;
        private float _thickness = 1;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate ColorChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate ThicknessChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate DirectionChanged;

        /// <summary>
        /// 
        /// </summary>
        public LineControl()
        {
            Direction = LineDirection.LeftToRight;
            Update();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        protected override void OnRenderUpdate(IRenderTarget target)
        {
            base.OnRenderUpdate(target);

            target.Draw(_vertices, PrimitiveType.Quads);
        }

        private void Update()
        {
            switch (_direction)
            {
                case LineDirection.VectorPoint:
                case LineDirection.LeftToRight:
                    _p1 = ScreenPosition;
                    _p2 = ScreenPosition + Size;
                    break;

                case LineDirection.RightToLeft:
                    _p1 = new Vector2f(ScreenPosition.X + Size.X, ScreenPosition.Y);
                    _p2 = new Vector2f(ScreenPosition.X, ScreenPosition.Y + Size.Y);
                    break;

                case LineDirection.Top:
                    _p1 = new Vector2f(ScreenPosition.X, ScreenPosition.Y);
                    _p2 = new Vector2f(ScreenPosition.X + Size.X, ScreenPosition.Y);
                    break;

                case LineDirection.Bottom:
                    _p1 = new Vector2f(ScreenPosition.X, ScreenPosition.Y + Size.Y);
                    _p2 = new Vector2f(ScreenPosition.X + Size.X, ScreenPosition.Y + Size.Y);
                    break;

                case LineDirection.Left:
                    _p1 = new Vector2f(ScreenPosition.X, ScreenPosition.Y);
                    _p2 = new Vector2f(ScreenPosition.X, ScreenPosition.Y + Size.Y);
                    break;

                case LineDirection.Right:
                    _p1 = new Vector2f(ScreenPosition.X + Size.X, ScreenPosition.Y);
                    _p2 = new Vector2f(ScreenPosition.X + Size.X, ScreenPosition.Y + Size.Y);
                    break;

                case LineDirection.Horizontal:
                    _p1 = new Vector2f(ScreenPosition.X, ScreenPosition.Y + (Size.Y / 2F));
                    _p2 = new Vector2f(ScreenPosition.X + Size.X, ScreenPosition.Y + (Size.Y / 2F));
                    break;

                case LineDirection.Vertical:
                    _p1 = new Vector2f(ScreenPosition.X + (Size.X / 2F), ScreenPosition.Y);
                    _p2 = new Vector2f(ScreenPosition.X + (Size.X / 2F), ScreenPosition.Y + Size.Y);
                    break;
            }

            var direction = _p2 - _p1;
            var unitDirection = direction / (float)Math.Sqrt(direction.X * direction.X + direction.Y * direction.Y);
            var unitPerpendicular = new Vector2f(-unitDirection.Y, unitDirection.X);
            var offset = (_thickness / 2F) * unitPerpendicular;

            _vertices[0].Position = _p1 + offset;
            _vertices[1].Position = _p2 + offset;
            _vertices[2].Position = _p2 - offset;
            _vertices[3].Position = _p1 - offset;

            for (int i = 0 ; i < 4 ; ++i)
                _vertices[i].Color = _color;
        }

        /// <summary>
        /// 
        /// </summary>
        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Vector2f Position
        {
            get
            {
                return base.Position;
            }
            set
            {
                base.Position = value;
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual float Thickness
        {
            get { return _thickness; }
            set { _thickness = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public LineDirection Direction
        {
            get { return _direction; }
            set
            {
                _direction = value;
                Update();
                OnDirectionChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnColorChanged()
        {
            if (ColorChanged != null) ColorChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnThicknessChanged()
        {
            if (ThicknessChanged != null) ThicknessChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnDirectionChanged()
        {
            if (DirectionChanged != null) DirectionChanged.Invoke(this);
        }
    }
}
