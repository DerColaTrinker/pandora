using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;
using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public class VertexArray : TransformableControl
    {
        /// <summary>
        /// 
        /// </summary>
        public VertexArray()
        {
            InternalPointer = NativeSFMLCalls.sfVertexArray_create();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public VertexArray(PrimitiveType type) :
            this()
        {
            PrimitiveType = type;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="vertexCount"></param>
        public VertexArray(PrimitiveType type, uint vertexCount) :
            this()
        {
            PrimitiveType = type;
            Resize(vertexCount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public VertexArray(VertexArray copy)
        {
            InternalPointer = NativeSFMLCalls.sfVertexArray_copy(copy.InternalPointer);
        }

        /// <summary>
        /// 
        /// </summary>
        public uint VertexCount
        {
            get { return NativeSFMLCalls.sfVertexArray_getVertexCount(InternalPointer); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Vertex this[uint index]
        {
            get
            {
                unsafe
                {
                    return *NativeSFMLCalls.sfVertexArray_getVertex(InternalPointer, index);
                }
            }
            set
            {
                unsafe
                {
                    *NativeSFMLCalls.sfVertexArray_getVertex(InternalPointer, index) = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            NativeSFMLCalls.sfVertexArray_clear(InternalPointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertexCount"></param>
        public void Resize(uint vertexCount)
        {
            NativeSFMLCalls.sfVertexArray_resize(InternalPointer, vertexCount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertex"></param>
        public void Append(Vertex vertex)
        {
            NativeSFMLCalls.sfVertexArray_append(InternalPointer, vertex);
        }

        /// <summary>
        /// 
        /// </summary>
        public PrimitiveType PrimitiveType
        {
            get { return NativeSFMLCalls.sfVertexArray_getPrimitiveType(InternalPointer); }
            set { NativeSFMLCalls.sfVertexArray_setPrimitiveType(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override FloatRect Bounds
        {
            get { return NativeSFMLCalls.sfVertexArray_getBounds(InternalPointer); }
        }

        internal override void InternalRenderUpdate(IRenderTarget target)
        {
            var state = new RenderStates(Transform);

            target.DrawVertexArray(Pointer, state);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            NativeSFMLCalls.sfVertexArray_destroy(InternalPointer);
        }
    }
}
