﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Interaction.GUI.Animations;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TransformableControl : Control, IRotationProperty, IScaleProperty, IOriginProperty
    {
        private Vector2f _origin = new Vector2f(0, 0);
        private float _rotation = 0;
        private Vector2f _scale = new Vector2f(1, 1);
        private Transform _transform;
        private Transform _inversetransform;
        private bool _transformneedupdate = true;
        private bool _inverseneedupdate = true;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate RotationChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate ScaleChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate OriginChanged;

        /// <summary>
        /// 
        /// </summary>
        public override Vector2f Position
        {
            get
            {
                return base.Position;
            }

            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                base.Position = value;
            }
        }

        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                base.Size = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual float Rotation
        {
            get
            {
                return _rotation;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _rotation = value;
                OnRotationChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Vector2f Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _scale = value;
                OnScaleChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Vector2f Origin
        {
            get
            {
                return _origin;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _origin = value;

                OnOriginChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Transform Transform
        {
            get
            {
                if (_transformneedupdate)
                {
                    _transformneedupdate = false;

                    var angle = -_rotation * 3.141592654F / 180.0F;
                    var cosine = (float)Math.Cos(angle);
                    var sine = (float)Math.Sin(angle);
                    var sxc = _scale.X * cosine;
                    var syc = _scale.Y * cosine;
                    var sxs = _scale.X * sine;
                    var sys = _scale.Y * sine;
                    var tx = -_origin.X * sxc - _origin.Y * sys + ScreenPosition.X;
                    var ty = _origin.X * sxs - _origin.Y * syc + ScreenPosition.Y;

                    _transform = new Transform(sxc, sys, tx, -sxs, syc, ty, 0.0F, 0.0F, 1.0F);
                }

                return _transform;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Transform InverseTransform
        {
            get
            {
                if (_inverseneedupdate)
                {
                    _inversetransform = Transform.GetInverse();
                    _inverseneedupdate = false;
                }
                return _inversetransform;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        protected virtual Transform AlternativPositionTransformation(Vector2f position)
        {
            var angle = -_rotation * 3.141592654F / 180.0F;
            var cosine = (float)Math.Cos(angle);
            var sine = (float)Math.Sin(angle);
            var sxc = _scale.X * cosine;
            var syc = _scale.Y * cosine;
            var sxs = _scale.X * sine;
            var sys = _scale.Y * sine;
            var tx = -_origin.X * sxc - _origin.Y * sys + position.X;
            var ty = _origin.X * sxs - _origin.Y * syc + position.Y;

            return new Transform(sxc, sys, tx, -sxs, syc, ty, 0.0F, 0.0F, 1.0F);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual FloatRect GetLocalBounds()
        {
            return Transform.TransformRect(Bounds);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual FloatRect GetGlobalBounds()
        {
            return Transform.TransformRect(ScreenBounds);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnRotationChanged()
        {
            if (RotationChanged != null) RotationChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnScaleChanged()
        {
            if (ScaleChanged != null) ScaleChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnOriginChanged()
        {
            if (OriginChanged != null) OriginChanged.Invoke(this);
        }
    }
}
