﻿using Pandora.Game.Interaction.GUI.Animations;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public class RectShapeControl : BorderShape
    {
        /// <summary>
        /// 
        /// </summary>
        public RectShapeControl()
            : this(new Vector2f(0, 0))
        {
            base.PointCount = 4;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        public RectShapeControl(Vector2f size)
        {
            Size = size;
            base.PointCount = 4;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public RectShapeControl(RectShapeControl copy) :
            base(copy)
        {
            Size = copy.Size;
            base.PointCount = 4;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override uint PointCount
        {
            get
            {
                return base.PointCount;
            }
            set
            {
                // Darf nicht geändert werden...
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected override Vector2f GetPoint(uint index)
        {
            switch (index)
            {
                default:
                case 0: return new Vector2f(0, 0);
                case 1: return new Vector2f(base.Size.X, 0);
                case 2: return new Vector2f(base.Size.X, base.Size.Y);
                case 3: return new Vector2f(0, base.Size.Y);
            }
        }
    }
}
