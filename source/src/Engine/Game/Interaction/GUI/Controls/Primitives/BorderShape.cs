﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Interaction.GUI.Animations;
using Pandora.Game.Interaction.GUI.Drawing;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BorderShape : ShapeControl, IFillColorProperty, IOutlineColorProperty
    {
        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate FillColorChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate OutlineColorChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate OutlineThicknessChanged;

        /// <summary>
        /// 
        /// </summary>
        public BorderShape()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public BorderShape(BorderShape copy)
            : base(copy)
        {
            FillColor = copy.FillColor;
            OutlineColor = copy.OutlineColor;
            OutlineThickness = copy.OutlineThickness;
        }

        /// <summary>
        /// 
        /// </summary>
        public Color FillColor
        {
            get { return NativeSFMLCalls.sfShape_getFillColor(Pointer); }
            set
            {
                NativeSFMLCalls.sfShape_setFillColor(Pointer, value);
                OnFillColorChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Color OutlineColor
        {
            get { return NativeSFMLCalls.sfShape_getOutlineColor(Pointer); }
            set
            {
                NativeSFMLCalls.sfShape_setOutlineColor(Pointer, value);
                OnOutlineColorChanged();
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float OutlineThickness
        {
            get { return NativeSFMLCalls.sfShape_getOutlineThickness(Pointer); }
            set
            {
                NativeSFMLCalls.sfShape_setOutlineThickness(Pointer, value);
                OnOutlineThicknessChanged();
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnFillColorChanged()
        {
            if (FillColorChanged != null) FillColorChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnOutlineColorChanged()
        {
            if (OutlineColorChanged != null) OutlineColorChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnOutlineThicknessChanged()
        {
            if (OutlineThicknessChanged != null) OutlineThicknessChanged.Invoke(this);
        }
    }
}
