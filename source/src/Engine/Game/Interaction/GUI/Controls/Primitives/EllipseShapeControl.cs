﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public class EllipseShapeControl : BorderShape
    {
        /// <summary>
        /// 
        /// </summary>
        public EllipseShapeControl()
            : this(new Vector2f())
        {
            PointCount = 30;
        }

        /// <summary>
        /// Erstellt eine neue Instanz der CircleShapeControl-Klasse
        /// </summary>
        public EllipseShapeControl(Vector2f size)
        {
            Size = size;
            PointCount = 30;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public EllipseShapeControl(EllipseShapeControl copy) :
            base(copy)
        {
            Size = copy.Size;
            PointCount = 30;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected override Vector2f GetPoint(uint index)
        {
            var angle = (float)(index * 2F * Math.PI / PointCount - Math.PI / 2F);
            var x = (float)(Math.Cos(angle) * Size.X);
            var y = (float)(Math.Sin(angle) * Size.Y);

            return new Vector2f(Size.X + x, Size.Y + y);
        }
    }
}
