﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    [Flags]
    public enum TextStyle
    {
        /// <summary>
        /// 
        /// </summary>
        Regular = 0,

        /// <summary>
        /// 
        /// </summary>
        Bold = 1 << 0,

        /// <summary>
        /// 
        /// </summary>
        Italic = 1 << 1,

        /// <summary>
        /// 
        /// </summary>
        Underlined = 1 << 2,

        /// <summary>
        /// 
        /// </summary>
        StrikeThrough = 1 << 3
    }
}
