﻿using Pandora.Game.Interaction.GUI.Animations;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// Stellt ein Text Steuerelement dar
    /// </summary>
    public class TextControl : TransformableControl, IColorProperty
    {
        private TextAlignment _textalignment = TextAlignment.TopLeft;
        private Font _font;

        /// <summary>
        /// Wird aufgerufen wenn die Schriftfarbe geändert wird
        /// </summary>
        public event ControlEventDelegate ColorChanged;

        /// <summary>
        /// Wird aufgerufen wenn der Text geändert wird
        /// </summary>
        public event ControlEventDelegate TextChanged;

        /// <summary>
        /// Wird aufgerufen wenn der Schriftstiel geändert wird
        /// </summary>
        public event ControlEventDelegate StyleChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate FontSizeChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ControlEventDelegate FontChanged;

        /// <summary>
        /// Erstellt eine neue Instanz der TextControl-Klasse
        /// </summary>
        public TextControl()
        {
            AutoSize = true;

            InternalPointer = NativeSFMLCalls.sfText_create();
        }

        private void ReSetBounds()
        {
            TextBounds = GetLocalBounds();

            // Die größe des Bereichs nur übergeben wenn das Autosize ausgeschaltet ist.
            if (AutoSize)
            {
                base.Size = new Vector2f(TextBounds.Width, TextBounds.Height);
                TextPosition = ScreenPosition;
            }
            else
            {
                ResetTextPosition();
            }
        }

        private void ResetTextPosition()
        {
            switch (TextAlignment)
            {
                case TextAlignment.TopLeft: TextPosition = new Vector2f(ScreenPosition.X - TextBounds.Left, ScreenPosition.Y - TextBounds.Top); break;
                case TextAlignment.TopCenter: TextPosition = new Vector2f(ScreenPosition.X + (Size.X / 2F) - ((TextBounds.Left + TextBounds.Width) / 2F), ScreenPosition.Y - TextBounds.Top); break;
                case TextAlignment.TopRight: TextPosition = new Vector2f(ScreenPosition.X + Size.X - (TextBounds.Left + TextBounds.Width), ScreenPosition.Y - TextBounds.Top); break;

                case TextAlignment.MiddleLeft: TextPosition = new Vector2f(ScreenPosition.X - TextBounds.Left, ScreenPosition.Y + (Size.Y / 2F) - ((TextBounds.Top + TextBounds.Height) / 2F)); break;
                case TextAlignment.MiddleCenter: TextPosition = new Vector2f(ScreenPosition.X + (Size.X / 2F) - ((TextBounds.Left + TextBounds.Width) / 2F), ScreenPosition.Y + (Size.Y / 2F) - ((TextBounds.Top + TextBounds.Height) / 2F)); break;
                case TextAlignment.MiddelRight: TextPosition = new Vector2f(ScreenPosition.X + Size.X - (TextBounds.Left + TextBounds.Width), ScreenPosition.Y + (Size.Y / 2F) - ((TextBounds.Top + TextBounds.Height) / 2F)); break;

                case TextAlignment.BottomLeft: TextPosition = new Vector2f(ScreenPosition.X - TextBounds.Left, ScreenPosition.Y + Size.Y - (TextBounds.Top + TextBounds.Height)); break;
                case TextAlignment.BottomCenter: TextPosition = new Vector2f(ScreenPosition.X + (Size.X / 2F) - (TextBounds.Width / 2F), ScreenPosition.Y + Size.Y - (TextBounds.Top + TextBounds.Height)); break;
                case TextAlignment.BottomRight: TextPosition = new Vector2f(ScreenPosition.X + Size.X - TextBounds.Width, ScreenPosition.Y + Size.Y - (TextBounds.Top + TextBounds.Height)); break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override FloatRect GetGlobalBounds()
        {
            return Transform.TransformRect(GetLocalBounds());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override FloatRect GetLocalBounds()
        {
            return NativeSFMLCalls.sfText_getLocalBounds(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Vector2f FindCharacterPos(uint index)
        {
            return NativeSFMLCalls.sfText_findCharacterPos(Pointer, index);
        }

        internal Vector2f TextPosition
        {
            get;
            set;
        }

        /// <summary>
        /// Liefert die Ausmaße des Textes
        /// </summary>
        public FloatRect TextBounds { get; private set; }

        /// <summary>
        /// Gibt an ob die Größe des Steuerelements sich des Textes anpassen sollen oder legt es fest
        /// </summary>
        public virtual bool AutoSize { get; set; }

        /// <summary>
        /// Liefert die Schriftfarbe oder legt sie fest
        /// </summary>
        public Color Color
        {
            get
            {
                return NativeSFMLCalls.sfText_getColor(Pointer);
            }
            set
            {
                NativeSFMLCalls.sfText_setColor(Pointer, value);
                OnColorChanged();
            }
        }

        /// <summary>
        /// Liefert den Text der Angezeigt wird order legt ihn fest
        /// </summary>
        public virtual string Text
        {
            get
            {
                // Get a pointer to the source string (UTF-32)
                IntPtr source = NativeSFMLCalls.sfText_getUnicodeString(Pointer);

                // Find its length (find the terminating 0)
                uint length = 0;
                unsafe
                {
                    for (uint* ptr = (uint*)source.ToPointer() ; *ptr != 0 ; ++ptr)
                        length++;
                }

                // Copy it to a byte array
                byte[] sourceBytes = new byte[length * 4];
                Marshal.Copy(source, sourceBytes, 0, sourceBytes.Length);

                // Convert it to a C# string
                return Encoding.UTF32.GetString(sourceBytes);
            }

            set
            {
                // Copy the string to a null-terminated UTF-32 byte array
                byte[] utf32 = Encoding.UTF32.GetBytes(value + '\0');

                // Pass it to the C API
                unsafe
                {
                    fixed (byte* ptr = utf32)
                    {
                        NativeSFMLCalls.sfText_setUnicodeString(Pointer, (IntPtr)ptr);
                    }
                }
            }
        }

        /// <summary>
        /// Liefert die Schiftgröße oder legt sie fest
        /// </summary>
        public virtual uint FontSize
        {
            get
            {
                return NativeSFMLCalls.sfText_getCharacterSize(Pointer);
            }
            set
            {
                NativeSFMLCalls.sfText_setCharacterSize(Pointer, value);
                ReSetBounds();
                OnFontSizeChanged();
            }
        }

        /// <summary>
        /// Gibt die aktuelle Schriftart zurück oder legt sie fest
        /// </summary>
        public virtual Font Font
        {
            get
            {
                return _font;
            }
            set
            {
                _font = value;
                NativeSFMLCalls.sfText_setFont(Pointer, value.Pointer);
                OnFontChanged();
            }
        }

        /// <summary>
        /// Gibt die Position des Steuerelements zurück oder legt es fest
        /// </summary>
        public override Vector2f Position
        {
            get
            {
                return base.Position;
            }
            set
            {
                base.Position = value;
                ReSetBounds();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public TextAlignment TextAlignment
        {
            get { return _textalignment; }
            set { _textalignment = value; ReSetBounds(); }
        }

        /// <summary>
        /// Gibt die Ausmaße des Steurelements zurück oder legt es fest
        /// </summary>
        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                // Wenn Autosize ausgeschaltet ist, kann die größe erst weitergegeben werden
                if (!AutoSize)
                {
                    base.Size = value;
                }

                ReSetBounds();
            }
        }

        /// <summary>
        /// Liefert den Schiftstiel oder legt ihn fest
        /// </summary>
        public TextStyle Style
        {
            get
            {
                return NativeSFMLCalls.sfText_getStyle(Pointer);
            }
            set
            {
                NativeSFMLCalls.sfText_setStyle(Pointer, value);
                ReSetBounds();
                OnStyleChanged();
            }
        }

        /// <summary>
        /// Zeichnet das Steuerelement
        /// </summary>
        /// <param name="target"></param>
        internal override void InternalRenderUpdate(IRenderTarget target)
        {
            var state = new RenderStates(AlternativPositionTransformation(TextPosition));

            target.DrawText(Pointer, state);
        }

        /// <summary>
        /// Wird aufgerufen wenn die Schriftfarbe geändert wird
        /// </summary>
        protected virtual void OnColorChanged()
        {
            if (ColorChanged != null) ColorChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Schriftstiel geändert wird
        /// </summary>
        protected virtual void OnStyleChanged()
        {
            if (StyleChanged != null) StyleChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Text geändert wird
        /// </summary>
        protected virtual void OnTextChanged()
        {
            if (TextChanged != null) TextChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn die Schriftgröße geändert wird
        /// </summary>
        protected virtual void OnFontSizeChanged()
        {
            if (FontSizeChanged != null) FontSizeChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnFontChanged()
        {
            if (FontChanged != null) FontChanged.Invoke(this);
        }
    }
}
