﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    public class PictureBoxControl : RectShapeControl
    {
        /// <summary>
        /// 
        /// </summary>
        public PictureBoxControl()
            : this(new Vector2f(0, 0))
        {
          
        }

        /// <summary>
        /// 
        /// </summary>
        public PictureBoxControl(Texture texture)
            : this(new Vector2f(0, 0),texture)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        public PictureBoxControl(Vector2f size)
        {
            Size = size;
    
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        public PictureBoxControl(Vector2f size, Texture texture)
        {
            Size = size;
            Texture = texture;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public PictureBoxControl(PictureBoxControl copy) :
            base(copy)
        {
            Size = copy.Size;
            base.PointCount = 4;
        }
    }
}
