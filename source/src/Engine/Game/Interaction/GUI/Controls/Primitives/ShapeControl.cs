using Pandora.Game.Interaction.GUI.Animations;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;
using Pandora.Runtime.SFML;
using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Pandora.Game.Interaction.GUI.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ShapeControl : TexturedControl
    {
        private GetPointCountCallbackType myGetPointCountCallback;
        private GetPointCallbackType myGetPointCallback;

        private uint _pointcount;

        /// <summary>
        /// 
        /// </summary>
        protected ShapeControl()
        {
            InternalPointer = IntPtr.Zero;
            myGetPointCountCallback = new GetPointCountCallbackType(InternalGetPointCount);
            myGetPointCallback = new GetPointCallbackType(InternalGetPoint);
            InternalPointer = NativeSFMLCalls.sfShape_create(myGetPointCountCallback, myGetPointCallback, IntPtr.Zero);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public ShapeControl(ShapeControl copy)
            : base(copy)
        {
            InternalPointer = IntPtr.Zero;
            myGetPointCountCallback = new GetPointCountCallbackType(InternalGetPointCount);
            myGetPointCallback = new GetPointCallbackType(InternalGetPoint);
            InternalPointer = NativeSFMLCalls.sfShape_create(myGetPointCountCallback, myGetPointCallback, IntPtr.Zero);

            Origin = copy.Origin;
            Position = copy.Position;
            Rotation = copy.Rotation;
            Scale = copy.Scale;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            NativeSFMLCalls.sfShape_destroy(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                Update();
                base.Size = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual uint PointCount
        {
            get { return _pointcount; }
            set
            {
                _pointcount = value;
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected abstract Vector2f GetPoint(uint index);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override FloatRect GetLocalBounds()
        {
            return NativeSFMLCalls.sfShape_getLocalBounds(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override FloatRect GetGlobalBounds()
        {
            return Transform.TransformRect(GetLocalBounds());
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Update()
        {
            NativeSFMLCalls.sfShape_update(Pointer);
        }

        internal override void InternalRenderUpdate(IRenderTarget target)
        {
            var state = new RenderStates(Transform);

            target.DrawShape(Pointer, state);
        }

        private uint InternalGetPointCount(IntPtr userData)
        {
            return PointCount;
        }

        private Vector2f InternalGetPoint(uint index, IntPtr userData)
        {
            return GetPoint(index);
        }


    }
}
