﻿using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Controls
{
    /// <summary>
    /// Stellt eine Sammlung von Steuerelementen dar
    /// </summary>
    public sealed class ControlCollection : IEnumerable<Control>
    {
        #region neasted

        class ControlCompare : IComparer<Control>
        {
            #region IComparer<Control> Member

            public int Compare(Control x, Control y)
            {
                if (x.ZOrder > y.ZOrder)
                    return 1;
                else if (x.ZOrder < y.ZOrder)
                    return -1;

                return 0;
            }

            #endregion
        }

        #endregion

        private Control _parentcontrol;
        private Scene _parentwindow;
        private List<Control> _controls = new List<Control>();

        internal ControlCollection(Control control)
        {
            _parentcontrol = control;

            while (control != null)
            {
                if (control is Scene)
                {
                    _parentwindow = control as Scene;
                    break;
                }

                control = control.ParentControl;
            }
        }

        /// <summary>
        /// Fügt eine Gruppe von Steuerelementen hinzu
        /// </summary>
        /// <param name="controls"></param>
        public void AddRange(IEnumerable<Control> controls)
        {
            foreach (var control in controls)
            {
                _controls.Add(control);
                control.ParentControl = _parentcontrol;
                control.ParentWindow = _parentwindow;
                control.InternalLoad();
            }

            ReorderControls();
        }

        /// <summary>
        /// Fügt ein Steuerelement hinzu
        /// </summary>
        /// <param name="control"></param>
        public void Add(Control control)
        {
            _controls.Add(control);
            control.ParentControl = _parentcontrol;
            control.ParentWindow = _parentwindow;
            control.InternalLoad();

            ReorderControls();
        }

        /// <summary>
        /// Entfernt ein Steuerelement
        /// </summary>
        /// <param name="control"></param>
        public void Remove(Control control)
        {
            _controls.Remove(control);

            ReorderControls();
        }

        /// <summary>
        /// Liefert die Anzahl der Steuerelemente
        /// </summary>
        public int Count { get { return _controls.Count; } }

        private void ReorderControls()
        {
            _controls.Sort(new ControlCompare());
        }

        internal void InternalRenderUpdate(IRenderTarget target)
        {
            foreach (var control in _controls)
            {
                control.InternalRenderUpdate(target);
            }
        }

        /// <summary>
        /// Prüft ob ein Steuerelement an der angegebenen Position ist
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public bool HitTest(Vector2f position)
        {
            return _controls.Where(m => m.ScreenBounds.Contains(position)).Count() > 0;
        }

        /// <summary>
        /// Liefert das Steuerelement an der angegebenen Position
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public Control GetControl(Vector2f position)
        {
            return _controls.Where(m => m.ScreenBounds.Contains(position)).LastOrDefault();
        }

        #region IEnumerable<Control> Member

        /// <summary>
        /// Liefert eine Auflistung der Steuerelemente
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Control> GetEnumerator()
        {
            return _controls.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _controls.GetEnumerator();
        }

        #endregion

        #region Events

        internal bool MouseButtonPressed(Vector2f position, MouseButton button)
        {
            var control = GetControl(position);

            if (control == null)
            {
                return false;
            }
            else
            {
                return control.InternalMouseButtonPressed(position, button);
            }
        }

        internal bool MouseButtonReleased(Vector2f position, MouseButton button)
        {
            var control = GetControl(position);

            if (control == null)
            {
                return false;
            }
            else
            {
                return control.InternalMouseButtonReleased(position, button);
            }
        }

        internal bool MouseDoubleClick(Vector2f position)
        {
            var control = GetControl(position);

            if (control == null)
            {
                return false;
            }
            else
            {
                return control.InternalMouseDoubleClick(position);
            }
        }

        internal bool MouseMoved(Vector2f position)
        {
            var control = GetControl(position);

            if (control == null)
            {
                return false;
            }
            else
            {
                return control.InternalMouseMoved(position);
            }
        }

        internal bool MouseEnter(Vector2f position)
        {
            var control = GetControl(position);

            if (control == null)
            {
                return false;
            }
            else
            {
                return control.InternalMouseEnter(position);
            }
        }

        internal bool MouseLeave(Vector2f position)
        {
            var control = GetControl(position);

            if (control == null)
            {
                return false;
            }
            else
            {
                return control.InternalMouseLeave(position);
            }
        }

        internal bool MouseWheel(Vector2f position, int delta)
        {
            var control = GetControl(position);

            if (control == null)
            {
                return false;
            }
            else
            {
                return control.InternalMouseWheel(position, delta);
            }
        }

        #endregion

        internal void InternalLocationChanged()
        {
            foreach (var control in _controls)
            {
                control.InternalParentLocationChanged();
            }
        }



   
    }
}
