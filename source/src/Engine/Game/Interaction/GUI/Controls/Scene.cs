﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Interaction.GUI.Drawing;

namespace Pandora.Game.Interaction.GUI.Controls
{
    /// <summary>
    /// Stellt ein Ausgabefenster dar, das Steuerelemente aufnehmen kann
    /// </summary>
    public abstract class Scene : ControlContainer
    {
        private bool _fullscreen;

        /// <summary>
        /// Wird aufgerufen wenn das Fenster wieder aktiv ist
        /// </summary>
        public event WindowEventDelegate Activate;

        internal void InternalShow()
        {
            OnActivate();
        }

        internal void InternalSceneResize(Vector2f size)
        {
            if (_fullscreen)
                Size = size;

            OnResize();
        }

        /// <summary>
        /// Zeigt das Fenster an
        /// </summary>
        public void Show()
        {
            if (InstanceHolder.__VisualHandler == null)
                throw new ArgumentNullException("StaticHandler", "Window show not allowed, Visual handler not initialized.");

            ParentControl = null;
            ParentWindow = null;

            InstanceHolder.__VisualHandler.Show(this);
        }

        public bool Fullscreen
        {
            get { return _fullscreen; }
            set
            {
                _fullscreen = value;

                if (_fullscreen)
                {
                    base.Size = InstanceHolder.__VisualHandler.Size.ToVector2f();
                    base.Position = InstanceHolder.__zerovector;
                }
            }
        }

        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }

            set
            {
                if (_fullscreen) return;
                base.Size = value;
            }
        }

        public override Vector2f Position
        {
            get
            {
                return base.Position;
            }

            set
            {
                if (_fullscreen) return;
                base.Position = value;
            }
        }

        /// <summary>
        /// Wird aufgerufen wenn das Fenster wieder aktiv ist
        /// </summary>
        protected virtual void OnActivate()
        {
            if (Activate != null) Activate.Invoke(this);
        }

        protected virtual void OnResize()
        { }
    }
}
