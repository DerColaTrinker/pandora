﻿using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;
using Pandora.Game.Interaction.GUI.Animations;
using Pandora.Game.Interaction.GUI.Animations.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Interaction.GUI.Controls.Primitives;
using Pandora.Runtime.SFML;
using Pandora.Game.Interaction.Caching;

namespace Pandora.Game.Interaction.GUI.Controls
{
    /// <summary>
    /// Stellt die Basis eines Steuerelements dar
    /// </summary>
    public abstract class Control : PointerObject, ILocationProperty, ISizeProperty, IControl
    {
        private Vector2f _size = new Vector2f();
        private Control _parentcontrol;
        private Scene _parentscene;
        private uint _zorder;
        private bool _visible;
        private Vector2f _position;

        /// <summary>
        /// Wird aufgerufen wenn die Size Eigenschaft verändert wird
        /// </summary>
        public event ControlEventDelegate SizeChanged;

        /// <summary>
        /// Wird aufgerufen wenn die Location Eigenschaft verändert wird
        /// </summary>
        public event ControlEventDelegate PositionChanged;

        /// <summary>
        /// Wird aufgerufen wenn die ParentLocation Eigenschaft verändert wird
        /// </summary>
        public event ParentLocationChangedDelegate ParentLocationChanged;

        /// <summary>
        /// Wird aufgerufen wenn die ParentSize Eigenschaft verändert wird
        /// </summary>
        public event ParentSizeChangedDelegate ParentSizeChanged;

        /// <summary>
        /// Wird aufgerufen wenn die ZOrder Eigenschaft verändert wird
        /// </summary>
        public event ControlEventDelegate ZOrderChanged;

        /// <summary>
        /// Wird aufgerufen wenn das Steuerelement geladen wird
        /// </summary>
        public event ControlEventDelegate Load;

        /// <summary>
        /// Wird aufgerufen wenn das Steuerelement neu gezeichnet werden muss
        /// </summary>
        public event RenderUpdateDelegate RenderUpdate;

        /// <summary>
        /// Wird aufgerufen wenn die ParentControl Eigenschaft verändert wird
        /// </summary>
        public event ControlEventDelegate ParentControlChanged;

        /// <summary>
        /// Wird aufgerufen wenn die ParentWindow Eigenschaft verändert wird
        /// </summary>
        public event ControlEventDelegate ParentWindowChanged;

        /// <summary>
        /// Wird aufgerufen wenn die Sichtbarkeit geändert wird
        /// </summary>
        public event ControlEventDelegate VisibleChanged;

        /// <summary>
        /// Wird aufgerufen wenn eine Maustaste gedrückt wurde
        /// </summary>
        public event ControlMouseButtonDelegate MouseButtonPressed;

        /// <summary>
        /// Wird aufgerufen wenn eine Maustaste losgelassen wird
        /// </summary>
        public event ControlMouseButtonDelegate MouseButtonReleased;

        /// <summary>
        /// Wird aufgerufen wenn die Maus bewegt wird
        /// </summary>
        public event ControlMousePositionDelegate MouseMoved;

        public event ControlMousePositionDelegate MouseDoubleClick;

        public event ControlMousePositionDelegate MouseEnter;

        public event ControlMousePositionDelegate MouseLeave;

        /// <summary>
        /// 
        /// </summary>
        public event ControlMouseWheelDelegate MouseWheel;

        /// <summary>
        /// Erstellt eine neue Instanz der Control-Klasse
        /// </summary>
        protected Control()
        {
            InternalPointer = IntPtr.Zero;

            GUID = Guid.NewGuid().ToString();
            _visible = true;
        }

        /// <summary>
        /// Liefert eine eindeutige ID des Steuerelements.
        /// </summary>
        public string GUID { get; private set; }

        /// <summary>
        /// Liefert die Interaction Manager
        /// </summary>
        public InteractionManager Manager { get { return InstanceHolder.__VisualHandler.Manager; } }

        /// <summary>
        /// Liefert die UI Steuerung 
        /// </summary>
        public IVisualHandler Visuals { get { return InstanceHolder.__VisualHandler; } }

        /// <summary>
        /// Liefert die Animation-Engine
        /// </summary>
        public AnimationHandler Animation { get { return InstanceHolder.__VisualHandler.Animation; } }

        internal void InternalLoad()
        {
            if (!IsInitialized)
            {
                //_font = VisualHandler.__instance.DefaultFont;

                Size = new Vector2f(50F, 50F);
                Position = new Vector2f(0F, 0F);

                OnLoad();
            }

            IsInitialized = true;
        }

        internal void InternalParentLocationChanged()
        {
            OnParentLocationChanged();
        }

        internal virtual void InternalRenderUpdate(IRenderTarget target)
        {
            if (!Visible) return;

            OnRenderUpdate(target);
        }

        internal virtual bool InternalMouseButtonPressed(Vector2f position, MouseButton button)
        {
            if (MouseButtonPressed == null) return false;

            MouseButtonPressed.Invoke(this,  position- ScreenPosition, button);

            return true;
        }

        internal virtual bool InternalMouseButtonReleased(Vector2f position, MouseButton button)
        {
            if (MouseButtonReleased == null) return false;

            InstanceHolder.__LastMouseButtonReleaseControl = this;

            MouseButtonReleased.Invoke(this, position - ScreenPosition, button);

            return true;
        }

        internal virtual bool InternalMouseDoubleClick(Vector2f position)
        {
            if (MouseDoubleClick == null) return false;
            if (InstanceHolder.__LastMouseButtonReleaseControl != this) return false;

            MouseDoubleClick.Invoke(this, position - ScreenPosition);

            return true;
        }

        internal virtual bool InternalMouseMoved(Vector2f position)
        {
                        if (MouseEnter != null)
            {
                if (InstanceHolder.__LastMouseOverControl != this)
                {
                    if (InstanceHolder.__LastMouseOverControl != null) 
                        InstanceHolder.__LastMouseOverControl.InternalMouseLeave(position);

                    InstanceHolder.__LastMouseOverControl = this;
                    InternalMouseEnter(position);                   
                }
            }

                        if (MouseMoved == null) return false;

                        MouseMoved.Invoke(this, position - ScreenPosition);

            return true;
        }

        internal virtual bool InternalMouseEnter(Vector2f position)
        {
            if (MouseEnter == null) return false;

            MouseEnter.Invoke(this, position - ScreenPosition);

            return true;
        }

        internal virtual bool InternalMouseLeave(Vector2f position)
        {
            if (MouseLeave == null) return false;

            MouseLeave.Invoke(this, position - ScreenPosition);

            return true;
        }
            
        internal virtual bool InternalMouseWheel(Vector2f position, int delta)
        {
            if (MouseWheel == null) return false;

            MouseWheel.Invoke(this, position - ScreenPosition, delta);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Vector2f Size { get { return _size; } set { _size = value; OnSizeChanged(); } }

        /// <summary>
        /// 
        /// </summary>
        public virtual Vector2f Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
                OnPositionChanged();
            }
        }

        /// <summary>
        /// Liefert die Bildschirmposition
        /// </summary>
        public Vector2f ScreenPosition
        {
            get
            {
                if (ParentControl == null)
                {
                    return Position;
                }
                else
                {
                    return ParentControl.Position + Position;
                }
            }
        }

        /// <summary>
        /// Liefert die Dimensionen des Steuerelements
        /// </summary>
        public virtual FloatRect Bounds { get { return new FloatRect(Position, Size); } }

        /// <summary>
        /// Liefert die Dimensionen des Steurelements bezogen auf den Bildschirm
        /// </summary>
        public FloatRect ScreenBounds { get { return new FloatRect(ScreenPosition, Size); } }

        /// <summary>
        /// Liefert die Reihenfolge in der tiefe.
        /// </summary>
        /// <remarks>Diese Eingenschaft wird meist durch den Container festgelegt und ist abhängig von der RenderPipeline</remarks>
        public virtual uint ZOrder { get { return _zorder; } set { _zorder = value; ; OnZOrderChanged(); } }

        /// <summary>
        /// Liefert das Eltern-Steuerelement
        /// </summary>
        public virtual Control ParentControl
        {
            get { return _parentcontrol; }
            internal set
            {
                _parentcontrol = value;

                OnParentControlChanged();

                if (value == null) return;

                _parentcontrol.PositionChanged += ParentlLocationChanged;
                _parentcontrol.SizeChanged += OnParentControlSizeChanged;
            }
        }

        /// <summary>
        /// Liefert das Fenster in dem das Steuerelement gebunden ist
        /// </summary>
        public virtual Scene ParentWindow { get { return _parentscene; } internal set { _parentscene = value; OnParentWindowChanged(); } }

        /// <summary>
        /// Gibt an ob das Steuerelement sichtbar ist oder legt es fest
        /// </summary>
        public virtual bool Visible { get { return _visible; } set { _visible = value; OnVisibleChange(); } }

        /// <summary>
        /// Gibt an ob diese Steuerelement initialisiert wurde
        /// </summary>
        public bool IsInitialized { get; private set; }

        public CacheHandler Cache { get { return InstanceHolder.__VisualHandler.Manager.Cache; } }

        /// <summary>
        /// Wird aufgerufen wenn die ParentControlLocation Eingeschaft geändert wurde
        /// </summary>
        /// <param name="control"></param>
        protected virtual void ParentlLocationChanged(Control control)
        {
            Position = ScreenPosition;
            if (ParentLocationChanged != null) ParentLocationChanged.Invoke(this, control);
        }

        /// <summary>
        /// Wird aufgerufen wenn die ParentControlSize Eingeschaft geändert wurde
        /// </summary>
        /// <param name="control"></param>
        protected virtual void OnParentControlSizeChanged(Control control)
        {
            if (ParentSizeChanged != null) ParentSizeChanged.Invoke(this, control);
        }

        /// <summary>
        /// Wird aufgerufen wenn das Steuerelement geladen wird
        /// </summary>
        protected virtual void OnLoad()
        {
            if (Load != null) Load.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn die Position des Elternelements geändert wird
        /// </summary>
        protected virtual void OnParentLocationChanged()
        {

        }

        /// <summary>
        /// Wird aufgerufen wenn das Steuerelement neu gezeichnet wird
        /// </summary>
        /// <param name="target"></param>
        protected virtual void OnRenderUpdate(IRenderTarget target)
        {
            if (RenderUpdate != null) RenderUpdate.Invoke(this, target);
        }

        /// <summary>
        /// Wird aufgerufen wenn die ParentControl Eingeschaft geändert wurde
        /// </summary>
        protected virtual void OnParentControlChanged()
        {
            if (ParentControlChanged != null) ParentControlChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn die ParentWindow Eingeschaft geändert wurde
        /// </summary>
        protected virtual void OnParentWindowChanged()
        {
            if (ParentWindowChanged != null) ParentWindowChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn die Size Eingeschaft geändert wurde
        /// </summary>
        protected virtual void OnSizeChanged()
        {
            if (SizeChanged != null) SizeChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn die Location Eingeschaft geändert wurde
        /// </summary>
        protected virtual void OnPositionChanged()
        {
            if (PositionChanged != null) PositionChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn die ZOrder Eingeschaft geändert wurde
        /// </summary>
        protected virtual void OnZOrderChanged()
        {
            if (ZOrderChanged != null) ZOrderChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn die Sichtbarkeit geändert wird
        /// </summary>
        protected virtual void OnVisibleChange()
        {
            if (VisibleChanged != null) VisibleChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn eine Maustaste gedrückt wird
        /// </summary>
        /// <param name="position"></param>
        /// <param name="button"></param>
        protected virtual void OnMouseButtonPressed(Vector2f position, MouseButton button)
        {
            if (MouseButtonPressed != null) MouseButtonPressed.Invoke(this, position, button);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Mauszeiger über das Steuerelement gezogen wird
        /// </summary>
        /// <param name="position"></param>
        protected virtual void OnMouseMoved(Vector2f position)
        {
            if (MouseMoved != null) MouseMoved.Invoke(this, position);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="delta"></param>
        protected virtual void OnMouseWheel(Vector2f position, int delta)
        {
            if (MouseWheel != null) MouseWheel.Invoke(this, position, delta);
        }
    }
}
