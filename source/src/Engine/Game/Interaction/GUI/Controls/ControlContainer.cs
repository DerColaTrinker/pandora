﻿using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Controls
{
    /// <summary>
    /// Ein Steuerelement das als Kontainer für weitere Steuerelemente dient
    /// </summary>
    public abstract class ControlContainer : Control
    {
        /// <summary>
        /// Erstellt eine neue Instnaz der ControlContainer-Klasse
        /// </summary>
        public ControlContainer()
        {
            Controls = new ControlCollection(this);
        }

        /// <summary>
        /// Liefert eine Sammlung von Steuerelementen die innerhalb dieses Steuerelements eingeordnet werden.
        /// </summary>
        public ControlCollection Controls { get; private set; }

        /// <summary>
        /// Liefert das Steuerelement das in der Renderreihenfolge ganz oben liegt
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public Control GetTopControl(Vector2f position)
        {
            var control = Controls.GetControl(position);

            if (control == null)
            {
                return this;
            }
            else
            {
                if (!IgnoreEventsCallOnControlCollection)
                {
                    if (control is ControlContainer)
                    {
                        return ((ControlContainer)control).GetTopControl(position);
                    }
                    else
                    {
                        return control;
                    }
                }
                else
                {
                    return this;
                }
            }
        }

        /// <summary>
        /// Gibt die Position des Steuerelements zurück oder legt es fest
        /// </summary>
        public override Vector2f Position
        {
            get
            {
                return base.Position;
            }
            set
            {
                base.Position = value;
                Controls.InternalLocationChanged();
            }
        }

        /* Das Render Update muss hier abgefangen werden um erst dieses Steuerelement zu Rendern und dann die in der ControlCollection */
        internal override void InternalRenderUpdate(IRenderTarget target)
        {
            if (!Visible) return;

            base.InternalRenderUpdate(target);
            Controls.InternalRenderUpdate(target);
        }

        /* Das Mouse Button Event muss hier abgefangen werden um es auf die Steuerelemente in der ControlCollection weiter zu leiten. 
         * Wenn kein Steuerelement gefunden wurde kann das Event auf dieses Steurelement angewendet werden. */
        internal override bool InternalMouseButtonPressed(Vector2f position, MouseButton button)
        {
            if (!Visible) return false;
            if (IgnoreEventsCallOnControlCollection) return base.InternalMouseButtonPressed(position, button);
            if (!Controls.MouseButtonPressed(position, button)) return base.InternalMouseButtonPressed(position, button);

            return false;
        }

        internal override bool InternalMouseButtonReleased(Vector2f position, MouseButton button)
        {
            if (!Visible) return false;
            if (IgnoreEventsCallOnControlCollection) return base.InternalMouseButtonReleased(position, button);
            if (!Controls.MouseButtonReleased(position, button)) return base.InternalMouseButtonReleased(position, button);

            return false;
        }

        internal override bool InternalMouseDoubleClick(Vector2f position)
        {
            if (!Visible) return false;
            if (IgnoreEventsCallOnControlCollection) return base.InternalMouseDoubleClick(position);
            if (!Controls.MouseDoubleClick(position)) return base.InternalMouseDoubleClick(position);

            return false;
        }

        internal override bool InternalMouseMoved(Vector2f position)
        {
            if (!Visible) return false;
            if (IgnoreEventsCallOnControlCollection) return base.InternalMouseMoved(position);
            if (!Controls.MouseMoved(position)) return base.InternalMouseMoved(position);

            return false;
        }

        internal override bool InternalMouseEnter(Vector2f position)
        {
            if (!Visible) return false;
            if (IgnoreEventsCallOnControlCollection) return base.InternalMouseEnter(position);
            if (!Controls.MouseEnter(position)) return base.InternalMouseEnter(position);

            return false;
        }

        internal override bool InternalMouseLeave(Vector2f position)
        {
            if (!Visible) return false;
            if (IgnoreEventsCallOnControlCollection) return base.InternalMouseLeave(position);
            if (!Controls.MouseLeave(position)) return base.InternalMouseLeave(position);

            return false;
        }

        internal override bool InternalMouseWheel(Vector2f position, int delta)
        {
            if (!Visible) return false;
            if (IgnoreEventsCallOnControlCollection) return base.InternalMouseWheel(position, delta);
            if (!Controls.MouseWheel(position, delta)) return base.InternalMouseWheel(position, delta);

            return false;
        }

        /// <summary>
        /// Wenn 'True' werden eingehende Events nicht an die darunter liegenden Steuerelemente weitergeleitet.
        /// </summary>
        protected bool IgnoreEventsCallOnControlCollection { get; set; }
    }
}
