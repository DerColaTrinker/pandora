using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Runtime.ConstrainedExecution;

namespace Pandora.Game.Interaction.GUI
{
    internal class Context : CriticalFinalizerObject
    {
        public Context()
        {
            _pointer = NativeSFMLCalls.sfContext_create();
        }

        ~Context()
        {
            NativeSFMLCalls.sfContext_destroy(_pointer);
        }

        public void SetActive(bool active)
        {
            NativeSFMLCalls.sfContext_setActive(_pointer, active);
        }

        public static Context Global
        {
            get
            {
                if (ourGlobalContext == null)
                    ourGlobalContext = new Context();

                return ourGlobalContext;
            }
        }

        public override string ToString()
        {
            return "[Context]";
        }

        private static Context ourGlobalContext = null;

        private IntPtr _pointer = IntPtr.Zero;
    }
}
