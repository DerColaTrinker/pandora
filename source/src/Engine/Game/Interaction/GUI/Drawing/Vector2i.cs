﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Drawing
{
    /// <summary>
    /// Stellt einen Punkt mit zwei Koordinaten dar
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Vector2i : IEquatable<Vector2i>
    {
        /// <summary>
        /// Erstellt einen Vektor aus zwei Koordinaten
        /// </summary>
        public Vector2i(int x, int y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Subtrahieren
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector2i operator -(Vector2i v)
        {
            return new Vector2i(-v.X, -v.Y);
        }

        /// <summary>
        /// Subtrahieren
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector2i operator -(Vector2i v1, Vector2i v2)
        {
            return new Vector2i(v1.X - v2.X, v1.Y - v2.Y);
        }

        /// <summary>
        /// Addieren
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector2i operator +(Vector2i v1, Vector2i v2)
        {
            return new Vector2i(v1.X + v2.X, v1.Y + v2.Y);
        }

        /// <summary>
        /// Multiplizieren
        /// </summary>
        /// <param name="v"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static Vector2i operator *(Vector2i v, int x)
        {
            return new Vector2i(v.X * x, v.Y * x);
        }

        /// <summary>
        /// Multiplizieren
        /// </summary>
        /// <param name="v"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static Vector2i operator *(int x, Vector2i v)
        {
            return new Vector2i(v.X * x, v.Y * x);
        }

        /// <summary>
        /// Dividieren
        /// </summary>
        /// <param name="v"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static Vector2i operator /(Vector2i v, int x)
        {
            return new Vector2i(v.X / x, v.Y / x);
        }

        /// <summary>
        /// Gleich
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static bool operator ==(Vector2i v1, Vector2i v2)
        {
            return v1.Equals(v2);
        }

        /// <summary>
        /// Ungleich
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static bool operator !=(Vector2i v1, Vector2i v2)
        {
            return !v1.Equals(v2);
        }

        /// <summary>
        /// Gibt eine Zeichenkette aus
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[Vector2i]" +
                   " X(" + X + ")" +
                   " Y(" + Y + ")";
        }

        /// <summary>
        /// Vergleicht den Vektor
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (obj is Vector2i) && obj.Equals(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Vector2i other)
        {
            return (X == other.X) &&
                   (Y == other.Y);
        }

        /// <summary>
        /// Liefert einen HasCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return X.GetHashCode() ^
                   Y.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static implicit operator Vector2f(Vector2i v)
        {
            return new Vector2f((float)v.X, (float)v.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static implicit operator Vector2u(Vector2i v)
        {
            return new Vector2u((uint)v.X, (uint)v.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        public int X;

        /// <summary>
        /// 
        /// </summary>
        public int Y;
    }
}
