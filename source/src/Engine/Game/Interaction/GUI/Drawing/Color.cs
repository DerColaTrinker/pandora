using System;
using System.Runtime.InteropServices;

namespace Pandora.Game.Interaction.GUI.Drawing
{
    /// <summary>
    /// Stellt eine Farbe dar
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Color : IEquatable<Color>
    {
        /// <summary>
        /// Erstellt einen neuen Wert mit den angegebenen Farbanteilen
        /// </summary>
        /// <param name="red"></param>
        /// <param name="green"></param>
        /// <param name="blue"></param>
        public Color(byte red, byte green, byte blue) :
            this(red, green, blue, 255)
        { }

        /// <summary>
        /// Erstellt einen neuen Wert mit den angegebenen Farbanteilen
        /// </summary>
        /// <param name="red"></param>
        /// <param name="green"></param>
        /// <param name="blue"></param>
        /// <param name="alpha"></param>
        public Color(byte red, byte green, byte blue, byte alpha)
        {
            R = red;
            G = green;
            B = blue;
            A = alpha;
        }

        /// <summary>
        /// Erstellt eine kopie des Farb-Werts
        /// </summary>
        /// <param name="color"></param>
        public Color(Color color) :
            this(color.R, color.G, color.B, color.A)
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[Color]" +
                   " R(" + R + ")" +
                   " G(" + G + ")" +
                   " B(" + B + ")" +
                   " A(" + A + ")";
        }

        /// <summary>
        /// Vergleicht Farben
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (obj is Color) && obj.Equals(this);
        }

        /// <summary>
        /// Vergleicht Farben
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Color other)
        {
            return (R == other.R) &&
                   (G == other.G) &&
                   (B == other.B) &&
                   (A == other.A);
        }

        /// <summary>
        /// Liefert einen eindeutigen Hashcode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (int)(R << 24) |
                   (int)(G << 16) |
                   (int)(B << 8) |
                   (int)A;
        }

        /// <summary>
        /// Gleich
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Color left, Color right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Ungleich
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(Color left, Color right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Addieren
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Color operator +(Color left, Color right)
        {
            return new Color((byte)Math.Min(left.R + right.R, 255),
                             (byte)Math.Min(left.G + right.G, 255),
                             (byte)Math.Min(left.B + right.B, 255),
                             (byte)Math.Min(left.A + right.A, 255));
        }

        /// <summary>
        /// Subtrahieren
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Color operator -(Color left, Color right)
        {
            return new Color((byte)Math.Max(left.R - right.R, 0),
                             (byte)Math.Max(left.G - right.G, 0),
                             (byte)Math.Max(left.B - right.B, 0),
                             (byte)Math.Max(left.A - right.A, 0));
        }

        /// <summary>
        /// Multiplizieren
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Color operator *(Color left, Color right)
        {
            return new Color((byte)Math.Min(left.R * right.R, 255),
                             (byte)Math.Min(left.G * right.G, 255),
                             (byte)Math.Min(left.B * right.B, 255),
                             (byte)Math.Min(left.A * right.A, 255));
        }

        /// <summary>
        /// Rotanteil
        /// </summary>
        public byte R;

        /// <summary>
        /// Gr�nanteil
        /// </summary>
        public byte G;

        /// <summary>
        /// Blauanteil
        /// </summary>
        public byte B;

        /// <summary>
        /// Alphawert
        /// </summary>
        public byte A;

        /// <summary>
        /// Schwarz
        /// </summary>
        public static readonly Color Black = new Color(0, 0, 0);

        /// <summary>
        /// Wei�
        /// </summary>
        public static readonly Color White = new Color(255, 255, 255);

        /// <summary>
        /// Rot
        /// </summary>
        public static readonly Color Red = new Color(255, 0, 0);

        /// <summary>
        /// Gr�n
        /// </summary>
        public static readonly Color Green = new Color(0, 255, 0);

        /// <summary>
        /// Blau
        /// </summary>
        public static readonly Color Blue = new Color(0, 0, 255);

        /// <summary>
        /// Gelb
        /// </summary>
        public static readonly Color Yellow = new Color(255, 255, 0);

        /// <summary>
        /// Magenta
        /// </summary>
        public static readonly Color Magenta = new Color(255, 0, 255);

        /// <summary>
        /// T�rkis
        /// </summary>
        public static readonly Color Cyan = new Color(0, 255, 255);

        /// <summary>
        /// Grau
        /// </summary>
        public static readonly Color Grey = new Color(128, 128, 128);

        /// <summary>
        /// Transparent
        /// </summary>
        public static readonly Color Transparent = new Color(0, 0, 0, 0);
    }
}