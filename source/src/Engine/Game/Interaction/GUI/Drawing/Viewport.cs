﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Drawing
{
    public class Viewport
    {
        private FloatRect _source;
        private Vector2f _destination;
        private Vector2f _factor;
        private Vector2f _zoomposition;
        private Vector2f _zoomsize;
        private float _zoom = 0F;

        public Viewport(Vector2f source, Vector2f destination)
        {
            _source = new FloatRect(Vector2f.Zero, source);
            _destination = destination;
            UpdateViewport();
        }

        public Vector2f Source
        {
            get { return _source.Size; }
            set { _source = new FloatRect(Vector2f.Zero, value); UpdateViewport(); }
        }

        public Vector2f Destination
        {
            get { return _destination; }
            set { _destination = value; UpdateViewport(); }
        }

        public Vector2f Factor
        {
            get { return _factor; }
        }

        public FloatRect SourceViewRect
        {
            get { return new FloatRect(_zoomposition, _zoomsize); }
        }

        public Vector2f ZoomSize
        {
            get { return _zoomsize; }
        }

        public Vector2f ZoomPosition
        {
            get { return _zoomposition; }
            set
            {
                if ( value <= Vector2f.Zero) return;
                if ((value + _zoomsize) > _source.Size) return;

                _zoomposition = value;
            }
        }

        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value; UpdateViewport(); }
        }

        private void UpdateViewport()
        {
            _factor = new Vector2f(_destination.X / _source.Size.X, _destination.Y / _source.Size.Y) * (1 + _zoom);
            _zoomsize = _source.Size / (1 + _zoom);

            // Position anpassen, wenn sich der Viewport beim rauszoomen außerhalb der Source befindet.
            if ((_zoomposition + _zoomsize) > _source.Size)
            {
                _zoomposition += (_source.Size - (_zoomposition + _zoomsize));
            }
        }

        public Vector2f CoordsToSource(Vector2f position)
        {
            return position / _factor;
        }

        public Vector2f AdjustPosition(Vector2f objposition)
        {
            return (objposition - _zoomposition) * _factor;
        }
    }
}
