using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;
using System.IO;
using Pandora.Runtime;
using Pandora.Runtime.SFML;

namespace Pandora.Game.Interaction.GUI.Drawing
{
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Font is the low-level class for loading and
    /// manipulating character fonts. This class is meant to
    /// be used by String2D
    /// </summary>
    ////////////////////////////////////////////////////////////
    public class Font : PointerObject
    {
        private StreamAdaptor _stream;
        private Dictionary<uint, Texture> _textures = new Dictionary<uint, Texture>();

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Construct the font from a file
        /// </summary>
        /// <param name="filename">Font file to load</param>
        /// <exception cref="LoadingFailedException" />
        ////////////////////////////////////////////////////////////
        public Font(string filename) :
            base()
        {
            InternalPointer = NativeSFMLCalls.sfFont_createFromFile(filename);

            if (Pointer == IntPtr.Zero)
                throw new LoadingFailedException("font", filename);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Construct the font from a custom stream
        /// </summary>
        /// <param name="stream">Source stream to read from</param>
        /// <exception cref="LoadingFailedException" />
        ////////////////////////////////////////////////////////////
        public Font(Stream stream)
        {
            _stream = new StreamAdaptor(stream);
            InternalPointer = NativeSFMLCalls.sfFont_createFromStream(_stream.InputStreamPtr);

            if (Pointer == IntPtr.Zero)
                throw new LoadingFailedException("font");
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Construct the font from a file in memory
        /// </summary>
        /// <param name="bytes">Byte array containing the file contents</param>
        /// <exception cref="LoadingFailedException" />
        ////////////////////////////////////////////////////////////
        public Font(byte[] bytes)
        {
            GCHandle pin = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            try
            {
                InternalPointer = NativeSFMLCalls.sfFont_createFromMemory(pin.AddrOfPinnedObject(), Convert.ToUInt64(bytes.Length));
            }
            finally
            {
                pin.Free();
            }
            if (Pointer == IntPtr.Zero)
                throw new LoadingFailedException("font");
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Construct the font from another font
        /// </summary>
        /// <param name="copy">Font to copy</param>
        ////////////////////////////////////////////////////////////
        public Font(Font copy)
        {
            InternalPointer = NativeSFMLCalls.sfFont_copy(copy.Pointer);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get a glyph in the font
        /// </summary>
        /// <param name="codePoint">Unicode code point of the character to get</param>
        /// <param name="characterSize">Character size</param>
        /// <param name="bold">Retrieve the bold version or the regular one?</param>
        /// <returns>The glyph corresponding to the character</returns>
        ////////////////////////////////////////////////////////////
        public Glyph GetGlyph(uint codePoint, uint characterSize, bool bold)
        {
            return NativeSFMLCalls.sfFont_getGlyph(Pointer, codePoint, characterSize, bold);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get the kerning offset between two glyphs
        /// </summary>
        /// <param name="first">Unicode code point of the first character</param>
        /// <param name="second">Unicode code point of the second character</param>
        /// <param name="characterSize">Character size</param>
        /// <returns>Kerning offset, in pixels</returns>
        ////////////////////////////////////////////////////////////
        public float GetKerning(uint first, uint second, uint characterSize)
        {
            return NativeSFMLCalls.sfFont_getKerning(Pointer, first, second, characterSize);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get spacing between two consecutive lines
        /// </summary>
        /// <param name="characterSize">Character size</param>
        /// <returns>Line spacing, in pixels</returns>
        ////////////////////////////////////////////////////////////
        public float GetLineSpacing(uint characterSize)
        {
            return NativeSFMLCalls.sfFont_getLineSpacing(Pointer, characterSize);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get the position of the underline
        /// </summary>
        /// <param name="characterSize">Character size</param>
        /// <returns>Underline position, in pixels</returns>
        ////////////////////////////////////////////////////////////
        public float GetUnderlinePosition(uint characterSize)
        {
            return NativeSFMLCalls.sfFont_getUnderlinePosition(Pointer, characterSize);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get the thickness of the underline
        /// </summary>
        /// <param name="characterSize">Character size</param>
        /// <returns>Underline thickness, in pixels</returns>
        ////////////////////////////////////////////////////////////
        public float GetUnderlineThickness(uint characterSize)
        {
            return NativeSFMLCalls.sfFont_getUnderlineThickness(Pointer, characterSize);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get the texture containing the glyphs of a given size
        /// </summary>
        /// <param name="characterSize">Character size</param>
        /// <returns>Texture storing the glyphs for the given size</returns>
        ////////////////////////////////////////////////////////////
        public Texture GetTexture(uint characterSize)
        {
            _textures[characterSize] = new Texture(NativeSFMLCalls.sfFont_getTexture(Pointer, characterSize));
            return _textures[characterSize];
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get the font information
        /// </summary>
        /// <returns>A structure that holds the font information</returns>
        ////////////////////////////////////////////////////////////
        public Info GetInfo()
        {
            InfoMarshalData data = NativeSFMLCalls.sfFont_getInfo(Pointer);
            Info info = new Info();

            info.Family = Marshal.PtrToStringAnsi(data.Family);

            return info;
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Provide a string describing the object
        /// </summary>
        /// <returns>String description of the object</returns>
        ////////////////////////////////////////////////////////////
        public override string ToString()
        {
            return "[Font]";
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Handle the destruction of the object
        /// </summary>
        /// <param name="disposing">Is the GC disposing the object, or is it an explicit call ?</param>
        ////////////////////////////////////////////////////////////
        protected override void Destroy(bool disposing)
        {
            if (!disposing)
                Context.Global.SetActive(true);

            NativeSFMLCalls.sfFont_destroy(Pointer);

            if (disposing)
            {
                foreach (Texture texture in _textures.Values)
                    texture.Dispose();

                if (_stream != null)
                    _stream.Dispose();
            }

            if (!disposing)
                Context.Global.SetActive(false);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Internal constructor
        /// </summary>
        /// <param name="pointer">Pointer to the object in C library</param>
        ////////////////////////////////////////////////////////////
        private Font(IntPtr pointer)
        {
            InternalPointer = pointer;
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Info holds various information about a font
        /// </summary>
        ////////////////////////////////////////////////////////////
        public struct Info
        {
            /// <summary>The font family</summary>
            public string Family;
        }
    }
}
