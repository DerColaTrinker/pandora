﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Drawing
{
    /// <summary>
    /// Stellt einen Punkt mit zwei Koordinaten dar
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public partial struct Vector2u : IEquatable<Vector2u>
    {
        /// <summary>
        /// Erstellt einen Vektor aus zwei Koordinaten
        /// </summary>
        public Vector2u(uint x, uint y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector2u operator -(Vector2u v1, Vector2u v2)
        {
            return new Vector2u(v1.X - v2.X, v1.Y - v2.Y);
        }

        /// <summary>
        /// Addieren
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector2u operator +(Vector2u v1, Vector2u v2)
        {
            return new Vector2u(v1.X + v2.X, v1.Y + v2.Y);
        }

        /// <summary>
        /// Multiplizieren
        /// </summary>
        /// <param name="v"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static Vector2u operator *(Vector2u v, uint x)
        {
            return new Vector2u(v.X * x, v.Y * x);
        }

        /// <summary>
        /// Multiplizieren
        /// </summary>
        /// <param name="v"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static Vector2u operator *(uint x, Vector2u v)
        {
            return new Vector2u(v.X * x, v.Y * x);
        }

        /// <summary>
        /// Dividieren
        /// </summary>
        /// <param name="v"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static Vector2u operator /(Vector2u v, uint x)
        {
            return new Vector2u(v.X / x, v.Y / x);
        }

        /// <summary>
        /// Gleich
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static bool operator ==(Vector2u v1, Vector2u v2)
        {
            return v1.Equals(v2);
        }

        /// <summary>
        /// Ungleich
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static bool operator !=(Vector2u v1, Vector2u v2)
        {
            return !v1.Equals(v2);
        }

        /// <summary>
        /// Größer
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static bool operator >(Vector2u v1, Vector2u v2)
        {
            return v1.X > v2.X | v1.Y > v2.Y;
        }

        /// <summary>
        /// Kleiner
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static bool operator <(Vector2u v1, Vector2u v2)
        {
            return v1.X < v2.X | v1.Y < v2.Y;
        }

        /// <summary>
        /// Größer gleich
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static bool operator >=(Vector2u v1, Vector2u v2)
        {
            return v1.X >= v2.X | v1.Y >= v2.Y;
        }

        /// <summary>
        /// Kleiner gleich
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static bool operator <=(Vector2u v1, Vector2u v2)
        {
            return v1.X <= v2.X | v1.Y <= v2.Y;
        }

        /// <summary>
        /// Gibt eine Zeichenkette aus
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[Vector2u]" +
                   " X(" + X + ")" +
                   " Y(" + Y + ")";
        }

        /// <summary>
        /// Vergleicht den Vektor
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (obj is Vector2u) && obj.Equals(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Vector2u other)
        {
            return (X == other.X) &&
                   (Y == other.Y);
        }

        /// <summary>
        /// Liefert einen HasCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return X.GetHashCode() ^
                   Y.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static implicit operator Vector2i(Vector2u v)
        {
            return new Vector2i((int)v.X, (int)v.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static implicit operator Vector2f(Vector2u v)
        {
            return new Vector2f((float)v.X, (float)v.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        public uint X;

        /// <summary>
        /// 
        /// </summary>
        public uint Y;
    }
}
