using System;
using System.Runtime.InteropServices;

namespace Pandora.Game.Interaction.GUI.Drawing
{
    /// <summary>
    /// 
    /// </summary>
    public enum PrimitiveType
    {
        /// <summary>
        /// 
        /// </summary>
        Points,

        /// <summary>
        /// 
        /// </summary>
        Lines,

        /// <summary>
        /// 
        /// </summary>
        LinesStrip,

        /// <summary>
        /// 
        /// </summary>
        Triangles,

        /// <summary>
        /// 
        /// </summary>
        TrianglesStrip,

        /// <summary>
        /// 
        /// </summary>
        TrianglesFan,

        /// <summary>
        /// 
        /// </summary>
        Quads
    }
}