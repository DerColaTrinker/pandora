﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Drawing
{
    /// <summary>
    /// Stellt ein Rechteckbereich dar
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct IntRect : IEquatable<IntRect>
    {
        /// <summary>
        /// Neuer Rechteckbereich
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        ////////////////////////////////////////////////////////////
        public IntRect(int left, int top, int width, int height)
        {
            Left = left;
            Top = top;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Neuer Rechteckbereich
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        public IntRect(Vector2i position, Vector2i size)
            : this(position.X, position.Y, size.X, size.Y)
        {
        }

        /// <summary>
        /// Gibt an ob sich die Koordinaten innerhalb des Rechtecks befinden
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        ////////////////////////////////////////////////////////////
        public bool Contains(int x, int y)
        {
            int minX = Math.Min(Left, Left + Width);
            int maxX = Math.Max(Left, Left + Width);
            int minY = Math.Min(Top, Top + Height);
            int maxY = Math.Max(Top, Top + Height);

            return (x >= minX) && (x < maxX) && (y >= minY) && (y < maxY);
        }

        /// <summary>
        /// Gibt an ob sich das angegebene Rechteck innerhalb dieses Rechtecks befindet
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public bool Intersects(IntRect rect)
        {
            IntRect overlap;
            return Intersects(rect, out overlap);
        }

        /// <summary>
        /// Gibt an ob sich das angegebene Rechteck innerhalb dieses Rechtecks befindet
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="overlap"></param>
        /// <returns></returns>
        public bool Intersects(IntRect rect, out IntRect overlap)
        {
            // Rectangles with negative dimensions are allowed, so we must handle them correctly

            // Compute the min and max of the first rectangle on both axes
            int r1MinX = Math.Min(Left, Left + Width);
            int r1MaxX = Math.Max(Left, Left + Width);
            int r1MinY = Math.Min(Top, Top + Height);
            int r1MaxY = Math.Max(Top, Top + Height);

            // Compute the min and max of the second rectangle on both axes
            int r2MinX = Math.Min(rect.Left, rect.Left + rect.Width);
            int r2MaxX = Math.Max(rect.Left, rect.Left + rect.Width);
            int r2MinY = Math.Min(rect.Top, rect.Top + rect.Height);
            int r2MaxY = Math.Max(rect.Top, rect.Top + rect.Height);

            // Compute the intersection boundaries
            int interLeft = Math.Max(r1MinX, r2MinX);
            int interTop = Math.Max(r1MinY, r2MinY);
            int interRight = Math.Min(r1MaxX, r2MaxX);
            int interBottom = Math.Min(r1MaxY, r2MaxY);

            // If the intersection is valid (positive non zero area), then there is an intersection
            if ((interLeft < interRight) && (interTop < interBottom))
            {
                overlap.Left = interLeft;
                overlap.Top = interTop;
                overlap.Width = interRight - interLeft;
                overlap.Height = interBottom - interTop;
                return true;
            }
            else
            {
                overlap.Left = 0;
                overlap.Top = 0;
                overlap.Width = 0;
                overlap.Height = 0;
                return false;
            }
        }

        /// <summary>
        /// Gibt eine Zeichenkette aus
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[IntRect]" +
                   " Left(" + Left + ")" +
                   " Top(" + Top + ")" +
                   " Width(" + Width + ")" +
                   " Height(" + Height + ")";
        }

        /// <summary>
        /// Vergleicht zwei Rechtecke
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (obj is IntRect) && obj.Equals(this);
        }

        /// <summary>
        /// Vergleicht zwei Rechtecke
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IntRect other)
        {
            return (Left == other.Left) &&
                   (Top == other.Top) &&
                   (Width == other.Width) &&
                   (Height == other.Height);
        }

        /// <summary>
        /// Liefrt einen Hashcode
        /// </summary>
        /// <returns></returns>
        ////////////////////////////////////////////////////////////
        public override int GetHashCode()
        {
            return unchecked((int)((uint)Left ^
                   (((uint)Top << 13) | ((uint)Top >> 19)) ^
                   (((uint)Width << 26) | ((uint)Width >> 6)) ^
                   (((uint)Height << 7) | ((uint)Height >> 25))));
        }

        /// <summary>
        /// Gleich
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static bool operator ==(IntRect r1, IntRect r2)
        {
            return r1.Equals(r2);
        }

        /// <summary>
        /// Ungleich
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static bool operator !=(IntRect r1, IntRect r2)
        {
            return !r1.Equals(r2);
        }

        /// <summary>
        /// Konvertiert ein IntRect in ein FloatRect
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public static explicit operator FloatRect(IntRect r)
        {
            return new FloatRect((float)r.Left,
                                 (float)r.Top,
                                 (float)r.Width,
                                 (float)r.Height);
        }

        /// <summary>
        /// X-Koordinaten Punkt
        /// </summary>
        public int Left;

        /// <summary>
        /// Y-Koodinaten Punkt
        /// </summary>
        public int Top;

        /// <summary>
        /// Breite
        /// </summary>
        public int Width;

        /// <summary>
        /// Höhe
        /// </summary>
        public int Height;
    }
}
