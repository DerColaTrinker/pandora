﻿using Pandora.Runtime;
using Pandora.Runtime.SFML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Drawing
{
    /// <summary>
    /// 
    /// </summary>
    public class Image : PointerObject
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Image(uint width, uint height) :
            this(width, height, Color.Black)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="color"></param>
        public Image(uint width, uint height, Color color)
        {
            InternalPointer = NativeSFMLCalls.sfImage_createFromColor(width, height, color);

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("image");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public Image(string filename)
        {
            InternalPointer = NativeSFMLCalls.sfImage_createFromFile(filename);

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("image", filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        public Image(Stream stream)
        {
            using (StreamAdaptor adaptor = new StreamAdaptor(stream))
            {
                InternalPointer = NativeSFMLCalls.sfImage_createFromStream(adaptor.InputStreamPtr);
            }

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("image");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        public Image(byte[] bytes)
        {
            GCHandle pin = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            try
            {
                InternalPointer = NativeSFMLCalls.sfImage_createFromMemory(pin.AddrOfPinnedObject(), Convert.ToUInt64(bytes.Length));
            }
            finally
            {
                pin.Free();
            }
            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("image");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pixels"></param>
        public Image(Color[,] pixels)
        {
            uint Width = (uint)pixels.GetLength(0);
            uint Height = (uint)pixels.GetLength(1);

            // Transpose the array (.Net gives dimensions in reverse order of what SFML expects)
            Color[,] transposed = new Color[Height, Width];
            for (int x = 0 ; x < Width ; ++x)
                for (int y = 0 ; y < Height ; ++y)
                    transposed[y, x] = pixels[x, y];

            unsafe
            {
                fixed (Color* PixelsPtr = transposed)
                {
                    InternalPointer = NativeSFMLCalls.sfImage_createFromPixels(Width, Height, (byte*)PixelsPtr);
                }
            }

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("image");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="pixels"></param>
        public Image(uint width, uint height, byte[] pixels)
        {
            unsafe
            {
                fixed (byte* PixelsPtr = pixels)
                {
                    InternalPointer = NativeSFMLCalls.sfImage_createFromPixels(width, height, PixelsPtr);
                }
            }

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("image");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public Image(Image copy)
        {
            InternalPointer = NativeSFMLCalls.sfImage_copy(copy.InternalPointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool SaveToFile(string filename)
        {
            return NativeSFMLCalls.sfImage_saveToFile(InternalPointer, filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        public void CreateMaskFromColor(Color color)
        {
            CreateMaskFromColor(color, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        /// <param name="alpha"></param>
        public void CreateMaskFromColor(Color color, byte alpha)
        {
            NativeSFMLCalls.sfImage_createMaskFromColor(InternalPointer, color, alpha);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destX"></param>
        /// <param name="destY"></param>
        public void Copy(Image source, uint destX, uint destY)
        {
            Copy(source, destX, destY, new IntRect(0, 0, 0, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destX"></param>
        /// <param name="destY"></param>
        /// <param name="sourceRect"></param>
        public void Copy(Image source, uint destX, uint destY, IntRect sourceRect)
        {
            Copy(source, destX, destY, sourceRect, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destX"></param>
        /// <param name="destY"></param>
        /// <param name="sourceRect"></param>
        /// <param name="applyAlpha"></param>
        public void Copy(Image source, uint destX, uint destY, IntRect sourceRect, bool applyAlpha)
        {
            NativeSFMLCalls.sfImage_copyImage(InternalPointer, source.InternalPointer, destX, destY, sourceRect, applyAlpha);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Color GetPixel(uint x, uint y)
        {
            return NativeSFMLCalls.sfImage_getPixel(InternalPointer, x, y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="color"></param>
        public void SetPixel(uint x, uint y, Color color)
        {
            NativeSFMLCalls.sfImage_setPixel(InternalPointer, x, y, color);
        }

        /// <summary>
        /// 
        /// </summary>
        public byte[] Pixels
        {
            get
            {
                Vector2u size = Size;
                byte[] PixelsPtr = new byte[size.X * size.Y * 4];
                Marshal.Copy(NativeSFMLCalls.sfImage_getPixelsPtr(InternalPointer), PixelsPtr, 0, PixelsPtr.Length);
                return PixelsPtr;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2u Size
        {
            get { return NativeSFMLCalls.sfImage_getSize(InternalPointer); }
        }

        /// <summary>
        /// 
        /// </summary>
        public void FlipHorizontally()
        {
            NativeSFMLCalls.sfImage_flipHorizontally(InternalPointer);
        }

        /// <summary>
        /// 
        /// </summary>
        public void FlipVertically()
        {
            NativeSFMLCalls.sfImage_flipVertically(InternalPointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[Image]" +
                   " Size(" + Size + ")";
        }

        internal Image(IntPtr pointer)
        {
            InternalPointer = pointer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            NativeSFMLCalls.sfImage_destroy(InternalPointer);
        }
    }
}
