﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Drawing
{
    /// <summary>
    /// Stellt ein Rechteckbereich dar
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FloatRect : IEquatable<FloatRect>
    {
        /// <summary>
        /// Neuer Rechteckbereich
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public FloatRect(float left, float top, float width, float height)
        {
            Left = left;
            Top = top;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Neuer Rechteckbereich
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        public FloatRect(Vector2f position, Vector2f size)
            : this(position.X, position.Y, size.X, size.Y)
        {
        }

        public Vector2f Position { get { return new Vector2f(Left, Top); } }

        public Vector2f Size { get { return new Vector2f(Width, Height); } }

        /// <summary>
        /// Gibt an ob sich die Koordinaten innerhalb des Rechtecks befinden
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Contains(float x, float y)
        {
            float minX = Math.Min(Left, Left + Width);
            float maxX = Math.Max(Left, Left + Width);
            float minY = Math.Min(Top, Top + Height);
            float maxY = Math.Max(Top, Top + Height);

            return (x >= minX) && (x < maxX) && (y >= minY) && (y < maxY);
        }

        /// <summary>
        /// Gibt an ob sich die Koordinaten innerhalb des Rechtecks befinden
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public bool Contains(Vector2f vector)
        {
            return Contains(vector.X, vector.Y);
        }

        /// <summary>
        /// Gibt an ob sich das angegebene Rechteck innerhalb dieses Rechtecks befindet
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public bool Intersects(FloatRect rect)
        {
            FloatRect overlap;
            return Intersects(rect, out overlap);
        }

        /// <summary>
        /// Gibt an ob sich das angegebene Rechteck innerhalb dieses Rechtecks befindet
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="overlap"></param>
        /// <returns></returns>
        public bool Intersects(FloatRect rect, out FloatRect overlap)
        {
            // Rectangles with negative dimensions are allowed, so we must handle them correctly

            // Compute the min and max of the first rectangle on both axes
            float r1MinX = Math.Min(Left, Left + Width);
            float r1MaxX = Math.Max(Left, Left + Width);
            float r1MinY = Math.Min(Top, Top + Height);
            float r1MaxY = Math.Max(Top, Top + Height);

            // Compute the min and max of the second rectangle on both axes
            float r2MinX = Math.Min(rect.Left, rect.Left + rect.Width);
            float r2MaxX = Math.Max(rect.Left, rect.Left + rect.Width);
            float r2MinY = Math.Min(rect.Top, rect.Top + rect.Height);
            float r2MaxY = Math.Max(rect.Top, rect.Top + rect.Height);

            // Compute the intersection boundaries
            float interLeft = Math.Max(r1MinX, r2MinX);
            float interTop = Math.Max(r1MinY, r2MinY);
            float interRight = Math.Min(r1MaxX, r2MaxX);
            float interBottom = Math.Min(r1MaxY, r2MaxY);

            // If the intersection is valid (positive non zero area), then there is an intersection
            if ((interLeft < interRight) && (interTop < interBottom))
            {
                overlap.Left = interLeft;
                overlap.Top = interTop;
                overlap.Width = interRight - interLeft;
                overlap.Height = interBottom - interTop;
                return true;
            }
            else
            {
                overlap.Left = 0;
                overlap.Top = 0;
                overlap.Width = 0;
                overlap.Height = 0;
                return false;
            }
        }

        /// <summary>
        /// Gibt eine Zeichenkette aus
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[FloatRect]" +
                   " Left(" + Left + ")" +
                   " Top(" + Top + ")" +
                   " Width(" + Width + ")" +
                   " Height(" + Height + ")";
        }

        /// <summary>
        /// Vergleicht zwei Rechtecke
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (obj is FloatRect) && obj.Equals(this);
        }

        /// <summary>
        /// Vergleicht zwei Rechtecke
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(FloatRect other)
        {
            return (Left == other.Left) &&
                   (Top == other.Top) &&
                   (Width == other.Width) &&
                   (Height == other.Height);
        }

        /// <summary>
        /// Liefrt einen Hashcode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return unchecked((int)((uint)Left ^
                   (((uint)Top << 13) | ((uint)Top >> 19)) ^
                   (((uint)Width << 26) | ((uint)Width >> 6)) ^
                   (((uint)Height << 7) | ((uint)Height >> 25))));
        }

        /// <summary>
        /// Gleich
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static bool operator ==(FloatRect r1, FloatRect r2)
        {
            return r1.Equals(r2);
        }

        /// <summary>
        /// Ungleich
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static bool operator !=(FloatRect r1, FloatRect r2)
        {
            return !r1.Equals(r2);
        }

        /// <summary>
        /// Konvertiert ein FloatRect in ein IntRect
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public static implicit operator IntRect(FloatRect r)
        {
            return new IntRect((int)r.Left,
                               (int)r.Top,
                               (int)r.Width,
                               (int)r.Height);
        }

        /// <summary>
        /// X-Koordinaten Punkt
        /// </summary>
        public float Left;

        /// <summary>
        /// Y-Koodinaten Punkt
        /// </summary>
        public float Top;

        /// <summary>
        /// Breite
        /// </summary>
        public float Width;

        /// <summary>
        /// Höhe
        /// </summary>
        public float Height;

        
    }
}
