﻿using Pandora.Game.Interaction.GUI.Animations;
using Pandora.Game.Interaction.GUI.Controls;
using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Interaction.GUI
{
    /// <summary>
    /// 
    /// </summary>
    public interface IVisualHandler
    {
        /// <summary>
        /// 
        /// </summary>
        Vector2u Size { get; }

        /// <summary>
        /// 
        /// </summary>
        InteractionManager Manager { get; }

        /// <summary>
        /// 
        /// </summary>
        AnimationHandler Animation { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        void Show(Scene scene);
    }
}
