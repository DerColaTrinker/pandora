﻿using Pandora.Game.Interaction.Events;
using Pandora.Game.Interaction.GUI.Controls;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Renderer;
using Pandora.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TRoot"></typeparam>
    public sealed class VisualHandler : IVisualHandler, IRenderTarget
    {
        internal VisualHandler(InteractionManager manager)
        {
            Manager = manager;
            Engine = manager.Engine;
        }

        internal bool Initialize()
        {
            Logger.Trace("Loading context settings");
            _contextsettings.DepthBits = Engine.Configuration.GetValue<uint>("Visuals", "DepthBits", 0);
            _contextsettings.StencilBits = Engine.Configuration.GetValue<uint>("Visual", "StencilBits", 0);
            _contextsettings.AntialiasingLevel = Engine.Configuration.GetValue<uint>("Visual", "AntialiasingLevel", 0);
            _contextsettings.MajorVersion = Engine.Configuration.GetValue<uint>("Visual", "OpenGLMajorVersion", 2);
            _contextsettings.MinorVersion = Engine.Configuration.GetValue<uint>("Visual", "OpenGLMinorVersion", 0);

            Logger.Trace("Loading video settings");
            var width = Engine.Configuration.GetValue<uint>("Visual", "ScreenWidth", 1024);
            var height = Engine.Configuration.GetValue<uint>("Visual", "ScreenHeight", 786);
            var bpp = Engine.Configuration.GetValue<uint>("Visual", "BPP", 32);
            var style = WindowStyle.Close | WindowStyle.Titlebar | WindowStyle.Resize;

            var video = new VideoMode(width, height, bpp);

            try
            {
                _window = NativeSFMLCalls.sfRenderWindow_create(video, "Pandora", style, ref _contextsettings);
                DefaultView = new View(NativeSFMLCalls.sfRenderWindow_getDefaultView(_window));
            }
            catch (Exception ex)
            {
                Logger.Error("VisualHandler target Window failed");
                Logger.Exception(ex);
                return false;
            }

            Logger.Trace("Configurate target window");
            var vsync = Engine.Configuration.GetValue<bool>("Visual", "VSync", false);
            var maxfps = Engine.Configuration.GetValue<uint>("Visual", "MaxFPS", 30);
            NativeSFMLCalls.sfRenderWindow_setActive(_window, true);
            NativeSFMLCalls.sfRenderWindow_setVerticalSyncEnabled(_window, vsync);
            if (!vsync) NativeSFMLCalls.sfRenderWindow_setFramerateLimit(_window, maxfps);

            DoubleClickMillisecond = Engine.Configuration.GetValue("Visual", "DoubleClickTime", 1000F);

            InstanceHolder.__VisualHandler = this;

            Animation = new Animations.AnimationHandler(this);

            Logger.Normal("SFML Visual initialized");
            return true;
        }

        public InteractionManager Manager { get; private set; }

        public PandoraEngine Engine { get; private set; }

        public float DoubleClickMillisecond { get; set; }

        #region Visual Handler Impl

        private Color _clearcolor = new Color(60, 60, 60);
        private List<Scene> _scenes = new List<Scene>();
        private Scene[] _scenerenderpipeline = new Scene[] { };

        /// <summary>
        /// Zeigt ein Fenster an
        /// </summary>
        /// <param name="scene"></param>
        public void Show(Scene scene)
        {
            if (!scene.IsInitialized)
                scene.InternalLoad();

            scene.ZOrder = _scenes.Count > 0 ? _scenes.Max(m => m.ZOrder) + 1 : 1;

            if (!_scenes.Contains(scene))
            {
                _scenes.Add(scene);
                scene.InternalShow();
            }

            ReorderWindows();
        }

        private void ReorderWindows()
        {
            var lowlimit = false;

            /*
            * Das Fenster-Rendersystem besitzt ein tiefen limit, das bedeutet das nur bis zum Vollbild gerendert wird der sich an Position 0,0 befindet. 
            * Das soll verhindern, das bei mehreren Vollbild-Fenstern alle gerendert und überdeckt werden.
            */
            _scenerenderpipeline = _scenes.OrderByDescending(scene => scene.ZOrder).Select(window =>
            {
                if (lowlimit) return null;
                if (window.Size.ToVector2u() >= Size & window.Position <= InstanceHolder.__zerovector) lowlimit = true;

                return window;
            }).Where(window => window != null).ToArray();
        }

        /// <summary>
        /// Bringt ein Fenster aus dem Hintergrund nach vorn
        /// </summary>
        /// <param name="scene"></param>
        public void BringToFront(Scene scene)
        {
            scene.ZOrder = _scenes.Max(m => m.ZOrder) + 1;
        }

        internal void RenderUpdate()
        {
            // Wenn das Fenster nicht mehr offen ist, die Runtime verlassen
            if (!NativeSFMLCalls.sfRenderWindow_isOpen(_window))
            {
                //TODO: Das Fenster wurde geschlossen
            }

            // Das Rendern neu auslösen           
            NativeSFMLCalls.sfRenderWindow_clear(_window, Color.Black);

            // Alle Window Rendern die Sichbar sind.
            foreach (var window in _scenerenderpipeline)
            {
                if (!window.Visible) continue;

                window.InternalRenderUpdate(this);
            }

            NativeSFMLCalls.sfRenderWindow_display(_window);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        internal void SystemUpdate(float ms, float s)
        {
            DispatchEvents(ms);
            Animation.SystemUpdate(ms, s);
            RenderUpdate();
        }

        #endregion

        #region SFML RenderWindow Impl

        private ContextSettings _contextsettings;
        private IntPtr _window;
        private float _doubleclicktimer;
        private Vector2f _position;

        /// <summary>
        /// 
        /// </summary>
        public Vector2i Position
        {
            get { return NativeSFMLCalls.sfRenderWindow_getPosition(_window); }
            set { NativeSFMLCalls.sfRenderWindow_setPosition(_window, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2u Size
        {
            get { return NativeSFMLCalls.sfRenderWindow_getSize(_window); }
            set { NativeSFMLCalls.sfRenderWindow_setSize(_window, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        public void SetTitle(string title)
        {
            // Copy the title to a null-terminated UTF-32 byte array
            byte[] titleAsUtf32 = Encoding.UTF32.GetBytes(title + '\0');

            unsafe
            {
                fixed (byte* titlePtr = titleAsUtf32)
                {
                    NativeSFMLCalls.sfRenderWindow_setUnicodeTitle(_window, (IntPtr)titlePtr);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="pixels"></param>
        public void SetIcon(uint width, uint height, byte[] pixels)
        {
            unsafe
            {
                fixed (byte* PixelsPtr = pixels)
                {
                    NativeSFMLCalls.sfRenderWindow_setIcon(_window, width, height, PixelsPtr);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="visible"></param>
        public void SetVisible(bool visible)
        {
            NativeSFMLCalls.sfRenderWindow_setVisible(_window, visible);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="visible"></param>
        public void SetMouseCursorVisible(bool visible)
        {
            NativeSFMLCalls.sfRenderWindow_setMouseCursorVisible(_window, visible);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enable"></param>
        public void SetKeyRepeatEnabled(bool enable)
        {
            NativeSFMLCalls.sfRenderWindow_setKeyRepeatEnabled(_window, enable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="active"></param>
        /// <returns></returns>
        public bool SetActive(bool active)
        {
            return NativeSFMLCalls.sfRenderWindow_setActive(_window, active);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="threshold"></param>
        public void SetJoystickThreshold(float threshold)
        {
            NativeSFMLCalls.sfRenderWindow_setJoystickThreshold(_window, threshold);
        }

        /// <summary>
        /// 
        /// </summary>
        public IntPtr SystemHandle
        {
            get { return NativeSFMLCalls.sfRenderWindow_getSystemHandle(_window); }
        }

        /// <summary>
        /// 
        /// </summary>
        public View DefaultView
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public View GetView()
        {
            return new View(NativeSFMLCalls.sfRenderWindow_getView(_window));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        public void SetView(View view)
        {
            NativeSFMLCalls.sfRenderWindow_setView(_window, view.Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        public IntRect GetViewport(View view)
        {
            return NativeSFMLCalls.sfRenderWindow_getViewport(_window, view.Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public Vector2f MapPixelToCoords(Vector2i point)
        {
            return MapPixelToCoords(point, GetView());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        public Vector2f MapPixelToCoords(Vector2i point, View view)
        {
            return NativeSFMLCalls.sfRenderWindow_mapPixelToCoords(_window, point, view != null ? view.Pointer : IntPtr.Zero);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public Vector2i MapCoordsToPixel(Vector2f point)
        {
            return MapCoordsToPixel(point, GetView());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        public Vector2i MapCoordsToPixel(Vector2f point, View view)
        {
            return NativeSFMLCalls.sfRenderWindow_mapCoordsToPixel(_window, point, view != null ? view.Pointer : IntPtr.Zero);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            NativeSFMLCalls.sfRenderWindow_clear(_window, Color.Black);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        public void Clear(Color color)
        {
            NativeSFMLCalls.sfRenderWindow_clear(_window, color);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        /// <param name="state"></param>
        public void DrawShape(IntPtr pointer, RenderStates state)
        {
            var marshal = state.Marshal();

            NativeSFMLCalls.sfRenderWindow_drawShape(_window, pointer, ref marshal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        /// <param name="state"></param>
        public void DrawText(IntPtr pointer, RenderStates state)
        {
            var marshal = state.Marshal();

            NativeSFMLCalls.sfRenderWindow_drawText(_window, pointer, ref marshal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pointer"></param>
        /// <param name="state"></param>
        public void DrawVertexArray(IntPtr Pointer, RenderStates state)
        {
            var marshal = state.Marshal();

            NativeSFMLCalls.sfRenderWindow_drawVertexArray(_window, Pointer, ref marshal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="type"></param>
        public void Draw(Vertex[] vertices, PrimitiveType type)
        {
            Draw(vertices, type, RenderStates.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="type"></param>
        /// <param name="states"></param>
        public void Draw(Vertex[] vertices, PrimitiveType type, RenderStates states)
        {
            Draw(vertices, 0, (uint)vertices.Length, type, states);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type)
        {
            Draw(vertices, start, count, type, RenderStates.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        /// <param name="states"></param>
        public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type, RenderStates states)
        {
            RenderStates.MarshalData marshaledStates = states.Marshal();

            unsafe
            {
                fixed (Vertex* vertexPtr = vertices)
                {
                    NativeSFMLCalls.sfRenderWindow_drawPrimitives(_window, vertexPtr + start, count, type, ref marshaledStates);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void PushGLStates()
        {
            NativeSFMLCalls.sfRenderWindow_pushGLStates(_window);
        }

        /// <summary>
        /// 
        /// </summary>
        public void PopGLStates()
        {
            NativeSFMLCalls.sfRenderWindow_popGLStates(_window);
        }

        /// <summary>
        /// 
        /// </summary>
        public void ResetGLStates()
        {
            NativeSFMLCalls.sfRenderWindow_resetGLStates(_window);
        }

        #endregion

        #region Events

        /// <summary>
        /// 
        /// </summary>
        public void WaitAndDispatchEvents()
        {
            Event e;
            if (NativeSFMLCalls.sfRenderWindow_waitEvent(_window, out e))
                CallEventHandler(e);
        }

        /// <summary>
        /// 
        /// </summary>
        public void DispatchEvents(float ms)
        {
            var e = new Event();

            _doubleclicktimer += ms;

            while (NativeSFMLCalls.sfRenderWindow_pollEvent(_window, out e))
                CallEventHandler(e);
        }

        private void CallEventHandler(Event e)
        {
            switch (e.Type)
            {
                case EventType.Closed:
                    //if (Closed != null)
                    //    Closed(this);
                    break;

                case EventType.GainedFocus:
                    //if (GainedFocus != null)
                    //    GainedFocus(this);
                    break;

                case EventType.JoystickButtonPressed:
                    //if (JoystickButtonPressed != null)
                    //    JoystickButtonPressed(this, e.JoystickButton.JoystickId, e.JoystickButton.Button);
                    break;

                case EventType.JoystickButtonReleased:
                    //if (JoystickButtonReleased != null)
                    //    JoystickButtonReleased(this, e.JoystickButton.JoystickId, e.JoystickButton.Button);
                    break;

                case EventType.JoystickMoved:
                    //if (JoystickMoved != null)
                    //    JoystickMoved(this, e.JoystickMove.JoystickId, e.JoystickMove.Axis, e.JoystickMove.Position);
                    break;

                case EventType.JoystickConnected:
                    //if (JoystickConnected != null)
                    //    JoystickConnected(this, e.JoystickConnect.JoystickId);
                    break;

                case EventType.JoystickDisconnected:
                    //if (JoystickDisconnected != null)
                    //    JoystickDisconnected(this, e.JoystickConnect.JoystickId);
                    break;

                case EventType.KeyPressed:
                    //if (KeyPressed != null)
                    //    KeyPressed(this, e.Key.Code, e.Key.Alt != 0, e.Key.Control != 0, e.Key.Shift != 0, e.Key.System != 0);
                    break;

                case EventType.KeyReleased:
                    //if (KeyReleased != null)
                    //    KeyReleased(this, e.Key.Code, e.Key.Alt != 0, e.Key.Control != 0, e.Key.Shift != 0, e.Key.System != 0);
                    break;

                case EventType.LostFocus:
                    //if (LostFocus != null)
                    //    LostFocus(this);
                    break;

                case EventType.MouseButtonPressed:
                    {
                        _position = new Vector2f(e.MouseButton.X, e.MouseButton.Y);
                        for (int i = _scenerenderpipeline.Length - 1; i >= 0; i--)
                        {
                            if (_scenerenderpipeline[i].ScreenBounds.Contains(_position))
                                _scenerenderpipeline[i].InternalMouseButtonPressed(_position, e.MouseButton.Button);
                        }
                    }
                    break;

                case EventType.MouseButtonReleased:
                    {
                        _position = new Vector2f(e.MouseButton.X, e.MouseButton.Y);

                        if (_doubleclicktimer > DoubleClickMillisecond)
                        {
                            _doubleclicktimer = 0;
                        }
                        else
                        {
                            for (int i = _scenerenderpipeline.Length - 1; i >= 0; i--)
                            {
                                if (_scenerenderpipeline[i].ScreenBounds.Contains(_position))
                                    _scenerenderpipeline[i].InternalMouseDoubleClick(_position);

                                _doubleclicktimer = DoubleClickMillisecond;
                            }
                        }

                        for (int i = _scenerenderpipeline.Length - 1; i >= 0; i--)
                        {
                            if (_scenerenderpipeline[i].ScreenBounds.Contains(_position))
                                _scenerenderpipeline[i].InternalMouseButtonReleased(_position, e.MouseButton.Button);
                        }
                    }
                    break;

                case EventType.MouseEntered:
                    //if (MouseEntered != null)
                    //    MouseEntered(this);
                    break;

                case EventType.MouseLeft:
                    //if (MouseLeft != null)
                    //    MouseLeft(this);
                    break;

                case EventType.MouseMoved:
                    {
                        _position = new Vector2f(e.MouseMove.X, e.MouseMove.Y);
                        for (int i = _scenerenderpipeline.Length - 1; i >= 0; i--)
                        {
                            if (_scenerenderpipeline[i].ScreenBounds.Contains(_position))
                                _scenerenderpipeline[i].InternalMouseMoved(_position);
                        }
                    }
                    break;

                case EventType.MouseWheelMoved:
                    {
                        _position = new Vector2f(e.MouseWheel.X, e.MouseWheel.Y);
                        for (int i = _scenerenderpipeline.Length - 1; i >= 0; i--)
                        {
                            if (_scenerenderpipeline[i].ScreenBounds.Contains(_position))
                                _scenerenderpipeline[i].InternalMouseWheel(_position, e.MouseWheel.Delta);
                        }
                    }
                    break;

                case EventType.Resized:
                    {
                        //sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
                        //window.setView(sf::View(visibleArea));

                        var center = Vector2f.Zero;
                        var size = new Vector2f(e.Size.Width, e.Size.Height);
                        Size = size.ToVector2u();

                        SetView(new View(new FloatRect(center, Size.ToVector2f())));

                        for (int i = _scenerenderpipeline.Length - 1; i >= 0; i--)
                        {
                            if (_scenerenderpipeline[i].ScreenBounds.Contains(_position))
                                _scenerenderpipeline[i].InternalSceneResize(size);
                        }
                    }
                    break;

                case EventType.TextEntered:
                    //if (TextEntered != null)
                    //{
                    //    var unicode = Char.ConvertFromUtf32((int)e.Text.Unicode);
                    //    TextEntered(this, unicode);
                    //}
                    break;

                case EventType.TouchBegan:
                    //if (TouchBegan != null)
                    //    TouchBegan(this, e.Touch.Finger, e.Touch.X, e.Touch.Y);
                    break;

                case EventType.TouchMoved:
                    //if (TouchMoved != null)
                    //    TouchMoved(this, e.Touch.Finger, e.Touch.X, e.Touch.Y);
                    break;

                case EventType.TouchEnded:
                    //if (TouchEnded != null)
                    //    TouchEnded(this, e.Touch.Finger, e.Touch.X, e.Touch.Y);
                    break;

                case EventType.SensorChanged:
                    //if (SensorChanged != null)
                    //    SensorChanged(this, e.Sensor.Type, e.Sensor.X, e.Sensor.Y, e.Sensor.Z);
                    break;

                default:
                    break;
            }
        }

        #endregion

        #region IVisualHandler Member

        /// <summary>
        /// 
        /// </summary>
        public Animations.AnimationHandler Animation
        {
            get;
            private set;
        }

        #endregion

        #region IPointerObject Member

        /// <summary>
        /// 
        /// </summary>
        public IntPtr Pointer
        {
            get { return _window; }
        }

        #endregion
    }
}
