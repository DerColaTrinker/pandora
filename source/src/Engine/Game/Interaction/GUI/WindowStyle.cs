﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI
{
    /// <summary>
    /// 
    /// </summary>
    [Flags]
    public enum WindowStyle
    {
        /// <summary>
        /// 
        /// </summary>
        None = 0,

        /// <summary>
        /// 
        /// </summary>
        Titlebar = 1 << 0,

        /// <summary>
        /// 
        /// </summary>
        Resize = 1 << 1,

        /// <summary>
        /// 
        /// </summary>
        Close = 1 << 2,

        /// <summary>
        /// 
        /// </summary>
        Fullscreen = 1 << 3,

        /// <summary>
        /// 
        /// </summary>
        Default = Titlebar | Resize | Close
    }
}
