﻿using Pandora.Game.Interaction.GUI.Controls;
using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI
{
    internal static class InstanceHolder
    {
        internal static IVisualHandler __VisualHandler { get; set; }
        internal static Vector2f __zerovector = new Vector2f();
        internal static Control __LastMouseButtonReleaseControl;
        internal static Control __LastMouseOverControl;

        //internal static Control __hovercontrol;
    }
}
