﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    public enum Equation
    {
        /// <summary>Pixel = Src * SrcFactor + Dst * DstFactor</summary>
        Add,

        /// <summary>Pixel = Src * SrcFactor - Dst * DstFactor</summary>
        Subtract
    }
}
