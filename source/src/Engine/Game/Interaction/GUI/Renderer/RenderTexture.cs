using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using Pandora.Game.Interaction.GUI.Drawing;

namespace Pandora.Game.Interaction.GUI.Renderer
{
    //public class RenderTexture : PointerObject, IRenderTarget
    //{
    //    private View _view = null;
    //    private Texture _texture = null;

    //    public RenderTexture(uint width, uint height) :
    //        this(width, height, false)
    //    { }

    //    public RenderTexture(uint width, uint height, bool depthBuffer)
    //    {
    //        InternalPointer = NativeSFMLCalls.sfRenderTexture_create(width, height, depthBuffer);
    //        _view = new View(NativeSFMLCalls.sfRenderTexture_getDefaultView(Pointer));
    //        _texture = new Texture(NativeSFMLCalls.sfRenderTexture_getTexture(Pointer));
    //        GC.SuppressFinalize(_view);
    //        GC.SuppressFinalize(_texture);
    //    }

    //    public bool SetActive(bool active)
    //    {
    //        return NativeSFMLCalls.sfRenderTexture_setActive(Pointer, active);
    //    }

    //    public Vector2u Size
    //    {
    //        get { return NativeSFMLCalls.sfRenderTexture_getSize(Pointer); }
    //    }

    //    public View DefaultView
    //    {
    //        get { return new View(_view); }
    //    }

    //    public View GetView()
    //    {
    //        return new View(NativeSFMLCalls.sfRenderTexture_getView(Pointer));
    //    }

    //    public void SetView(View view)
    //    {
    //        NativeSFMLCalls.sfRenderTexture_setView(Pointer, view.Pointer);
    //    }

    //    public IntRect GetViewport(View view)
    //    {
    //        return NativeSFMLCalls.sfRenderTexture_getViewport(Pointer, view.Pointer);
    //    }

    //    public Vector2f MapPixelToCoords(Vector2i point)
    //    {
    //        return MapPixelToCoords(point, GetView());
    //    }

    //    public Vector2f MapPixelToCoords(Vector2i point, View view)
    //    {
    //        return NativeSFMLCalls.sfRenderTexture_mapPixelToCoords(Pointer, point, view != null ? view.Pointer : IntPtr.Zero);
    //    }

    //    public Vector2i MapCoordsToPixel(Vector2f point)
    //    {
    //        return MapCoordsToPixel(point, GetView());
    //    }

    //    public Vector2i MapCoordsToPixel(Vector2f point, View view)
    //    {
    //        return NativeSFMLCalls.sfRenderTexture_mapCoordsToPixel(Pointer, point, view != null ? view.Pointer : IntPtr.Zero);
    //    }

    //    public void Clear()
    //    {
    //        NativeSFMLCalls.sfRenderTexture_clear(Pointer, Color.Black);
    //    }

    //    public void Clear(Color color)
    //    {
    //        NativeSFMLCalls.sfRenderTexture_clear(Pointer, color);
    //    }

    //    public void Display()
    //    {
    //        NativeSFMLCalls.sfRenderTexture_display(Pointer);
    //    }

    //    public Texture Texture
    //    {
    //        get { return _texture; }
    //    }

    //    public bool Smooth
    //    {
    //        get { return NativeSFMLCalls.sfRenderTexture_isSmooth(Pointer); }
    //        set { NativeSFMLCalls.sfRenderTexture_setSmooth(Pointer, value); }
    //    }

    //    public void Draw(IDrawable drawable)
    //    {
    //        Draw(drawable, RenderStates.Default);
    //    }

    //    public void Draw(IDrawable drawable, RenderStates states)
    //    {
    //        drawable.Draw(this, states);
    //    }

    //    public void Draw(Vertex[] vertices, PrimitiveType type)
    //    {
    //        Draw(vertices, type, RenderStates.Default);
    //    }

    //    public void Draw(Vertex[] vertices, PrimitiveType type, RenderStates states)
    //    {
    //        Draw(vertices, 0, (uint)vertices.Length, type, states);
    //    }

    //    public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type)
    //    {
    //        Draw(vertices, start, count, type, RenderStates.Default);
    //    }

    //    public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type, RenderStates states)
    //    {
    //        RenderStates.MarshalData marshaledStates = states.Marshal();

    //        unsafe
    //        {
    //            fixed (Vertex* vertexPtr = vertices)
    //            {
    //                NativeSFMLCalls.sfRenderTexture_drawPrimitives(Pointer, vertexPtr + start, count, type, ref marshaledStates);
    //            }
    //        }
    //    }

    //    public void PushGLStates()
    //    {
    //        NativeSFMLCalls.sfRenderTexture_pushGLStates(Pointer);
    //    }

    //    public void PopGLStates()
    //    {
    //        NativeSFMLCalls.sfRenderTexture_popGLStates(Pointer);
    //    }

    //    public void ResetGLStates()
    //    {
    //        NativeSFMLCalls.sfRenderTexture_resetGLStates(Pointer);
    //    }

    //    public override string ToString()
    //    {
    //        return "[RenderTexture]" +
    //               " Size(" + Size + ")" +
    //               " Texture(" + Texture + ")" +
    //               " DefaultView(" + DefaultView + ")" +
    //               " View(" + GetView() + ")";
    //    }

    //    protected override void Destroy(bool disposing)
    //    {
    //        if (!disposing)
    //            Context.Global.SetActive(true);

    //        NativeSFMLCalls.sfRenderTexture_destroy(Pointer);

    //        if (disposing)
    //        {
    //            _view.Dispose();
    //            _texture.Dispose();
    //        }

    //        if (!disposing)
    //            Context.Global.SetActive(false);
    //    }
    //}
}
