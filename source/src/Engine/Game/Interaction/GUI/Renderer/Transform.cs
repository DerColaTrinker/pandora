﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Transform
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a00"></param>
        /// <param name="a01"></param>
        /// <param name="a02"></param>
        /// <param name="a10"></param>
        /// <param name="a11"></param>
        /// <param name="a12"></param>
        /// <param name="a20"></param>
        /// <param name="a21"></param>
        /// <param name="a22"></param>
        public Transform(float a00, float a01, float a02,
                         float a10, float a11, float a12,
                         float a20, float a21, float a22)
        {
            m00 = a00; m01 = a01; m02 = a02;
            m10 = a10; m11 = a11; m12 = a12;
            m20 = a20; m21 = a21; m22 = a22;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Transform GetInverse()
        {
            return NativeSFMLCalls.sfTransform_getInverse(ref this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Vector2f TransformPoint(float x, float y)
        {
            return TransformPoint(new Vector2f(x, y));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public Vector2f TransformPoint(Vector2f point)
        {
            return NativeSFMLCalls.sfTransform_transformPoint(ref this, point);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rectangle"></param>
        /// <returns></returns>
        public FloatRect TransformRect(FloatRect rectangle)
        {
            return NativeSFMLCalls.sfTransform_transformRect(ref this, rectangle);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transform"></param>
        public void Combine(Transform transform)
        {
            NativeSFMLCalls.sfTransform_combine(ref this, ref transform);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Translate(float x, float y)
        {
            NativeSFMLCalls.sfTransform_translate(ref this, x, y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        public void Translate(Vector2f offset)
        {
            Translate(offset.X, offset.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        public void Rotate(float angle)
        {
            NativeSFMLCalls.sfTransform_rotate(ref this, angle);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        /// <param name="centerX"></param>
        /// <param name="centerY"></param>
        public void Rotate(float angle, float centerX, float centerY)
        {
            NativeSFMLCalls.sfTransform_rotateWithCenter(ref this, angle, centerX, centerY);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        /// <param name="center"></param>
        public void Rotate(float angle, Vector2f center)
        {
            Rotate(angle, center.X, center.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        public void Scale(float scaleX, float scaleY)
        {
            NativeSFMLCalls.sfTransform_scale(ref this, scaleX, scaleY);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        /// <param name="centerX"></param>
        /// <param name="centerY"></param>
        public void Scale(float scaleX, float scaleY, float centerX, float centerY)
        {
            NativeSFMLCalls.sfTransform_scaleWithCenter(ref this, scaleX, scaleY, centerX, centerY);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="factors"></param>
        public void Scale(Vector2f factors)
        {
            Scale(factors.X, factors.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="factors"></param>
        /// <param name="center"></param>
        public void Scale(Vector2f factors, Vector2f center)
        {
            Scale(factors.X, factors.Y, center.X, center.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Transform operator *(Transform left, Transform right)
        {
            left.Combine(right);
            return left;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector2f operator *(Transform left, Vector2f right)
        {
            return left.TransformPoint(right);
        }

        /// <summary>
        /// 
        /// </summary>
        public static Transform Identity
        {
            get
            {
                return new Transform(1, 0, 0,
                                     0, 1, 0,
                                     0, 0, 1);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("[Transform]" +
                   " Matrix(" +
                   "{0}, {1}, {2}," +
                   "{3}, {4}, {5}," +
                   "{6}, {7}, {8}, )",
                   m00, m01, m02,
                   m10, m11, m12,
                   m20, m21, m22);
        }

        /// <summary>
        /// 
        /// </summary>
        float m00, m01, m02;

        /// <summary>
        /// 
        /// </summary>
        float m10, m11, m12;

        /// <summary>
        /// 
        /// </summary>
        float m20, m21, m22;
    }
}
