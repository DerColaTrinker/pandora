using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Collections.Generic;
using Pandora.Runtime;
using Pandora.Game.Interaction.GUI.Renderer;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Runtime.SFML;

namespace Pandora.Game.Interaction.GUI.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    public class Shader : PointerObject
    {
        private Dictionary<string, Texture> _textures = new Dictionary<string, Texture>();

        /// <summary>
        /// 
        /// </summary>
        public class CurrentTextureType { }

        /// <summary>
        /// 
        /// </summary>
        public static readonly CurrentTextureType CurrentTexture = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertexShaderFilename"></param>
        /// <param name="fragmentShaderFilename"></param>
        public Shader(string vertexShaderFilename, string fragmentShaderFilename)
        {
            InternalPointer = NativeSFMLCalls.sfShader_createFromFile(vertexShaderFilename, fragmentShaderFilename);

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("shader", vertexShaderFilename + " " + fragmentShaderFilename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertexShaderStream"></param>
        /// <param name="fragmentShaderStream"></param>
        public Shader(Stream vertexShaderStream, Stream fragmentShaderStream)
        {
            StreamAdaptor vertexAdaptor = new StreamAdaptor(vertexShaderStream);
            StreamAdaptor fragmentAdaptor = new StreamAdaptor(fragmentShaderStream);
            InternalPointer = NativeSFMLCalls.sfShader_createFromStream(vertexAdaptor.InputStreamPtr, fragmentAdaptor.InputStreamPtr);
            vertexAdaptor.Dispose();
            fragmentAdaptor.Dispose();

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("shader");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertexShader"></param>
        /// <param name="fragmentShader"></param>
        /// <returns></returns>
        public static Shader FromString(string vertexShader, string fragmentShader)
        {
            IntPtr ptr = NativeSFMLCalls.sfShader_createFromMemory(vertexShader, fragmentShader);
            if (ptr == IntPtr.Zero)
                throw new LoadingFailedException("shader");

            return new Shader(ptr);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="x"></param>
        public void SetParameter(string name, float x)
        {
            NativeSFMLCalls.sfShader_setFloatParameter(InternalPointer, name, x);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetParameter(string name, float x, float y)
        {
            NativeSFMLCalls.sfShader_setFloat2Parameter(InternalPointer, name, x, y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void SetParameter(string name, float x, float y, float z)
        {
            NativeSFMLCalls.sfShader_setFloat3Parameter(InternalPointer, name, x, y, z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        public void SetParameter(string name, float x, float y, float z, float w)
        {
            NativeSFMLCalls.sfShader_setFloat4Parameter(InternalPointer, name, x, y, z, w);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="vector"></param>
        public void SetParameter(string name, Vector2f vector)
        {
            SetParameter(name, vector.X, vector.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="color"></param>
        public void SetParameter(string name, Color color)
        {
            NativeSFMLCalls.sfShader_setColorParameter(InternalPointer, name, color);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="transform"></param>
        public void SetParameter(string name, Transform transform)
        {
            NativeSFMLCalls.sfShader_setTransformParameter(InternalPointer, name, transform);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="texture"></param>
        public void SetParameter(string name, Texture texture)
        {
            _textures[name] = texture;
            NativeSFMLCalls.sfShader_setTextureParameter(InternalPointer, name, texture.Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="current"></param>
        public void SetParameter(string name, CurrentTextureType current)
        {
            NativeSFMLCalls.sfShader_setCurrentTextureParameter(InternalPointer, name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="shader"></param>
        public static void Bind(Shader shader)
        {
            NativeSFMLCalls.sfShader_bind(shader != null ? shader.InternalPointer : IntPtr.Zero);
        }

        /// <summary>
        /// 
        /// </summary>
        public static bool IsAvailable
        {
            get { return NativeSFMLCalls.sfShader_isAvailable(); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[Shader]";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            if (!disposing)
                Context.Global.SetActive(true);

            _textures.Clear();
            NativeSFMLCalls.sfShader_destroy(InternalPointer);

            if (!disposing)
                Context.Global.SetActive(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        public Shader(IntPtr pointer)
        {
            InternalPointer = pointer;
        }
    }
}
