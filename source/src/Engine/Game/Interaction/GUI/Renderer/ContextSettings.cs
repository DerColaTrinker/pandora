using System;
using System.Runtime.InteropServices;

namespace Pandora.Game.Interaction.GUI.Renderer
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct ContextSettings
    {
        public ContextSettings(uint depthBits, uint stencilBits) :
            this(depthBits, stencilBits, 0)
        { }

        public ContextSettings(uint depthBits, uint stencilBits, uint antialiasingLevel) :
            this(depthBits, stencilBits, antialiasingLevel, 2, 0)
        { }

        public ContextSettings(uint depthBits, uint stencilBits, uint antialiasingLevel, uint majorVersion, uint minorVersion)
        {
            DepthBits = depthBits;
            StencilBits = stencilBits;
            AntialiasingLevel = antialiasingLevel;
            MajorVersion = majorVersion;
            MinorVersion = minorVersion;
        }

        public override string ToString()
        {
            return "[ContextSettings]" +
                   " DepthBits(" + DepthBits + ")" +
                   " StencilBits(" + StencilBits + ")" +
                   " AntialiasingLevel(" + AntialiasingLevel + ")" +
                   " MajorVersion(" + MajorVersion + ")" +
                   " MinorVersion(" + MinorVersion + ")";
        }

        public uint DepthBits;

        public uint StencilBits;

        public uint AntialiasingLevel;

        public uint MajorVersion;

        public uint MinorVersion;
    }
}