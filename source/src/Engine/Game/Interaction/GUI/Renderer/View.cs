﻿using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Runtime.SFML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    public class View : PointerObject
    {
        /// <summary>
        /// 
        /// </summary>
        public View() :
            base()
        {
            InternalPointer = NativeSFMLCalls.sfView_create();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewRect"></param>
        public View(FloatRect viewRect)
        {
            InternalPointer = NativeSFMLCalls.sfView_createFromRect(viewRect);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="center"></param>
        /// <param name="size"></param>
        public View(Vector2f center, Vector2f size)
            : this()
        {
            Center = center;
            Size = size;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public View(View copy)
        {
            InternalPointer = NativeSFMLCalls.sfView_copy(copy.InternalPointer);
        }

        internal View(IntPtr pointer)
        {
            InternalPointer = pointer;
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2f Center
        {
            get { return NativeSFMLCalls.sfView_getCenter(InternalPointer); }
            set { NativeSFMLCalls.sfView_setCenter(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2f Size
        {
            get { return NativeSFMLCalls.sfView_getSize(InternalPointer); }
            set { NativeSFMLCalls.sfView_setSize(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Rotation
        {
            get { return NativeSFMLCalls.sfView_getRotation(InternalPointer); }
            set { NativeSFMLCalls.sfView_setRotation(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public FloatRect Viewport
        {
            get { return NativeSFMLCalls.sfView_getViewport(InternalPointer); }
            set { NativeSFMLCalls.sfView_setViewport(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rectangle"></param>
        public void Reset(FloatRect rectangle)
        {
            NativeSFMLCalls.sfView_reset(InternalPointer, rectangle);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        public void Move(Vector2f offset)
        {
            NativeSFMLCalls.sfView_move(InternalPointer, offset);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        public void Rotate(float angle)
        {
            NativeSFMLCalls.sfView_rotate(InternalPointer, angle);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="factor"></param>
        public void Zoom(float factor)
        {
            NativeSFMLCalls.sfView_zoom(InternalPointer, factor);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[View]" +
                   " Center(" + Center + ")" +
                   " Size(" + Size + ")" +
                   " Rotation(" + Rotation + ")" +
                   " Viewport(" + Viewport + ")";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            NativeSFMLCalls.sfView_destroy(InternalPointer);
        }
    }
}
