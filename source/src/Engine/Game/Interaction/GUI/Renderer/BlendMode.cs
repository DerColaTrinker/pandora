﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BlendMode : IEquatable<BlendMode>
    {
        /// <summary>
        /// 
        /// </summary>
        public static readonly BlendMode Alpha = new BlendMode(Factor.SrcAlpha, Factor.OneMinusSrcAlpha, Equation.Add,
                                                               Factor.One, Factor.OneMinusSrcAlpha, Equation.Add);

        /// <summary>
        /// 
        /// </summary>
        public static readonly BlendMode Add = new BlendMode(Factor.SrcAlpha, Factor.One, Equation.Add,
                                                             Factor.One, Factor.One, Equation.Add);

        /// <summary>
        /// 
        /// </summary>
        public static readonly BlendMode Multiply = new BlendMode(Factor.DstColor, Factor.Zero);

        /// <summary>
        /// 
        /// </summary>
        public static readonly BlendMode None = new BlendMode(Factor.One, Factor.Zero);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SourceFactor"></param>
        /// <param name="DestinationFactor"></param>
        public BlendMode(Factor SourceFactor, Factor DestinationFactor)
            : this(SourceFactor, DestinationFactor, Equation.Add)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SourceFactor"></param>
        /// <param name="DestinationFactor"></param>
        /// <param name="BlendEquation"></param>
        public BlendMode(Factor SourceFactor, Factor DestinationFactor, Equation BlendEquation)
            : this(SourceFactor, DestinationFactor, BlendEquation, SourceFactor, DestinationFactor, BlendEquation)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColorSourceFactor"></param>
        /// <param name="ColorDestinationFactor"></param>
        /// <param name="ColorBlendEquation"></param>
        /// <param name="AlphaSourceFactor"></param>
        /// <param name="AlphaDestinationFactor"></param>
        /// <param name="AlphaBlendEquation"></param>
        public BlendMode(Factor ColorSourceFactor, Factor ColorDestinationFactor, Equation ColorBlendEquation, Factor AlphaSourceFactor, Factor AlphaDestinationFactor, Equation AlphaBlendEquation)
        {
            ColorSrcFactor = ColorSourceFactor;
            ColorDstFactor = ColorDestinationFactor;
            ColorEquation = ColorBlendEquation;
            AlphaSrcFactor = AlphaSourceFactor;
            AlphaDstFactor = AlphaDestinationFactor;
            AlphaEquation = AlphaBlendEquation;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(BlendMode left, BlendMode right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(BlendMode left, BlendMode right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (obj is BlendMode) && obj.Equals(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(BlendMode other)
        {
            return (ColorSrcFactor == other.ColorSrcFactor) &&
                   (ColorDstFactor == other.ColorDstFactor) &&
                   (ColorEquation == other.ColorEquation) &&
                   (AlphaSrcFactor == other.AlphaSrcFactor) &&
                   (AlphaDstFactor == other.AlphaDstFactor) &&
                   (AlphaEquation == other.AlphaEquation);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return ColorSrcFactor.GetHashCode() ^
                   ColorDstFactor.GetHashCode() ^
                   ColorEquation.GetHashCode() ^
                   AlphaSrcFactor.GetHashCode() ^
                   AlphaDstFactor.GetHashCode() ^
                   AlphaEquation.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        public Factor ColorSrcFactor;

        /// <summary>
        /// 
        /// </summary>
        public Factor ColorDstFactor;

        /// <summary>
        /// 
        /// </summary>
        public Equation ColorEquation;

        /// <summary>
        /// 
        /// </summary>
        public Factor AlphaSrcFactor;

        /// <summary>
        /// 
        /// </summary>
        public Factor AlphaDstFactor;

        /// <summary>
        /// 
        /// </summary>
        public Equation AlphaEquation;
    }
}
