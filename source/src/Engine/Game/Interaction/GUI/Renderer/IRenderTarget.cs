using Pandora.Game.Interaction.GUI.Renderer;
using System;
using System.Runtime.InteropServices;
using System.Security;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Runtime.SFML;


namespace Pandora.Game.Interaction.GUI.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRenderTarget : IPointerObject
    {
        /// <summary>
        /// 
        /// </summary>
        Vector2u Size { get; }

        /// <summary>
        /// 
        /// </summary>
        View DefaultView { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        View GetView();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        void SetView(View view);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        IntRect GetViewport(View view);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        Vector2f MapPixelToCoords(Vector2i point);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        Vector2f MapPixelToCoords(Vector2i point, View view);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        Vector2i MapCoordsToPixel(Vector2f point);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        Vector2i MapCoordsToPixel(Vector2f point, View view);

        //void Clear();

        //void Clear(Color color);

        //void Draw(IDrawable drawable);

        //void Draw(IDrawable drawable, RenderStates states);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="type"></param>
        void Draw(Vertex[] vertices, PrimitiveType type);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="type"></param>
        /// <param name="states"></param>
        void Draw(Vertex[] vertices, PrimitiveType type, RenderStates states);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        /// <param name="states"></param>
        void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type, RenderStates states);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pointer"></param>
        /// <param name="state"></param>
        void DrawShape(IntPtr Pointer, RenderStates state);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pointer"></param>
        /// <param name="state"></param>
        void DrawText(IntPtr Pointer, RenderStates state);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pointer"></param>
        /// <param name="state"></param>
        void DrawVertexArray(IntPtr Pointer, RenderStates state);

        /// <summary>
        /// 
        /// </summary>
        void PushGLStates();

        /// <summary>
        /// 
        /// </summary>
        void PopGLStates();

        /// <summary>
        /// 
        /// </summary>
        void ResetGLStates();


    }
}
