﻿using Pandora.Game.Interaction.GUI.Controls;
using Pandora.Game.Interaction.GUI.Drawing;
using System;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Bezugspunkt Eingenschaften erlaubt
    /// </summary>
    public interface IOriginProperty : IControl
    {
        /// <summary>
        /// Liefert den Bezugspunkt oder legt ihn fest
        /// </summary>
        Vector2f Origin { get; set; }
    }
}
