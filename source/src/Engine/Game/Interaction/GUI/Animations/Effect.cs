﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Ein Delegate das aufgerufen wird, wenn sich der Status eines Effekts ändert
    /// </summary>
    /// <param name="effect"></param>
    /// <param name="status"></param>
    public delegate void EffectStatusChangedDelegate(Effect effect, EffectStatusEnum status);

    /// <summary>
    /// Stellt die Basisklasse für ein Effekt dar
    /// </summary>
    public abstract class Effect
    {
        private EffectStatusEnum _status = EffectStatusEnum.Stopped;

        /// <summary>
        /// Wird aufgerufen wenn der Effektstatus geändert wird
        /// </summary>
        public event EffectStatusChangedDelegate EffectStatusChanged;

        /// <summary>
        /// Erstellt eine neue Instanz der Effect-Klasse
        /// </summary>
        protected Effect()
        { }

        internal void InternalUpdate(float ms, float s)
        {
            Runtime += ms;

            Update(ms, s);
        }

        internal void InternalReset()
        {
            Runtime = 0F;
            IsComplet = false;
        }

        /// <summary>
        /// Gibt an ob der Effekt beendet wurde
        /// </summary>
        public bool IsComplet { get; private set; }

        /// <summary>
        /// Liefert den Status der Animation oder legt ihn fest
        /// </summary>
        public EffectStatusEnum Status
        {
            get { return _status; }
            set
            {
                // Ein Änderung des Effekts nur dann auslösen, wenn auch eine Änderung stattfindet.
                if (_status != value) OnEffectStatusChanged(this, value);

                _status = value;

                switch (_status)
                {
                    case EffectStatusEnum.Stopped:
                        Runtime = 0F;
                        OnStop();
                        break;

                    case EffectStatusEnum.Running:
                        OnStart();
                        IsComplet = false;
                        break;

                    case EffectStatusEnum.Pause:
                        OnPause();
                        break;
                }
            }
        }

        private void OnEffectStatusChanged(Effect effect, EffectStatusEnum value)
        {
            if (EffectStatusChanged != null) EffectStatusChanged.Invoke(effect, value);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt startet
        /// </summary>
        protected virtual void OnStart()
        {
            IsComplet = false;
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt angehalten wird
        /// </summary>
        protected virtual void OnPause()
        {
            // Nichts machen
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt gestoppt wird
        /// </summary>
        protected virtual void OnStop()
        {
            IsComplet = false;
        }

        /// <summary>
        /// Liefert die Laufzeit des Effekts
        /// </summary>
        public float Runtime { get; private set; }

        /// <summary>
        /// Gibt an das die Animation beendet ist
        /// </summary>
        protected virtual void SetComplet()
        {
            IsComplet = true;
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt neu berechnet wird
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected abstract void Update(float ms, float s);

        /// <summary>
        /// Wird aufgerufen wenn der Effekt zurückgesetzt werden muss
        /// </summary>
        protected abstract void Reset();
    }
}
