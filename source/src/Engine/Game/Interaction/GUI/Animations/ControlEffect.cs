﻿using Pandora.Game.Interaction.GUI.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Stellt Basis eines Steuerelement Effekts dar
    /// </summary>
    /// <typeparam name="TProperty"></typeparam>
    public abstract class ControlEffect<TProperty> : Effect where TProperty : IControl
    {
        /// <summary>
        /// Erstellt eine neue Instanz der ControlEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        protected ControlEffect(TProperty control)
            : base()
        {
            Control = control;
        }

        /// <summary>
        /// Liefert das Steuerelement auf das dieser Effekt angelehnt ist
        /// </summary>
        public TProperty Control { get; private set; }
    }
}
