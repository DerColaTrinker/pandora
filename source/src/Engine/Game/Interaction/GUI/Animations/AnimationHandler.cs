﻿using Pandora.Game.Interaction.GUI.Animations.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Ein System, das die Animationen verwaltet.
    /// </summary>
    public sealed class AnimationHandler
    {
        private HashSet<Effect> _effects = new HashSet<Effect>();

        internal AnimationHandler(IVisualHandler handler)
        {
            Visuals = handler;
        }

        internal bool Initialize()
        {
            Logger.Normal("Animation handler ready");
            return true;
        }

        internal void SystemUpdate(float ms, float s)
        {
            // Damit wir die Effekte direkt aus der Liste löschen können, müssen wir auf ForEach verzichten. Dazu nutzen wir RemoveWhere und ein Predikat.
            _effects.RemoveWhere(effect =>
            {
                if (effect.Status == EffectStatusEnum.Running)
                {
                    // Zuerst das Update innerhalb des Effekts aufrufen
                    effect.InternalUpdate(ms, s);
                }

                // Jetzt entscheiden ob es entfernt werden kann
                if (effect.IsComplet)
                {
                    effect.Status = EffectStatusEnum.Stopped;
                    Logger.Trace("Remove effect '{0}' ", effect.GetType().Name);
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }

        /// <summary>
        /// Liefert Zugriff auf den Interaction Manager
        /// </summary>
        public IVisualHandler Visuals { get; private set; }

        /// <summary>
        /// Startet ein Effekt
        /// </summary>
        /// <param name="effect"></param>
        public void Start(Effect effect)
        {
            if (_effects.Add(effect))
            {
                effect.Status = EffectStatusEnum.Running;
                Logger.Trace("Start effect '{0}' ", effect.GetType().Name);
                return;
            }
            else
            {
                //_effects.Add(effect);           
                effect.InternalReset();
                Logger.Trace("Reset effect '{0}' ", effect.GetType().Name);
            }
        }

        /// <summary>
        /// Stoppt ein Effekt
        /// </summary>
        /// <param name="effect"></param>
        public void Stop(Effect effect)
        {
            // Bei einem Stop, den Effekt aus der Liste nehmen
            effect.Status = EffectStatusEnum.Stopped;

            if (_effects.Remove(effect))
                Logger.Trace("Stop effect '{0}' ", effect.GetType().Name);
        }

        /// <summary>
        /// Hält einen Effekt an
        /// </summary>
        /// <param name="effect"></param>
        public void Pause(Effect effect)
        {
            // Bei einer Pause wird nur ein Flag gesetzt
            effect.Status = EffectStatusEnum.Pause;
            Logger.Trace("Pause effect '{0}' ", effect.GetType().Name);
        }

        /// <summary>
        /// Setzt einen Effekt zurück
        /// </summary>
        /// <param name="effect"></param>
        public void Reset(Effect effect)
        {
            effect.InternalReset();
        }
    }
}
