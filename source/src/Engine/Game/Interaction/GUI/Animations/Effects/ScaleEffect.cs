﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Animations.Effects
{
    /// <summary>
    /// 
    /// </summary>
    public class ScaleEffect : ControlEffect<IScaleProperty>
    {
        private Vector2f _source;
        private float _time;
        private Vector2f _factor;
        private Vector2f _target;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public ScaleEffect(IScaleProperty control, Vector2f target, float time)
            : base(control)
        {
            _source = control.Scale;
            _time = time;
            _factor = new Vector2f((control.Scale.X - target.X) / time, (control.Scale.Y - target.Y) / time);
            _target = target;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            // Prüfen ob die Zeit erreicht wurde
            if (Runtime >= _time)
            {
                // Das Objekt an den Zielwert festlegen, da wir auch mit FLOAT nicht genau das Ziel treffen.
                Control.Scale = _target;

                // Als Fertig markieren, damit wird der Effekt aus der Pipeline genommen.
                SetComplet();
            }

            // Größe anpassen entsprechend der vergangenen Zeit und dem Faktor
            Control.Scale += new Vector2f(_factor.X * ms, _factor.Y * ms);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            // UFF...
            Control.Scale = _source;
        }
    }
}
