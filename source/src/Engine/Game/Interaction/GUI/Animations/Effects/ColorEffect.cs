﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Animations.Effects
{
    /// <summary>
    /// Ein Effekt der die Füllfarbe ändert.
    /// </summary>
    /// <remarks> Das Steuerelement muss die <see cref="IFillColorProperty"/> Schnittstelle implementieren</remarks>
    public class ColorEffect : ControlEffect<IColorProperty>
    {
        private float _time;
        private Color _target;
        private float _cR;
        private float _cG;
        private float _cB;
        private float _cA;
        private float _fR;
        private float _fG;
        private float _fB;
        private float _fA;

        /// <summary>
        /// Erstellt eine neue Instnaz der FillColorEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public ColorEffect(IColorProperty control, Color target, float time)
            : base(control)
        {
            _time = time;
            _target = target;
        }

        /// <summary>
        /// Wird beim Start des Effekts aufgerufen
        /// </summary>
        protected override void OnStart()
        {
            ConvertToFloat();
            CreateFactors();

            base.OnStart();
        }

        private void CreateFactors()
        {
            // Faktoren
            _fR = ((float)_target.R - _cR) / _time;
            _fG = ((float)_target.G - _cG) / _time;
            _fB = ((float)_target.B - _cB) / _time;
            _fA = ((float)_target.A - _cA) / _time;
        }

        private void ConvertToFloat()
        {
            // Die Farbwerte als FLOAT speichern, da diese besser errechnet werden können
            _cR = (float)Control.Color.R;
            _cG = (float)Control.Color.G;
            _cB = (float)Control.Color.B;
            _cA = (float)Control.Color.A;
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Effekt aktualisiert werden muss
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            if (Runtime >= _time)
            {
                SetComplet();

                Control.Color = new Color((byte)_target.R,
                                            (byte)_target.G,
                                            (byte)_target.B,
                                            (byte)_target.A);
                return;
            }

            _cR += (_fR * ms);
            _cG += (_fG * ms);
            _cB += (_fB * ms);
            _cA += (_fA * ms);

            Control.Color = new Color((byte)_cR,
                                        (byte)_cG,
                                        (byte)_cB,
                                        (byte)_cA);
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Effekt zurückgesetzt wird
        /// </summary>
        protected override void Reset()
        {
            ConvertToFloat();
            CreateFactors();
        }
    }
}
