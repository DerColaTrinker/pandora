﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Animations.Effects
{
    /// <summary>
    /// Stellt einen Blendeffekt dar.
    /// </summary>
    /// <remarks>Dieser Effekt kann nur Steuerelemente bedienen der die <see cref="IColorProperty"/> schnittstelle implementieren</remarks>
    public class ColorBlendEffect : ControlEffect<IColorProperty>
    {
        private float _time;
        private float _target;
        private float _cR;
        private float _cG;
        private float _cB;
        private float _cA;
        private float _fA;

        /// <summary>
        /// Erstellt eine neue Instanz der BlendEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public ColorBlendEffect(IColorProperty control, byte target, float time)
            : base(control)
        {
            _time = time;
            _target = target;
        }

        /// <summary>
        /// Wird beim Start des Effekts aufgerufen
        /// </summary>
        protected override void OnStart()
        {
            ConvertToFloat();
            CreateFactors();

            base.OnStart();
        }

        private void CreateFactors()
        {
            // Faktoren
            _fA = (_target - _cA) / _time;
        }

        private void ConvertToFloat()
        {
            // Die Farbwerte als FLOAT speichern, da diese besser errechnet werden können
            _cR = (float)Control.Color.R;
            _cG = (float)Control.Color.G;
            _cB = (float)Control.Color.B;
            _cA = (float)Control.Color.A;
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Effekt aktualisiert werden muss
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            if (Runtime >= _time)
            {
                SetComplet();

                Control.Color = new Color((byte)Control.Color.R,
                                        (byte)Control.Color.B,
                                        (byte)Control.Color.G,
                                        (byte)_target);

                return;
            }

            _cA += (_fA * ms);

            Control.Color = new Color((byte)_cR,
                                       (byte)_cG,
                                       (byte)_cB,
                                       (byte)_cA);
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Effekt zurückgesetzt wird
        /// </summary>
        protected override void Reset()
        {
            ConvertToFloat();
            CreateFactors();
        }
    }
}
