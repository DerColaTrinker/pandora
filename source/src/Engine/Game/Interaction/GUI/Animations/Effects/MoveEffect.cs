﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Animations.Effects
{
    /// <summary>
    /// 
    /// </summary>
    public class MoveEffect : ControlEffect<ILocationProperty>
    {
        private Vector2f _factor;
        private Vector2f _target;
        private float _time;
        private Vector2f _source;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public MoveEffect(ILocationProperty control, Vector2f target, float time)
            : base(control)
        {
            _source = control.Position;
            _time = time;
            _factor = new Vector2f((control.Position.X - target.X) / time, (control.Position.Y - target.Y) / time);
            _target = target;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            // Prüfen ob die Zeit erreicht wurde
            if (Runtime >= _time)
            {
                // Das Objekt an die Zielposition lege, da wir auch mit FLOAT nicht genau das Ziel treffen.
                Control.Position = _target;

                // Als Fertig markieren, damit wird der Effekt aus der Pipeline genommen.
                SetComplet();
            }

            // Position anpassen entsprechend der vergangenen Zeit und dem Faktor
            Control.Position += new Vector2f(_factor.X * ms, _factor.Y * ms);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            // UFF...
            Control.Position = _source;
        }
    }
}
