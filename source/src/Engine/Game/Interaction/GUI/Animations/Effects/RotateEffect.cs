﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Animations.Effects
{
    /// <summary>
    /// 
    /// </summary>
    public class RotateEffect : ControlEffect<IRotationProperty>
    {
        private float _factor;
        private float _target;
        private float _time;
        private float _source;
        private DirectionMode _mode;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        /// <param name="mode"></param>
        public RotateEffect(IRotationProperty control, float target, float time, DirectionMode mode)
            : base(control)
        {
            _source = control.Rotation;
            _time = time;
            _target = target;
            _mode = mode;

            // je nach rotation die korrekte Drehrichtung festelegen
            switch (mode)
            {
                // Der Modus verwendet eine Drehrichtung die sich anhand des Deltas ermittelt
                case DirectionMode.Direct:
                    if (target > control.Rotation)
                        _factor = (target - control.Rotation) / time;
                    else
                        _factor = (control.Rotation - target) / time;
                    break;

                // Links Drehung
                case DirectionMode.Left:
                    _factor = (target - control.Rotation) / time;
                    break;

                // Rechts Drehung
                case DirectionMode.Right:
                    _factor = (control.Rotation - target) / time;
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            // Prüfen ob die Zeit erreicht wurde
            if (Runtime >= _time)
            {
                // Das Objekt an die Zielposition lege, da wir auch mit FLOAT nicht genau das Ziel treffen.
                Control.Rotation = _target;

                // Als Fertig markieren, damit wird der Effekt aus der Pipeline genommen.
                SetComplet();
            }

            // Position anpassen entsprechend der vergangenen Zeit und dem Faktor
            Control.Rotation += _factor * ms;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            // UFF...
            Control.Rotation = _source;
        }
    }
}
