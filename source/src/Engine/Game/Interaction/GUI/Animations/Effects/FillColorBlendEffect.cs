﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI.Animations.Effects
{
    /// <summary>
    /// Stellt einen Blendeffekt dar.
    /// </summary>
    /// <remarks>Dieser Effekt kann nur Steuerelemente bedienen der die IFillColorPorperty Schnittstelle implementieren</remarks>
    public class FillColorBlendEffect : ControlEffect<IFillColorProperty>
    {
        private Color _source;
        private float _time;
        private float _target;
        private float _cR;
        private float _cG;
        private float _cB;
        private float _cA;
        private float _fA;

        /// <summary>
        /// Erstellt eine neue Instanz der BlendEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public FillColorBlendEffect(IFillColorProperty control, byte target, float time)
            : base(control)
        {
            _source = control.FillColor;
            _time = time;
            _target = target;

            ConvertToFloat();
            CreateFactors();
        }

        private void CreateFactors()
        {
            // Faktoren
            _fA = ((float)_target - _cA) / _time;
        }

        private void ConvertToFloat()
        {
            // Die Farbwerte als FLOAT speichern, da diese besser errechnet werden können
            _cR = (float)_source.R;
            _cG = (float)_source.G;
            _cB = (float)_source.B;
            _cA = (float)_source.A;
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Effekt aktualisiert werden muss
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            if (Runtime >= _time)
            {
                SetComplet();

                Control.FillColor = new Color((byte)Control.FillColor.R,
                                              (byte)Control.FillColor.B,
                                              (byte)Control.FillColor.G,
                                              (byte)_target);

                return;
            }

            _cA += (_fA * ms);

            Control.FillColor = new Color((byte)_cR,
                                       (byte)_cG,
                                       (byte)_cB,
                                       (byte)_cA);
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Effekt zurückgesetzt wird
        /// </summary>
        protected override void Reset()
        {
            ConvertToFloat();
            CreateFactors();
        }
    }
}
