﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;
using Pandora.Game.Interaction.GUI.Controls;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Positions Eingenschaften erlaubt
    /// </summary>
    public interface IPositionProperty : IControl
    {
        /// <summary>
        /// Liefert die Position oder legt sie fest
        /// </summary>
        Vector2f Location { get; set; }
    }
}
