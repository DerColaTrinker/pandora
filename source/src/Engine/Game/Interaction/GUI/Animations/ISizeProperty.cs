﻿using Pandora.Game.Interaction.GUI.Drawing;
using System;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Größen Eingenschaften erlaubt
    /// </summary>
    public interface ISizeProperty
    {
        /// <summary>
        /// Liefert die Größe oder legt sie fest
        /// </summary>
        Vector2f Size { get; set; }
    }
}
