﻿using Pandora.Game.Interaction.GUI.Controls;
using Pandora.Game.Interaction.GUI.Drawing;
using System;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Skalierungs Eingenschaften erlaubt
    /// </summary>
    public interface IScaleProperty : IControl
    {
        /// <summary>
        /// Liefert die Skalierung oder legt sie fest
        /// </summary>
        Vector2f Scale { get; set; }
    }
}
