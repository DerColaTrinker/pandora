﻿using Pandora.Game.Interaction.GUI.Controls;
using System;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Rotations Eingenschaften erlaubt
    /// </summary>
    public interface IRotationProperty : IControl
    {
        /// <summary>
        /// Liefert die Rotation oder legt sie fest
        /// </summary>
        float Rotation { get; set; }
    }
}
