﻿using Pandora.Game.Interaction.GUI.Controls;
using Pandora.Game.Interaction.GUI.Drawing;
using System;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Füllfarbe Eingenschaften erlaubt
    /// </summary>
    public interface IFillColorProperty : IControl
    {
        /// <summary>
        /// Liefert die Füllfarbe oder legt sie fest
        /// </summary>
        Color FillColor { get; set; }
    }
}
