﻿using System;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEffectBase
    {
        /// <summary>
        /// 
        /// </summary>
        bool IsComplet { get; }

        /// <summary>
        /// 
        /// </summary>
        float Runtime { get; }

        /// <summary>
        /// 
        /// </summary>
        EffectStatusEnum Status { get; set; }
    }
}
