﻿using Pandora.Game.Interaction.GUI.Controls;
using Pandora.Game.Interaction.GUI.Drawing;
using System;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Linienfarb Eingenschaften erlaubt
    /// </summary>
    public interface IOutlineColorProperty : IControl
    {
        /// <summary>
        /// Liefert die Linienfarbe oder legt sie fest
        /// </summary>
        Color OutlineColor { get; set; }
    }
}
