﻿using Pandora.Game.Interaction.GUI.Controls;
using Pandora.Game.Interaction.GUI.Drawing;
using System;

namespace Pandora.Game.Interaction.GUI.Animations
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Farb Eingenschaften erlaubt
    /// </summary>
    public interface IColorProperty : IControl
    {
        /// <summary>
        /// Liefert die Farbe oder legt sie fest
        /// </summary>
        Color Color { get; set; }
    }
}
