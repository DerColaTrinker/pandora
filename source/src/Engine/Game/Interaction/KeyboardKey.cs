﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction
{
    /// <summary>
    /// 
    /// </summary>
    public enum KeyboardKey
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = -1,
        /// <summary>
        /// 
        /// </summary>
        A = 0,
        /// <summary>
        /// 
        /// </summary>
        B,
        /// <summary>
        /// 
        /// </summary>
        C,
        /// <summary>
        /// 
        /// </summary>
        D,
        /// <summary>
        /// 
        /// </summary>
        E,
        /// <summary>
        /// 
        /// </summary>
        F,
        /// <summary>
        /// 
        /// </summary>
        G,
        /// <summary>
        /// 
        /// </summary>
        H,
        /// <summary>
        /// 
        /// </summary>
        I,
        /// <summary>
        /// 
        /// </summary>
        J,
        /// <summary>
        /// 
        /// </summary>
        K,
        /// <summary>
        /// 
        /// </summary>
        L,
        /// <summary>
        /// 
        /// </summary>
        M,
        /// <summary>
        /// 
        /// </summary>
        N,
        /// <summary>
        /// 
        /// </summary>
        O,
        /// <summary>
        /// 
        /// </summary>
        P,
        /// <summary>
        /// 
        /// </summary>
        Q,
        /// <summary>
        /// 
        /// </summary>
        R,
        /// <summary>
        /// 
        /// </summary>
        S,
        /// <summary>
        /// 
        /// </summary>
        T,
        /// <summary>
        /// 
        /// </summary>
        U,
        /// <summary>
        /// 
        /// </summary>
        V,
        /// <summary>
        /// 
        /// </summary>
        W,
        /// <summary>
        /// 
        /// </summary>
        X,
        /// <summary>
        /// 
        /// </summary>
        Y,
        /// <summary>
        /// 
        /// </summary>
        Z,
        /// <summary>
        /// 
        /// </summary>
        Num0,
        /// <summary>
        /// 
        /// </summary>
        Num1,
        /// <summary>
        /// 
        /// </summary>
        Num2,
        /// <summary>
        /// 
        /// </summary>
        Num3,
        /// <summary>
        /// 
        /// </summary>
        Num4,
        /// <summary>
        /// 
        /// </summary>
        Num5,
        /// <summary>
        /// 
        /// </summary>
        Num6,
        /// <summary>
        /// 
        /// </summary>
        Num7,
        /// <summary>
        /// 
        /// </summary>
        Num8,
        /// <summary>
        /// 
        /// </summary>
        Num9,
        /// <summary>
        /// 
        /// </summary>
        Escape,
        /// <summary>
        /// 
        /// </summary>
        LControl,
        /// <summary>
        /// 
        /// </summary>
        LShift,
        /// <summary>
        /// 
        /// </summary>
        LAlt,
        /// <summary>
        /// 
        /// </summary>
        LSystem,
        /// <summary>
        /// 
        /// </summary>
        RControl,
        /// <summary>
        /// 
        /// </summary>
        RShift,
        /// <summary>
        /// 
        /// </summary>
        RAlt,
        /// <summary>
        /// 
        /// </summary>
        RSystem,
        /// <summary>
        /// 
        /// </summary>
        Menu,
        /// <summary>
        /// 
        /// </summary>
        LBracket,
        /// <summary>
        /// 
        /// </summary>
        RBracket,
        /// <summary>
        /// 
        /// </summary>
        SemiColon,
        /// <summary>
        /// 
        /// </summary>
        Comma,
        /// <summary>
        /// 
        /// </summary>
        Period,
        /// <summary>
        /// 
        /// </summary>
        Quote,
        /// <summary>
        /// 
        /// </summary>
        Slash,
        /// <summary>
        /// 
        /// </summary>
        BackSlash,
        /// <summary>
        /// 
        /// </summary>
        Tilde,
        /// <summary>
        /// 
        /// </summary>
        Equal,
        /// <summary>
        /// 
        /// </summary>
        Dash,
        /// <summary>
        /// 
        /// </summary>
        Space,
        /// <summary>
        /// 
        /// </summary>
        Return,
        /// <summary>
        /// 
        /// </summary>
        BackSpace,
        /// <summary>
        /// 
        /// </summary>
        Tab,
        /// <summary>
        ///
        /// </summary>
        PageUp,
        /// <summary>
        /// 
        /// </summary>
        PageDown,
        /// <summary>
        /// 
        /// </summary>
        End,
        /// <summary>
        /// 
        /// </summary>
        Home,
        /// <summary>
        /// 
        /// </summary>
        Insert,
        /// <summary>
        /// 
        /// </summary>
        Delete,
        /// <summary>
        /// 
        /// </summary>
        Add,
        /// <summary>
        /// 
        /// </summary>
        Subtract,
        /// <summary>
        /// 
        /// </summary>
        Multiply,
        /// <summary>
        /// 
        /// </summary>
        Divide,
        /// <summary>
        /// 
        /// </summary>
        Left,
        /// <summary>
        /// 
        /// </summary>
        Right,
        /// <summary>
        /// 
        /// </summary>
        Up,
        /// <summary>
        /// 
        /// </summary>
        Down,
        /// <summary>
        /// 
        /// </summary>
        Numpad0,
        /// <summary>
        /// 
        /// </summary>
        Numpad1,
        /// <summary>
        /// 
        /// </summary>
        Numpad2,
        /// <summary>
        /// 
        /// </summary>
        Numpad3,
        /// <summary>
        /// 
        /// </summary>
        Numpad4,
        /// <summary>
        /// 
        /// </summary>
        Numpad5,
        /// <summary>
        /// 
        /// </summary>
        Numpad6,
        /// <summary>
        /// 
        /// </summary>
        Numpad7,
        /// <summary>
        /// 
        /// </summary>
        Numpad8,
        /// <summary>
        /// 
        /// </summary>
        Numpad9,
        /// <summary>
        /// 
        /// </summary>
        F1,
        /// <summary>
        /// 
        /// </summary>
        F2,
        /// <summary>
        /// 
        /// </summary>
        F3,
        /// <summary>
        /// 
        /// </summary>
        F4,
        /// <summary>
        /// 
        /// </summary>
        F5,
        /// <summary>
        /// 
        /// </summary>
        F6,
        /// <summary>
        /// 
        /// </summary>
        F7,
        /// <summary>
        /// 
        /// </summary>
        F8,
        /// <summary>
        /// 
        /// </summary>
        F9,
        /// <summary>
        /// 
        /// </summary>
        F10,
        /// <summary>
        /// 
        /// </summary>
        F11,
        /// <summary>
        /// 
        /// </summary>
        F12,
        /// <summary>
        /// 
        /// </summary>
        F13,
        /// <summary>
        /// 
        /// </summary>
        F14,
        /// <summary>
        /// 
        /// </summary>
        F15,
        /// <summary>
        /// 
        /// </summary>
        Pause,
        /// <summary>
        /// 
        /// </summary>
        KeyCount
    };
}
