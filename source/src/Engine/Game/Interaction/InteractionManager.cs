﻿using Pandora.Game.Interaction.Caching;
using Pandora.Game.Interaction.GUI;
using Pandora.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction
{
    /// <summary>
    /// Stellt das SFML Interaction Modul dar
    /// </summary>
    /// <typeparam name="TRoot"></typeparam>
    public sealed class InteractionManager
    {
              internal InteractionManager(PandoraEngine engine)
        {
            Engine = engine;

            Logger.Trace("TranslationManager created");
        }

        internal bool Initialize()
        {
            Visuals = new VisualHandler(this);
            if (!Visuals.Initialize()) return false;

            Cache = new CacheHandler();

            Logger.Normal("InteractionManager initialized");
            return true;
        }
        
        public PandoraEngine Engine { get; private set; }

        /// <summary>
        /// Liefert das Grafiksystem
        /// </summary>
        public VisualHandler Visuals { get; private set; }

        public CacheHandler Cache { get; private set; }

        internal void SystemUpdate(float ms, float s)
        {
            Visuals.SystemUpdate(ms, s);
        }
    }
}
