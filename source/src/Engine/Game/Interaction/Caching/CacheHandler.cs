﻿using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.Caching
{
    public class CacheHandler
    {
        private Dictionary<string, Texture> _textures = new Dictionary<string, Texture>(StringComparer.InvariantCultureIgnoreCase);

        public bool LoadTexture(PandoraContentStream stream, string resourcecode)
        {
            if (!stream.File.Exists)
            {
                Logger.Error("Texture '{0}' not found", stream.File.Name);
                return false;
            }

            if(_textures.ContainsKey(resourcecode))
            {
                Logger.Warning("Texture resource code '{0}' already exists", resourcecode);
                return false;
            }


            _textures[resourcecode] = new Texture(stream) { Smooth = true };
            Logger.Debug("Texture loaded '{0}'", stream.File.Name);
            return true;
        }

        public bool ContainsTexture(string resourcecode)
        {
            return _textures.ContainsKey(resourcecode);
        }

        public Texture GetTexture(string resourcecode)
        {
            if (!_textures.ContainsKey(resourcecode))
            {
                Logger.Error("Texture resource code '{0}' not found", resourcecode);
                return null;
            }

            return _textures[resourcecode];
        }
    }
}
