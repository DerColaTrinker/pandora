using System;
using System.Runtime.Serialization;

namespace Pandora.Game.Interaction
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class LoadingFailedException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public LoadingFailedException() :
            base("Failed to load a resource")
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceName"></param>
        public LoadingFailedException(string resourceName) :
            base("Failed to load " + resourceName + " from memory")
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="innerException"></param>
        public LoadingFailedException(string resourceName, Exception innerException) :
            base("Failed to load " + resourceName + " from memory", innerException)
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="filename"></param>
        public LoadingFailedException(string resourceName, string filename) :
            base("Failed to load " + resourceName + " from file " + filename)
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="filename"></param>
        /// <param name="innerException"></param>
        public LoadingFailedException(string resourceName, string filename, Exception innerException) :
            base("Failed to load " + resourceName + " from file " + filename, innerException)
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public LoadingFailedException(SerializationInfo info, StreamingContext context) :
            base(info, context)
        { }
    }
}
