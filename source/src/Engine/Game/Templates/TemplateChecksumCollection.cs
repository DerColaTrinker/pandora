﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Eine Liste die Checksummen der Vorlagen Dateien enthält
    /// </summary>
    public sealed class TemplateChecksumCollection : IEnumerable<string>
    {
        private HashSet<string> _checksums = new HashSet<string>();
        private SHA256Managed _sha = new SHA256Managed();

        internal bool Add(Stream stream)
        {
            var pos = stream.Position;
            stream.Position = 0;

            var shaarray = _sha.ComputeHash(stream);
            var checksum = Convert.ToBase64String(shaarray);

            stream.Position = pos;

            Logger.Trace("Template checksum: '{0}'", checksum);                  
           return _checksums.Add(checksum);
        }

        /// <summary>
        /// Liefert die Anzahl der Checksummen
        /// </summary>
        public int Count { get { return _checksums.Count; } }

        #region IEnumerable<string> Member

        /// <summary>
        /// Liefert eine Auflistung
        /// </summary>
        /// <returns></returns>
        public IEnumerator<string> GetEnumerator()
        {
            return _checksums.GetEnumerator();
        }

        /// <summary>
        /// Liefert eine Auflistung
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _checksums.GetEnumerator();
        }

        #endregion

        /// <summary>
        /// Gibt an ob eine Checksumme Teil der Liste ist
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool HasChecksum(string p)
        {
            return _checksums.Contains(p);
        }
    }
}
