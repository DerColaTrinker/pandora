﻿using System;
using System.Collections.Generic;

namespace Pandora.Game.Templates
{
    public interface ITemplateManager
    {
        TemplateBase GetTemplate(string identifier);
        TTemplate GetTemplate<TTemplate>(string identifier) where TTemplate : TemplateBase;
        IEnumerable<TemplateBase> GetTemplates();
        IEnumerable<TTemplate> GetTemplates<TTemplate>();
    }
}
