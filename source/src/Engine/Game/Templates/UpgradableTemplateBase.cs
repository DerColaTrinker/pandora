﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    public abstract class UpgradableTemplateBase<TUpgradeTemplate> : TemplateBase, IUpgradableTemplateBase
        where TUpgradeTemplate : UpgradeTemplateBase
    {
        private List<TUpgradeTemplate> _upgrades = new List<TUpgradeTemplate>();

        public UpgradableTemplateBase()
            : base()
        { }

        public UpgradableTemplateBase(string identifier)
            : base(identifier)
        { }

        public void Add(TUpgradeTemplate upgrade)
        {
            if (_upgrades.Contains(upgrade))
            {
                Logger.Warning("Upgrade template '{0}' already exists", upgrade.Identifier);
                return;
            }

            _upgrades.Add(upgrade);
            upgrade.UpgradeLevel = _upgrades.Count;
        }

        public IEnumerable<TUpgradeTemplate> GetTemplates()
        {
            return _upgrades.AsEnumerable();
        }

        public TUpgradeTemplate GetTemplate(string identifier)
        {
            return _upgrades.Where(m => m.Identifier == identifier).FirstOrDefault();
        }

        public TUpgradeTemplate GetTemplate(int upgradelevel)
        {
            return _upgrades.Where(m => m.UpgradeLevel == upgradelevel).FirstOrDefault();
        }

        public Type UpgradeTemplateType { get { return typeof(TUpgradeTemplate); } }
    }
}
