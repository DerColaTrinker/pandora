﻿using System;

namespace Pandora.Game.Templates
{
    public interface IUpgradableTemplateBase
    {
        Type UpgradeTemplateType { get; }
    }
}
