﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Templates.Configuration;

namespace Pandora.Game.Templates
{
    public abstract class TemplateBase
    {
        protected TemplateBase()
            : this(Guid.NewGuid().ToString())
        { }

        public TemplateBase(string identifier)
        {
            Identifier = identifier;

            Name = string.Empty;
            Description = string.Empty;
        }

        [TemplateProperty(true, Color = SandboxColumnColors.System, Order = 0)]
        public string Identifier { get; internal set; }

        [TemplateProperty(true, Color = SandboxColumnColors.Black, Order = 0)]
        public string ResourceCode { get;  set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [SandboxPropertyRequired]
        [TemplateProperty(true, Color = SandboxColumnColors.Request, Order = 1)]
        public string Required1 { get; set; }

        [SandboxPropertyRequired]
        [TemplateProperty(true, Color = SandboxColumnColors.Request, Order = 1)]
        public string Required2 { get; set; }

        [SandboxPropertyRequired]
        [TemplateProperty(true, Color = SandboxColumnColors.Request, Order = 1)]
        public string Required3 { get; set; }

        [SandboxPropertyRequired]
        [TemplateProperty(true, Color = SandboxColumnColors.Request, Order = 1)]
        public string Required4 { get; set; }

        public virtual IEnumerable<string> OnValidate()
        {
            return null;
        }
    }
}
