﻿using Pandora.Game.Templates.Configuration;
using Pandora.Runtime;
using Pandora.Runtime.Extensions;
using Pandora.Runtime.IO;
using Pandora.Runtime.TemplateBindings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Pandora.Game.Templates
{
    public sealed class TemplateManager
    {
        private Dictionary<string, TemplateBase> _templates = new Dictionary<string, TemplateBase>();

        internal TemplateManager()
        {
            Logger.Trace("TemplateManager created");

            TemplateChecksums = new TemplateChecksumCollection();

            Bindings = new TemplateBindingHandler();
            Bindings.ScanTemplateBindings(Assembly.GetCallingAssembly());
        }

        public void AddTemplate<TTemplate>(TTemplate template)
            where TTemplate : TemplateBase
        {
            if (ContainsIdentifier(template.Identifier))
            {
                Logger.Warning("Template '{0}' alerady exists", template.Identifier);
                return;
            }

            _templates.Add(template.Identifier, template);
            Logger.Debug("Template '{0}' added", template.Identifier);
        }

        public IEnumerable<T> GetTemplates<T>() where T : TemplateBase
        {
            return _templates.Values.OfType<T>();
        }

        public IEnumerable<TemplateBase> GetTemplates(Type type)
        {
            return _templates.Values.Where(m => m.GetType() == type);
        }

        public IEnumerable<TemplateBase> GetRequestTemplates()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TemplateBase> GetTemplates()
        {
            return _templates.Values;
        }

        public TTemplate GetTemplate<TTemplate>(string identifier)
          where TTemplate : TemplateBase
        {
            var result = (TemplateBase)null;

            if (_templates.TryGetValue(identifier, out result))
            {
                try
                {
                    return (TTemplate)result;
                }
                catch (Exception)
                {
                    Logger.Error("Template '{0}' could not convert in '{1}'", identifier, typeof(TTemplate).Name);
                    return null;
                }
            }
            else
            {
                Logger.Error("Template '{0}' Guid '{1}' not found", typeof(TTemplate).Name, identifier);
                return null;
            }
        }

        public IEnumerable<string> GetTemplateIdentifiers()
        {
            return _templates.Keys.ToArray();
        }

        public bool ContainsIdentifier(string identifier)
        {
            return _templates.ContainsKey(identifier);
        }

        public int Count { get { return _templates.Count; } }

        public TemplateChecksumCollection TemplateChecksums { get; private set; }

        public IEnumerable<Type> GetTemplateTypes()
        {
            return _templates.Values.Select(m => m.GetType()).Distinct();
        }

        public TemplateBindingHandler Bindings { get; private set; }

        #region Loading File

        public bool Load(PandoraContentStream stream)
        {
            if (stream.ContentType == PandoraContentStreamTypes.Source)
            {
                if (!stream.File.Exists)
                {
                    Logger.Error("Template file '{0}' not found", stream.File.Name);
                    return false;
                }                              
            }

            Logger.Normal("Template file loading '{0}'", stream.File.Name);

            if (!TemplateChecksums.Add(stream))
            {
                Logger.Error("Template file '{0}' already loaded", stream.File.Name);
                return false;
            }
                       
            var xd = new XmlDocument();
            xd.Load(stream);

            foreach (XmlNode templatenode in xd.SelectNodes("pandora/templates/template"))
            {
                var type = templatenode.Attributes.GetValue("type", "");
                if (string.IsNullOrEmpty(type)) continue;
                var binding = Bindings.GetTemplateBinding(type);

                foreach (XmlNode instancenode in templatenode.SelectNodes("item"))
                {
                    var identifier = instancenode.Attributes.GetValue("Identifier", "");
                    if (string.IsNullOrEmpty(identifier)) continue;

                    var instance = (TemplateBase)Activator.CreateInstance(binding.TemplateType, new object[] { identifier });

                    foreach (var property in binding.Properties)
                    {
                        if (property.Name == "Identifier") continue;

                        property.SetValue(instance, Convert.ChangeType(instancenode.Attributes[property.Name].Value, property.PropertyType));
                    }

                    AddTemplate(instance);
                }
            }

            return true;
        }

        #endregion
    }
}
