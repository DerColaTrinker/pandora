﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Templates
{
    public abstract class UpgradeTemplateBase : TemplateBase
    {
        public UpgradeTemplateBase()
            : base()
        { }

        public UpgradeTemplateBase(string identifier)
            : base(identifier)
        { }

        public int UpgradeLevel { get; internal set; }

        public string BaseTemplateIdentifier { get; set; }
    }
}
