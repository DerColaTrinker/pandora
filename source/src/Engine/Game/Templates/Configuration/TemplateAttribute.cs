﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.Configuration
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class TemplateAttribute : Attribute
    {
        public TemplateAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
