﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.Configuration
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class TemplatePropertyAttribute : Attribute
    {
        internal TemplatePropertyAttribute(bool system)
        {
            SystemValue = system;
        }

        public TemplatePropertyAttribute()
        {
            Name = string.Empty;

            Color = SandboxColumnColors.Default;
        }

        public TemplatePropertyAttribute(string name)
            : this()
        {
            Name = name;
        }

        public string Name { get; internal set; }

        public SandboxColumnColors Color { get; set; }

        public bool SystemValue { get; private set; }

        public int Order { get; set; }
    }
}
