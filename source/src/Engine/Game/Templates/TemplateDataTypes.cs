﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Templates
{
    public enum TemplateDataTypes : byte
    {
        BYTE,
        BOOLEAN,
        INTEGER,
        FLOAT,
        STRING,
        ENUM,
        SELECTOR
    }
}
