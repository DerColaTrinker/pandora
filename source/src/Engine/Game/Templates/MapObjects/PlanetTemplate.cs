﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Templates.Configuration;

namespace Pandora.Game.Templates.MapObjects
{
    [Template("Planets")]
    public sealed class PlanetTemplate : TemplateBase
    {
        public PlanetTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty()]
        [SandboxRatio()]
        public float Ratio { get; set; }

        [TemplateProperty()]
        public int MinPopulation { get; set; }

        [TemplateProperty()]
        public int MaxPopulation { get; set; }

        [TemplateProperty()]
        public int MinWealth { get; set; }

        [TemplateProperty()]
        public int MaxWealth { get; set; }

        [TemplateProperty()]
        public int MinSize { get; set; }

        [TemplateProperty()]
        public int MaxSize { get; set; }

        [TemplateProperty()]
        public int MinTemperature { get; set; }

        [TemplateProperty()]
        public int MaxTemperature { get; set; }

        [TemplateProperty()]
        public float AtmosphereOxygen { get; set; }

        [TemplateProperty()]
        public float AtmosphereNitrogen { get; set; }

        [TemplateProperty()]
        public float AtmosphereMethane { get; set; }

        [TemplateProperty()]
        public float AtmosphereAmmonia { get; set; }

        [TemplateProperty()]
        public float AtmosphereCarbonDioxid { get; set; }
    }
}