﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Templates.Configuration;

namespace Pandora.Game.Templates.MapObjects
{
    [Template("Star Orbits")]
    public sealed class StarOrbitTemplates : TemplateBase
    {
        public StarOrbitTemplates(string identifier)
            : base(identifier)
        { }

        [TemplateProperty()]
        public float Radius { get; set; }

        [TemplateProperty()]
        public float MinTemperature { get; set; }

        [TemplateProperty()]
        public float MaxTemperature { get; set; }
    }
}
