﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Templates.Configuration;

namespace Pandora.Game.Templates.MapObjects
{
    [Template("Stars")]
    public sealed class StarTemplate : TemplateBase
    {
        public StarTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty()]
        [SandboxRatio()]
        public float Ratio { get; set; }
    }
}
