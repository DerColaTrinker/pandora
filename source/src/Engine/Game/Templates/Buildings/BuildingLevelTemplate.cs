﻿
using Pandora.Game.Templates.Configuration;
namespace Pandora.Game.Templates.Buildings
{
    /// <summary>
    /// Stellt eine Vorlage für eine Gebäudeausbaustufe dar
    /// </summary>
    [Template("Building-Upgrades")]
    [SandboxTemplateRequired]
    public sealed class BuildingLevelTemplate : UpgradeTemplateBase
    {
        public BuildingLevelTemplate()
            : base()
        { }

        public BuildingLevelTemplate(string idetifier)
            : base(idetifier)
        { }

        [TemplateProperty()]
        public float RequestConstructionPoints { get; set; }

        [TemplateProperty()]
        public float RequestSize { get; set; }

        #region Population

        [TemplateProperty()]
        public float MaxPopulationFix { get; set; }

        [TemplateProperty()]
        public float MaxPopulationPercent { get; set; }

        [TemplateProperty()]
        public float PopulationGrowthPercent { get; set; }

        #endregion

        #region Construction

        [TemplateProperty()]
        public float ConstructionPointsFix { get; set; }

        [TemplateProperty()]
        public float ConstructionPointsPercent { get; set; }

        [TemplateProperty()]
        public float ConstructionPointsPerPopulation { get; set; }


        #endregion

        #region Research

        [TemplateProperty()]
        public float ResearchPointsFix { get; set; }

        [TemplateProperty()]
        public float ResearchPointsPercent { get; set; }

        [TemplateProperty()]
        public float ResearchPointsPerPopulation { get; set; }

        #endregion

        #region Espionage

        [TemplateProperty()]
        public float EspionagePointsFix { get; set; }

        [TemplateProperty()]
        public float EspionagePointsPercent { get; set; }

        [TemplateProperty()]
        public float EspionagePointsPerPopulation { get; set; }

        #endregion

        #region BuildingSize

        [TemplateProperty()]
        public float BuildingSizeFix { get; set; }

        [TemplateProperty()]
        public float BuildingSizePercent { get; set; }

        #endregion

        #region Steuer Einnahmen

        [TemplateProperty()]
        public float MoneyPlusFix { get; set; }

        [TemplateProperty()]
        public float MoneyPlusPercent { get; set; }

        [TemplateProperty()]
        public float MoneyPlusPerPopulation { get; set; }

        #endregion

        #region Betriebskosten

        [TemplateProperty()]
        public float MoneyMinusFix { get; set; }

        [TemplateProperty()]
        public float MoneyMinusPercent { get; set; }

        [TemplateProperty()]
        public float MoneyMinusPerPopulation { get; set; }

        #endregion
    }
}
