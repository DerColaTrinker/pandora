﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.Buildings
{
    /// <summary>
    /// Gibt die Position des Gebäudes an
    /// </summary>
    public enum BuildingPosition : byte
    {
        /// <summary>
        /// Planet
        /// </summary>
        Planet = 0,

        /// <summary>
        /// Orbit
        /// </summary>
        Orbit = 1,

        /// <summary>
        /// Unterirdisch
        /// </summary>
        Underground = 2
    }
}
