﻿using Pandora.Game.Templates.Configuration;

namespace Pandora.Game.Templates.Buildings
{
    [Template("Buildings")]
    public sealed class BuildingTemplate : UpgradableTemplateBase<BuildingLevelTemplate>
    {
        /// <summary>
        /// Erstellt eine neue Instanz der BuildingTemplate-Klasse
        /// </summary>
        public BuildingTemplate()
            : base()
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der BuildingTemplate-Klasse
        /// </summary>
        /// <param name="guid">Vorlagen Bezeichner</param>
        public BuildingTemplate(string guid)
            : base(guid)
        { }

        [TemplateProperty()]
        public BuildingType BuildingType { get; set; }

        [TemplateProperty()]
        public BuildingPosition BuildingPosition { get; set; }

        [TemplateProperty()]
        public bool Buildable { get; set; }
    }
}
