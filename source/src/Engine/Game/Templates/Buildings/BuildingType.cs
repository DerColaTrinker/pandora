﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.Buildings
{
    /// <summary>
    /// Gibt den Gebäudetyp an
    /// </summary>
    public enum BuildingType : byte
    {
        /// <summary>
        /// Konstruktionsgebäude
        /// </summary>
        Construction = 1,

        /// <summary>
        /// Forschungsgebäude
        /// </summary>
        Science = 2,

        /// <summary>
        /// Spionagegebäude
        /// </summary>
        Espionage =3,

        /// <summary>
        /// Finanzgebäude
        /// </summary>
        Finance = 4,

        /// <summary>
        /// Wohnraum
        /// </summary>
        Population = 5,

        /// <summary>
        /// Schiffwerft
        /// </summary>
        Shipyard = 6,

        /// <summary>
        /// Planetarische Verteidigung
        /// </summary>
        Weapon = 7,

        /// <summary>
        /// Nicht angegeben
        /// </summary>
        Unkown = 0
    }
}
