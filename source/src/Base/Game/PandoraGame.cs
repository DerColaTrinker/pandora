﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    class PandoraGame : PandoraEngine
    {
        protected override bool OnLoadContent()
        {
            var datapath = Configuration.GetValue("Engine", "DataPath", "");
            if (string.IsNullOrEmpty(datapath)) return false;

            // Spieldaten laden
            if (!Translations.Load(new PandoraContentStream(Path.Combine(datapath, "de.language")))) return false;
            if (!Templates.Load(new PandoraContentStream(Path.Combine(datapath, "templates.template")))) return false;

            // Caching

            Interaction.Cache.LoadTexture(new PandoraContentStream(Path.Combine(datapath, "star_1.png")), "star_1");
            Interaction.Cache.LoadTexture(new PandoraContentStream(Path.Combine(datapath, "star_2.png")), "star_2");
            Interaction.Cache.LoadTexture(new PandoraContentStream(Path.Combine(datapath, "star_3.png")), "star_3");
            Interaction.Cache.LoadTexture(new PandoraContentStream(Path.Combine(datapath, "star_4.png")), "star_4");
                        
            return true;
        }
    }
}
