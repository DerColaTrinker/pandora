﻿using Pandora.Game.Interaction.GUI.Controls;
using Pandora.Game.Interaction.GUI.Controls.Primitives;
using Pandora.Game.Interaction.GUI.Drawing;
using Pandora.Game.Interaction.GUI.Drawing2D;
using Pandora.Game.Templates.MapObjects;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Interaction.GUI
{
    class GalaxyViewerScene : Scene
    {
        private Dictionary<string, RectShape> _systems = new Dictionary<string, RectShape>();
        private RectShapeControl _background;
        private Viewport _viewport;
        private int _zoom;
        private int _zoomstep = 10;
        private bool _rectposdo;
        private Vector2f _rectpos;

        protected override void OnLoad()
        {
            Fullscreen = true;

            _background = new RectShapeControl();
            _background.FillColor = Color.Black;
            _background.Size = Size;
            _background.Position = Vector2f.Zero;
            //Controls.Add(_background);

            _viewport = new Viewport(new Vector2f(Manager.Engine.World.Galaxy.GalaxyMapSizeX, Manager.Engine.World.Galaxy.GalaxyMapSizeY), Size);

            foreach (var template in Manager.Engine.Templates.GetTemplates<StarTemplate>())
            {
                _systems.Add(template.ResourceCode, new RectShape(new Vector2f(11, 11)) { Texture = Cache.GetTexture(template.ResourceCode) });
            }

            MouseWheel += GalaxyViewerScene_MouseWheel;
            MouseMoved += GalaxyViewerScene_MouseMoved;
            MouseButtonPressed += GalaxyViewerScene_MouseButtonPressed;
            MouseButtonReleased += GalaxyViewerScene_MouseButtonReleased;
        }

        protected override void OnResize()
        {
            _viewport.Destination = Size;
        }

        void GalaxyViewerScene_MouseMoved(Control control, Vector2f position)
        {
            if (_rectposdo)
            {
                var diff = _rectpos - position;
                _viewport.ZoomPosition += _viewport.CoordsToSource(diff);
                _rectpos = position;                
            }
        }

        void GalaxyViewerScene_MouseButtonReleased(Control control, Vector2f position, MouseButton button)
        {
            if (button == MouseButton.Right)
            {
                _rectposdo = false;
            }
        }

        void GalaxyViewerScene_MouseButtonPressed(Control control, Vector2f position, MouseButton button)
        {
            if (button == MouseButton.Right)
            {
                _rectpos = position;
                _rectposdo = true;
            }
        }

        void GalaxyViewerScene_MouseWheel(Control control, Vector2f position, int delta)
        {
            if (_zoom <= 0 & delta < 0) return;
            if (_zoom >= _zoomstep & delta > 0) return;

            _zoom += delta;
            _viewport.Zoom = (float)_zoom / (float)_zoomstep;
        }

        protected override void OnRenderUpdate(Renderer.IRenderTarget target)
        {
            foreach (var system in Manager.Engine.World.Galaxy.GetObjects<StarSystem>())
            {
                var systempos = new Vector2f(system.PositionX, system.PositionY);

                if (_viewport.SourceViewRect.Contains(systempos))
                {
                    var r = _systems[system.Template.ResourceCode];
                    r.Position = _viewport.AdjustPosition(systempos);

                    r.Draw(target);
                }
            }

            // Zuerst das durch die Control auslösen
            base.OnRenderUpdate(target);
        }
    }
}
