﻿using Pandora.Game;
using Pandora.Game.Interaction.GUI;
using Pandora.Game.World.Generators;
using Pandora.Runtime.Logging;
using Pandora.Runtime.Logging.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora
{
    class Program
    {
        static void Main(string[] args)
        {
            InitializeLogger();

            var engine = new PandoraGame();

            if(engine.Initialize())
            {
                //if (engine.CreateSingleplayergame(new SinglelPlayer("Manuel")))
                //{
                //    engine.Interaction.Visuals.Show(new GalaxyViewerScene());

                //    engine.Start();
                //}
            }

#if DEBUG
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
#endif
        }

        static void InitializeLogger()
        {
            LoggerManager.IsEnabled = true;
            LoggerManager.MinLevel = LoggerMessageType.Trace;
            LoggerManager.MaxLevel = LoggerMessageType.Error;
            LoggerManager.Add("console", new ConsoleLoggerTarget());
        }
    }
}
