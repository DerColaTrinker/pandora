﻿using Pandora.Game.Globalisation;
using Pandora.Runtime.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Pandora.Game.Tools.TranslationEditor.UI.Dialogs
{
    public partial class MainWindow : Form
    {
        private TranslationManager _manager = new TranslationManager();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            translationGridView1.UpdateDataGrid(_manager);
        }

        private void mnuFileNew_Click(object sender, EventArgs e)
        {
            _manager = new TranslationManager();
            translationGridView1.UpdateDataGrid(_manager);
        }

        private void mnuFileOpen_Click(object sender, EventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Title = "Open Translation";
                dialog.CheckFileExists = true;
                dialog.Filter = "Pandora translation file (*.language)|*.language| XML File (*.xml)|*.xml";

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var xd = new XmlDocument();
                    xd.Load(dialog.FileName);

                    _manager = new TranslationManager();

                    foreach (XmlNode dnode in xd.SelectNodes("pandora/translations/dictionary"))
                    {
                        var langcode = dnode.Attributes.GetValue("langcode", "");
                        if (string.IsNullOrEmpty(langcode)) continue;
                        if (!_manager.AddDictionary(langcode)) continue;

                        foreach (XmlNode inode in dnode.SelectNodes("entry"))
                        {
                            var identifier = inode.Attributes.GetValue("identifier", "");
                            var name = inode.Attributes.GetValue("name", string.Empty);
                            var description = inode.Attributes.GetValue("description", string.Empty);

                            _manager.AddEntry(langcode, identifier, name, description);                            
                        }
                    }

                    translationGridView1.UpdateDataGrid(_manager);
                }
            }
        }

        private void mnuFileSave_Click(object sender, EventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.Title = "Save Templates";
                dialog.CheckPathExists = true;
                dialog.Filter = "Pandora translation file (*.language)|*.language| XML File (*.xml)|*.xml";

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var writer = XmlWriter.Create(dialog.FileName);

                    writer.WriteStartDocument();
                    writer.WriteStartElement("pandora");
                    writer.WriteStartElement("translations");

                    foreach (var d in _manager.GetDictionaries())
                    {
                        writer.WriteStartElement("dictionary");
                        writer.WriteAttributeString("langcode", d.LanguageCode);

                        foreach (var i in d.GetEntries())
                        {
                            writer.WriteStartElement("entry");
                            writer.WriteAttributeString("identifier", i.Identifier);
                            writer.WriteAttributeString("name", i.Name);
                            writer.WriteAttributeString("description", i.Description);
                            writer.WriteEndElement();
                        }

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndDocument();

                    writer.Flush();
                }
            }
        }

        private void mnuFileImportTemplateIdentifiers_Click(object sender, EventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Title = "Open Template";
                dialog.CheckFileExists = true;
                dialog.Filter = "translation file (*.xml)|*.xml";

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var xd = new XmlDocument();
                    xd.Load(dialog.FileName);

                    foreach (XmlNode itemnodes in xd.SelectNodes("//pandora/templates/template/item"))
                    {
                        var identifier = itemnodes.Attributes.GetValue("Identifier", string.Empty);
                        if (string.IsNullOrEmpty(identifier)) continue;

                        _manager.AddEntry(identifier);
                        translationGridView1.UpdateDataGrid(_manager);
                    }
                }
            }
        }
    }
}
