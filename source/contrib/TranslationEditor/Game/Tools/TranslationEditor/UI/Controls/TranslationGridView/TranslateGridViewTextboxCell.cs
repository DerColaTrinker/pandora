﻿using Pandora.Game.Globalisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pandora.Game.Tools.TranslationEditor.UI.Controls.TranslationGridView
{
    class TranslateGridViewTextboxCell : DataGridViewTextBoxCell
    {
        public TranslateGridViewTextboxCell()
        {

        }

        public TranslateGridViewTextboxCell(TranslationEntry entry)
        {
            Entry = entry;
        }

        public override object Clone()
        {
            var clone = base.Clone() as TranslateGridViewTextboxCell;

            clone.Entry = Entry;

            return clone;
        }

        public TranslationEntry Entry { get; private set; }
    }
}
