﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pandora.Game.Globalisation;
using Pandora.Game.Tools.UI.Dialogs;

namespace Pandora.Game.Tools.TranslationEditor.UI.Controls.TranslationGridView
{
    partial class TranslationGridView : UserControl
    {
        private TranslationManager _manager;

        public TranslationGridView()
        {
            InitializeComponent();
        }

        public void UpdateDataGrid(TranslationManager manager)
        {
            dgvTable.Rows.Clear();

            _manager = manager;

            UpdateLanguages();
            UpdateRow();
        }

        private void UpdateLanguages()
        {
            ddlLanguage.Items.Clear();
            ddlLanguage.Items.AddRange(_manager.GetDictionaries().Select(m => m.LanguageCode).ToArray());
            ddlLanguage.SelectedItem = _manager.CurrentDictionary == null ? null : _manager.CurrentDictionary.LanguageCode;
        }

        private void UpdateRow()
        {
            dgvTable.Rows.Clear();

            if (_manager.CurrentDictionary == null)
            {
                DocumentName = "No language selected";
                return;
            }
            else
            {
                DocumentName = "Translation Editor - " + _manager.CurrentDictionary.LanguageCode.ToUpper();
            }

            foreach (var entry in _manager.CurrentDictionary.GetEntries())
            {
                var row = new TranslateGridViewRow(entry);

                row.Cells.Add(new TranslateGridViewTextboxCell(entry) { Value = entry.Identifier });
                row.Cells.Add(new TranslateGridViewTextboxCell(entry) { Value = entry.Name });
                row.Cells.Add(new TranslateGridViewTextboxCell(entry) { Value = entry.Description });

                dgvTable.Rows.Add(row);
            }
        }

        private void btnRowAdd_Click(object sender, EventArgs e)
        {
            var identifier = Guid.NewGuid().ToString();
            _manager.AddEntry(identifier, string.Empty, string.Empty);

            var entry = _manager.GetEntry(identifier);

            var row = new TranslateGridViewRow(entry);
            row.Cells.Add(new TranslateGridViewTextboxCell(entry) { Value = entry.Identifier });
            row.Cells.Add(new TranslateGridViewTextboxCell(entry) { Value = entry.Name });
            row.Cells.Add(new TranslateGridViewTextboxCell(entry) { Value = entry.Description });

            dgvTable.Rows.Add(row);
        }

        private void btnRowRemove_Click(object sender, EventArgs e)
        {
            var rows = dgvTable.SelectedRows.Cast<TranslateGridViewRow>().ToArray();

            foreach (var row in rows)
            {
                dgvTable.Rows.Remove(row);

                _manager.RemoveEntry(row.Entry.Identifier);
            }
        }

        private void dgvTable_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            var row = dgvTable.Rows[e.RowIndex] as TranslateGridViewRow;

            if (string.IsNullOrEmpty((string)row.Cells[0].Value))
                row.ErrorText = "Missing identifier";
            else
                row.ErrorText = "";
        }

        private void dgvTable_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 | e.ColumnIndex < 0) return;

            var row = dgvTable.Rows[e.RowIndex] as TranslateGridViewRow;

            switch (e.ColumnIndex)
            {
                case 0:
                    var newname = row.Cells[0].Value;
                    if (!_manager.Rename((string)row.Entry.Identifier, (string)newname))
                    {
                        row.Cells[0].Value = row.Entry.Identifier; ;
                        return;
                    }
                    break;
                case 1:
                    row.Entry.Name = (string)row.Cells[1].Value;
                    break;
                case 2:
                    row.Entry.Description = (string)row.Cells[2].Value;
                    break;
                default:
                    break;
            }
        }

        public string DocumentName { get; set; }

        private void btnAddLanguage_Click(object sender, EventArgs e)
        {
           var langcode = string.Empty;

            if (InputBox.ShowDialog("Create language", "Please enter new language code", out langcode) == DialogResult.OK)
            {
                if (_manager.AddDictionary(langcode))
                {
                    UpdateLanguages();
                    UpdateRow();
                }
            }
        }

        private void btnRemoveLanguage_Click(object sender, EventArgs e)
        {
            var langcode = (string)ddlLanguage.SelectedItem;

            if (string.IsNullOrEmpty(langcode)) return;

            if(MessageBox.Show("Do you want to delete " + langcode + "?", "Delete",  MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2)== DialogResult.Yes)
            {
                if(_manager.RemoveDictionary(langcode))
                {
                    UpdateLanguages();
                    UpdateRow();
                }
            }
        }

        private void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            var langcode = (string)ddlLanguage.SelectedItem;

            if (string.IsNullOrEmpty(langcode))
            {
                btnRowAdd.Enabled = false;
                btnRowRemove.Enabled = false;
                return;
            }
            else
            {
                btnRowAdd.Enabled = true;
                btnRowRemove.Enabled = true;
                
                _manager.SelectTranslation(langcode);
                UpdateRow();
            }
        }
    }
}
