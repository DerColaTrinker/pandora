﻿using Pandora.Game.Globalisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pandora.Game.Tools.TranslationEditor.UI.Controls.TranslationGridView
{
    class TranslateGridViewRow : DataGridViewRow
    {
        public TranslateGridViewRow()
            : base()
        { }

        public TranslateGridViewRow(TranslationEntry entry)
            : base()
        {
            Entry = entry;
        }

        public override object Clone()
        {
            var clone = base.Clone() as TranslateGridViewRow;

            clone.Entry = Entry;
            
            return clone;
        }

        public TranslationEntry Entry { get; private set; }
    }
}
