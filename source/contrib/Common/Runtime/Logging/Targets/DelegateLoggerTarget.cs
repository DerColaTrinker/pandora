﻿using System;

namespace Pandora.Runtime.Logging.Targets
{
    delegate void LoggerMessageDelegate(LoggerMessageType type, string message);
    delegate void LoggerExceptionDelegate(Exception ex, string membername, int linenumber, string file);

    delegate void LoggerBeginProgressDelegate(float count, string message);
    delegate void LoggerUpdateProgressDelegate(float percent);
    delegate void LoggerEndProgressDelegate(bool abort);

    delegate void LoggerBeginStateDelegate(string message);
    delegate void LoggerEndStateDelegate(LoggerStateResult result);

    sealed class DelegateLoggerTarget : LoggerTarget
    {
        public event LoggerMessageDelegate LoggerMessage;
        public event LoggerExceptionDelegate LoggerException;
        public event LoggerBeginProgressDelegate LoggerBeginProgress;
        public event LoggerUpdateProgressDelegate LoggerUpdateProgress;
        public event LoggerEndProgressDelegate LoggerEndProgress;
        public event LoggerBeginStateDelegate LoggerBeginState;
        public event LoggerEndStateDelegate LoggerEndState;

        protected override void OnLogMessage(LoggerMessageType type, string message, object[] args)
        {
            if (LoggerMessage != null) LoggerMessage.Invoke(type, string.Format(message, args));
        }

        protected override void OnLogException(Exception ex, string membername, int linenumber, string file)
        {
            if (LoggerException != null) LoggerException.Invoke(ex, membername, linenumber, file);
        }

        protected override void OnBeginProgress(float count, string message, object[] args)
        {
            if (LoggerBeginProgress != null) LoggerBeginProgress.Invoke(count, string.Format(message, args));
        }

        protected override void OnUpdateProgress(float count, string message, float percent)
        {
            if (LoggerUpdateProgress != null) LoggerUpdateProgress.Invoke(percent);
        }

        protected override void OnEndProgress(float count, string message, bool abort)
        {
            if (LoggerEndProgress != null) LoggerEndProgress.Invoke(abort);
        }

        protected override void OnBeginState(string message, object[] args)
        {
            if (LoggerBeginState != null) LoggerBeginState.Invoke(string.Format(message, args));
        }

        protected override void OnEndState(LoggerStateResult result)
        {
            if (LoggerEndState != null) LoggerEndState.Invoke(result);
        }
    }
}
