﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pandora.Game.Tools.UI.Dialogs
{
    public static class InputBox
    {
        public static DialogResult ShowDialog(string title, string description, out string value)
        {
            return ShowDialog(title, description, string.Empty, out value);
        }

        private static DialogResult ShowDialog(string title, string description, string defaultvalue, out string value)
        {
            value = string.Empty;

            using (var dialog = new InputBoxDialog())
            {
                dialog.Text = title;
                dialog.Description = description;
                dialog.Value = defaultvalue;

                var result = dialog.ShowDialog();

                value = dialog.Value;

                return result;
            }
        }
    }
}
