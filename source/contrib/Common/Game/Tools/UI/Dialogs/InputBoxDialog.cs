﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pandora.Game.Tools.UI.Dialogs
{
    internal partial class InputBoxDialog : Form
    {
        public InputBoxDialog()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        public string Value { get { return txtValue.Text; } set { txtValue.Text = value; } }

        public string Description { get { return lblDescription.Text; } set { lblDescription.Text = value; } }
    }
}
