﻿using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pandora.Game.Tools.TemplateEditor.UI.Controls.TemplateGridView
{
    class TemplateGridViewRequestCell : TemplateGridViewTextboxCell, ITemplateProperty
    {
        public TemplateGridViewRequestCell()
            : base()
        {

        }

        public TemplateGridViewRequestCell(TemplateBase instance, ITemplateProperty property)
            : base(instance, property)
        {

        }
    }
}
