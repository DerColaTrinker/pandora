﻿using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pandora.Game.Tools.TemplateEditor.UI.Controls.TemplateGridView
{
    class TemplateGridViewRow : DataGridViewRow
    {
        public TemplateGridViewRow()
            : base()
        { }

        public TemplateGridViewRow(object instance)
            : base()
        {
            TemplateInstance = instance as TemplateBase;
        }

        public override object Clone()
        {
            var clone = base.Clone() as TemplateGridViewRow;

            clone.TemplateInstance = TemplateInstance;

            return clone;
        }

        public TemplateBase TemplateInstance { get; private set; }
    }
}
