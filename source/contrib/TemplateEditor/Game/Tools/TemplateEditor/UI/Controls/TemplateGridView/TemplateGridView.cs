﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pandora.Game.Templates.Configuration;
using Pandora.Game.Templates;
using Pandora.Runtime.TemplateBindings;

namespace Pandora.Game.Tools.TemplateEditor.UI.Controls.TemplateGridView
{
    partial class TemplateGridView : UserControl
    {
        private TemplateBinding _binding;
        private List<TemplateBase> _templates;

        public TemplateGridView()
        {
            InitializeComponent();
        }

        public void UpdateDataGrid(List<TemplateBase> templates, TemplateBinding binding)
        {
            dgvTable.Rows.Clear();
            dgvTable.Columns.Clear();

            _templates = templates;
            _binding = binding;

            if (templates == null) return;
            if (binding == null) return;

            lblTemplate.Text = _binding.TemplateName;

            DocumentName = "Tempalte Editor - " + _binding.TemplateName;

            UpdateColumns();
        }

        private void UpdateColumns()
        {
            foreach (var property in _binding.Properties)
            {
                //if (property.Name == "Base" && property.up) { dgvTable.Columns.Add(new TemplateGridViewComboBoxColumn(property) { ValueType = property.PropertyType }); continue; }
                if (property.IsEnum) { dgvTable.Columns.Add(new TemplateGridViewComboBoxColumn(property) { ValueType = property.PropertyType }); continue; }
                if (property.IsRequiredColumn) { dgvTable.Columns.Add(new TemplateGridViewTextboxColumn(property) { ValueType = property.PropertyType }); continue; }
                if (property.PropertyType == typeof(float)) { dgvTable.Columns.Add(new TemplateGridViewTextboxColumn(property) { ValueType = typeof(float) }); continue; }
                if (property.PropertyType == typeof(int)) { dgvTable.Columns.Add(new TemplateGridViewTextboxColumn(property) { ValueType = typeof(int) }); continue; }
                if (property.PropertyType == typeof(byte)) { dgvTable.Columns.Add(new TemplateGridViewTextboxColumn(property) { ValueType = typeof(byte) }); continue; }
                if (property.PropertyType == typeof(string)) { dgvTable.Columns.Add(new TemplateGridViewTextboxColumn(property) { ValueType = typeof(string) }); continue; }
                if (property.PropertyType == typeof(bool)) { dgvTable.Columns.Add(new TemplateGridViewCheckboxColumn(property) { ValueType = typeof(bool) }); continue; }

                Logger.Error("Propertytype '{0}' not supported", property.PropertyType.Name);
            }

            foreach (var instance in _templates.Where(m => m.GetType() == _binding.TemplateType))
            {
                var row = CreateRow(instance);

                dgvTable.Rows.Add(row);
            }
        }

        private void btnRowAdd_Click(object sender, EventArgs e)
        {
            var identifier = Guid.NewGuid().ToString();

            var instance = Activator.CreateInstance(_binding.TemplateType, new object[] { identifier }) as TemplateBase;

            var row = CreateRow(instance);

            _templates.Add(instance);

            dgvTable.Rows.Add(row);
        }

        private void btnRowRemove_Click(object sender, EventArgs e)
        {
            var rows = dgvTable.SelectedRows.Cast<TemplateGridViewRow>().ToArray();

            foreach (var row in rows)
            {
                _templates.Remove(row.TemplateInstance);
                dgvTable.Rows.Remove(row);

                foreach (var item in _templates)
                {
                    if (!string.IsNullOrEmpty(row.TemplateInstance.Required1) && item.Required1 == "") { item.Required1 = string.Empty; Logger.Trace("Delete Request 1 '{0}' -> '{1}'.", row.TemplateInstance.Identifier, item.Identifier); }
                    if (!string.IsNullOrEmpty(row.TemplateInstance.Required2) && item.Required2 == "") { item.Required2 = string.Empty; Logger.Trace("Delete Request 2 '{0}' -> '{1}'.", row.TemplateInstance.Identifier, item.Identifier); }
                    if (!string.IsNullOrEmpty(row.TemplateInstance.Required3) && item.Required3 == "") { item.Required3 = string.Empty; Logger.Trace("Delete Request 3 '{0}' -> '{1}'.", row.TemplateInstance.Identifier, item.Identifier); }
                    if (!string.IsNullOrEmpty(row.TemplateInstance.Required4) && item.Required4 == "") { item.Required4 = string.Empty; Logger.Trace("Delete Request 4 '{0}' -> '{1}'.", row.TemplateInstance.Identifier, item.Identifier); }
                }
            }
        }

        private TemplateGridViewRow CreateRow(TemplateBase instance)
        {
            var row = new TemplateGridViewRow(instance);

            foreach (DataGridViewColumn column in dgvTable.Columns)
            {
                var property = column as ITemplateProperty;
                if (column is TemplateGridViewTextboxColumn && property.TemplateProperty.IsRequiredColumn) { row.Cells.Add(new TemplateGridViewRequestCell(instance, (ITemplateProperty)column) { ValueType = typeof(string) }); continue; }
                if (column is TemplateGridViewTextboxColumn) { row.Cells.Add(new TemplateGridViewTextboxCell(instance, (ITemplateProperty)column) { ValueType = column.ValueType }); continue; }
                if (column is TemplateGridViewCheckboxColumn) { row.Cells.Add(new TemplateGridViewCheckboxCell(instance, (ITemplateProperty)column) { ValueType = column.ValueType }); continue; }
                if (column is TemplateGridViewComboBoxColumn && property.TemplateProperty.IsEnum) { row.Cells.Add(new TemplateGridViewEnumCell(instance, (ITemplateProperty)column) { ValueType = typeof(string) }); continue; }
                if (column is TemplateGridViewComboBoxColumn && property.TemplateProperty.Name == "Base" && _binding.IsUpgradable) { row.Cells.Add(new TemplateGridViewBaseCell(_templates, instance, (ITemplateProperty)column) { ValueType = typeof(string) }); continue; }
            }

            return row;
        }

        private void dgvTable_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var row = dgvTable.Rows[e.RowIndex] as TemplateGridViewRow;
            var cell = row.Cells[e.ColumnIndex];
            var property = ((ITemplateProperty)cell).TemplateProperty;
            var instance = row.TemplateInstance;

            if (property.PropertyType.IsEnum)
            {
                property.SetValue(instance, Enum.Parse(property.PropertyType, (string)cell.Value));
            }
            else
            {
                if (property.Name == "Identifier")
                {
                    // Wenn es sich um einen Key handelt muss dieser zu erst aus der Templateliste entfernt und wieder neu eingefügt werden
                    var oldidentifier = (string)property.GetValue(instance);
                    var newidentifier = (string)cell.Value;

                    if (string.IsNullOrEmpty(newidentifier)) { cell.Value = oldidentifier; return; }

                    foreach (var template in _templates)
                    {
                        if (!string.IsNullOrEmpty(template.Required1) && template.Required1 == oldidentifier) { template.Required1 = newidentifier; Logger.Trace("Rename Request 1 '{0}' -> '{1}'.", template.Identifier, newidentifier); }
                        if (!string.IsNullOrEmpty(template.Required2) && template.Required2 == oldidentifier) { template.Required2 = newidentifier; Logger.Trace("Rename Request 2 '{0}' -> '{1}'.", template.Identifier, newidentifier); }
                        if (!string.IsNullOrEmpty(template.Required3) && template.Required3 == oldidentifier) { template.Required3 = newidentifier; Logger.Trace("Rename Request 3 '{0}' -> '{1}'.", template.Identifier, newidentifier); }
                        if (!string.IsNullOrEmpty(template.Required4) && template.Required4 == oldidentifier) { template.Required4 = newidentifier; Logger.Trace("Rename Request 4 '{0}' -> '{1}'.", template.Identifier, newidentifier); }
                    }
                }
                else
                {
                    property.SetValue(instance, cell.Value);
                }
            }
        }

        private void dgvTable_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            var row = dgvTable.Rows[e.RowIndex];
            var cell = row.Cells[e.ColumnIndex];

            cell.ErrorText = e.Exception.Message;
            row.ErrorText = e.Exception.Message;
        }

        private void dgvTable_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            var row = dgvTable.Rows[e.RowIndex];
            var cell = row.Cells[e.ColumnIndex];

            if (cell.ColumnIndex == 0 && cell.Value == null)
            {
                cell.ErrorText = "Es muss ein Eindeutiger Key verwendet werden.";
                row.ErrorText = "";
            }
            else
            {
                cell.ErrorText = "";
                row.ErrorText = "";
            }
        }

        private void dgvTable_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            var row = dgvTable.Rows[e.RowIndex] as TemplateGridViewRow;
            var instance = row.TemplateInstance;

            // TODO: Validation
            //instance.ValidationErrorText = "";

            //if (!instance.OnValidate())
            //    row.ErrorText = instance.ValidationErrorText;
        }

        public string DocumentName { get; set; }

        private void btnSychronizeTranslation_Click(object sender, EventArgs e)
        {
            dgvTable.Rows.Clear();
            dgvTable.Columns.Clear();

            UpdateColumns();
        }

        private void dgvTable_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            var cell = dgvTable.CurrentCell;
            var column = dgvTable.Columns[cell.ColumnIndex];

            if (e.Control is TextBox)
            {
                var control = e.Control as TextBox;

                if (cell is TemplateGridViewRequestCell)
                {
                    control.Multiline = false;

                    control.AutoCompleteCustomSource.Clear();
                    control.AutoCompleteCustomSource.AddRange(_templates.Select(m => m.Identifier).ToArray());

                    control.AutoCompleteMode = AutoCompleteMode.Suggest;
                    control.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    return;
                }
            }
        }
    }
}
