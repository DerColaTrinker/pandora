﻿using Pandora.Runtime.TemplateBindings;
using Pandora.Game.Templates.Configuration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pandora.Game.Tools.TemplateEditor.UI.Controls.TemplateGridView
{
    class ColumnColorHelper
    {
        public static void SetColor(TemplatePropertyBinding propertybinding, DataGridViewColumn column)
        {
            switch (propertybinding.ColumnColor)
            {
                case SandboxColumnColors.Default:
                    column.DefaultCellStyle.BackColor = Color.White;
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.System:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(128, 0, 0);
                    column.DefaultCellStyle.ForeColor = Color.White;
                    break;
                case SandboxColumnColors.Request:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(80, 0, 0);
                    column.DefaultCellStyle.ForeColor = Color.White;
                    break;
                case SandboxColumnColors.Color1:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(251, 213, 181);
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.Color2:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(183, 221, 232);
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.Color3:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(204, 193, 217);
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.Color4:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(215, 227, 188);
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.Color5:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(229, 185, 183);
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.Color6:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(184, 204, 228);
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.Color7:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(141, 179, 226);
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.Color8:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(196, 189, 151);
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.Color9:
                    column.DefaultCellStyle.BackColor = Color.FromArgb(192, 192, 192);
                    column.DefaultCellStyle.ForeColor = Color.Black;
                    break;
                case SandboxColumnColors.Black:
                    column.DefaultCellStyle.BackColor = Color.Black;
                    column.DefaultCellStyle.ForeColor = Color.White;
                    break;
                default:
                    break;
            }
        }
    }
}
