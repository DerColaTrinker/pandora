﻿using Pandora.Runtime.TemplateBindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pandora.Game.Tools.TemplateEditor.UI.Controls.TemplateGridView
{
    class TemplateGridViewCheckboxColumn : DataGridViewCheckBoxColumn, ITemplateProperty
    {
        public TemplateGridViewCheckboxColumn(TemplatePropertyBinding desc)
            : base()
        {
            TemplateProperty = desc;
            HeaderText = desc.Name;
            Name = desc.Name;

            ColumnColorHelper.SetColor(desc, this);
        }

        public TemplatePropertyBinding TemplateProperty { get; private set; }

        public override object Clone()
        {
            var original = base.Clone() as TemplateGridViewCheckboxColumn;
            original.TemplateProperty = TemplateProperty;
            return original;
        }
    }
}
