﻿using Pandora.Game.Templates;
using Pandora.Runtime.TemplateBindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pandora.Game.Tools.TemplateEditor.UI.Controls.TemplateGridView
{
    class TemplateGridViewEnumCell : DataGridViewComboBoxCell, ITemplateProperty
    {
        private ITemplateProperty _property;
        private TemplateBase _instance;

        public TemplateGridViewEnumCell()
        { }

        public TemplateGridViewEnumCell(TemplateBase instance, ITemplateProperty property)
            : base()
        {
            _property = property;
            _instance = instance;

            Items.AddRange(Enum.GetNames(property.TemplateProperty.PropertyType));

            var current = TemplateProperty.GetValue(_instance).ToString();
            var index = Items.IndexOf(current);

            base.Value = current;
        }

        public override object Clone()
        {
            var clone = base.Clone() as TemplateGridViewEnumCell;

            clone._property = _property;
            clone._instance = _instance;

            return clone;
        }

        #region ITemplateProperty Member

        public TemplatePropertyBinding TemplateProperty
        {
            get { return _property.TemplateProperty; }
        }

        #endregion
    }
}
