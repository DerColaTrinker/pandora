﻿using Pandora.Game.Templates;
using Pandora.Runtime.TemplateBindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pandora.Game.Tools.TemplateEditor.UI.Controls.TemplateGridView
{
    class TemplateGridViewTextboxCell : DataGridViewTextBoxCell, ITemplateProperty
    {
        private ITemplateProperty _property;
        private TemplateBase _instance;

        public TemplateGridViewTextboxCell()
        {

        }

        public TemplateGridViewTextboxCell(TemplateBase instance, ITemplateProperty property)
        {
            _property = property;
            _instance = instance;

            ReadOnly = property.TemplateProperty.ReadOnly;
            Value = _property.TemplateProperty.GetValue(_instance);
        }

        public override object Clone()
        {
            var clone = base.Clone() as TemplateGridViewTextboxCell;

            clone._property = _property;
            clone._instance = _instance;

            return clone;
        }

        #region ITemplateProperty Member

        public TemplatePropertyBinding TemplateProperty
        {
            get { return _property.TemplateProperty; }
        }

        #endregion
    }
}
