﻿using Pandora.Runtime.TemplateBindings;
using System;

namespace Pandora.Game.Tools.TemplateEditor.UI.Controls.TemplateGridView
{
    interface ITemplateProperty
    {
        TemplatePropertyBinding TemplateProperty { get; }
    }
}
