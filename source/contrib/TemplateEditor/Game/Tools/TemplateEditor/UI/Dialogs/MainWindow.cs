﻿using Pandora.Runtime.TemplateBindings;
using Pandora.Runtime.Extensions;
using System.Xml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pandora.Game.Templates;

namespace Pandora.Game.Tools.TemplateEditor.UI.Dialogs
{
    public partial class MainWindow : Form
    {
        private List<Pandora.Game.Templates.TemplateBase> _templates = new List<Pandora.Game.Templates.TemplateBase>();
        private TemplateBindingHandler _binding;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            _binding = new TemplateBindingHandler();

            foreach (var item in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (item.FullName.Contains("ppengine")) _binding.ScanTemplateBindings(item);
            }

            foreach (var binding in _binding.GetTemplateBindings())
            {
                var item = new ToolStripMenuItem() { Text = binding.TemplateName };
                item.Click += InternalTemplateSelect;
                item.Tag = binding;

                mnuTemplates.DropDownItems.Add(item);
            }
        }

        private void InternalTemplateSelect(object sender, EventArgs e)
        {
            templateGridView1.UpdateDataGrid(_templates, (TemplateBinding)((ToolStripMenuItem)sender).Tag);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _templates.Clear();

            templateGridView1.UpdateDataGrid(_templates, null);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Title = "Open Templates";
                dialog.CheckFileExists = true;
                dialog.Filter = "Pandora template file (*.template)|*.template| XML File (*.xml)|*.xml";

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var xd = new XmlDocument();
                    xd.Load(dialog.FileName);

                    _templates.Clear();

                    foreach (XmlNode templatenode in xd.SelectNodes("pandora/templates/template"))
                    {
                        var type = templatenode.Attributes.GetValue("type", "");
                        if (string.IsNullOrEmpty(type)) continue;
                        var binding = _binding.GetTemplateBinding(type);

                        foreach (XmlNode instancenode in templatenode.SelectNodes("item"))
                        {
                            var identifier = instancenode.Attributes.GetValue("Identifier", "");
                            if (string.IsNullOrEmpty(identifier)) continue;

                            var instance = (TemplateBase)Activator.CreateInstance(binding.TemplateType, new object[] { identifier });

                            foreach (var property in binding.Properties)
                            {
                                if (property.Name == "Identifier") continue;

                                var value = (string)null;

                                try
                                {
                                    value = instancenode.Attributes[property.Name].Value;

                                    property.SetValue(instance, Convert.ChangeType(value, property.PropertyType));
                                }
                                catch (Exception)
                                {
                                    Logger.Warning("Missing property '{0}' in source file", property.Name);
                                }                                
                            }

                            _templates.Add(instance);
                        }
                    }
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.Title = "Save Templates";
                dialog.CheckPathExists = true;
                dialog.Filter = "Pandora template file (*.template)|*.template| XML File (*.xml)|*.xml";

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var writer = XmlWriter.Create(dialog.FileName);

                    writer.WriteStartDocument();

                    XmlWritePandoraCore(writer);

                    writer.WriteEndDocument();

                    writer.Flush();
                }
            }
        }

        private void XmlWritePandoraCore(XmlWriter writer)
        {
            writer.WriteStartElement("pandora");

            XmlWriteTemplates(writer);

            writer.WriteEndElement();
        }

        private void XmlWriteTemplates(XmlWriter writer)
        {
            writer.WriteStartElement("templates");

            XmlWriteTemplateInstance(writer);

            writer.WriteEndElement();
        }

        private void XmlWriteTemplateInstance(XmlWriter writer)
        {
            foreach (var item in _binding.GetTemplateBindings())
            {
                writer.WriteStartElement("template");
                writer.WriteAttributeString("type", item.TemplateType.FullName);

                foreach (var instance in _templates.Where(m => m.GetType() == item.TemplateType))
                {
                    writer.WriteStartElement("item");
                    foreach (var property in item.Properties)
                    {
                        writer.WriteAttributeString(property.Name, Convert.ToString(property.GetValue(instance)));
                    }
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }
        }
    }
}
